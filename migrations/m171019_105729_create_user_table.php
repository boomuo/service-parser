<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171019_105729_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string(),
            'password' => $this->string(),
            'email' => $this->string(),
            'authKey' => $this->string(),
            'accessToken' => $this->string(),
            'level' => $this->string(),
            'datetime' => $this->datetime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
