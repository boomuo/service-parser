<?php

use yii\db\Migration;

class m171019_115816_rename_login_column_to_user_table extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('User', 'login', 'username');
    }

    public function safeDown()
    {
        echo "m171019_115816_rename_login_column_to_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171019_115816_rename_login_column_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
