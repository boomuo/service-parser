<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tubes`.
 */
class m171019_110837_create_tubes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tubes', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'descriptionTube' => $this->string(),
            'url' => $this->string(),
            'mainPaginationPage' => $this->string(),
            'searchPaginationPage' => $this->string(),
            'categoriesPaginationPage' => $this->string(),
            'categoriesListPage' => $this->string(),
            'categoriesListLinkTag' => $this->string(),
            'articlesListLinkTag' => $this->string(),
            'titleTag' => $this->string(),
            'fileLink' => $this->string(),
            'posterLink' => $this->string(),
            'tagsTag' => $this->string(),
            'descriptionTag' => $this->string(),
            'translateRusEng' => $this->string(),
            'dataAuth' => $this->string(),
            'trailer' => $this->string(),
            'posting' => $this->string(),
            'postingOnline' => $this->string(),
            'txt' => $this->string(),
            'status' => $this->string()->defaultValue(1),
            'dateAdd' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tubes');
    }
}
