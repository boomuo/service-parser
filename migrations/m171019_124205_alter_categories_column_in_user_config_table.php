<?php

use yii\db\Migration;

class m171019_124205_alter_categories_column_in_user_config_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user_config', 'categories', $this->string());
    }

    public function safeDown()
    {
        echo "m171019_124205_alter_categories_column_in_user_config_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171019_124205_alter_categories_column_in_user_config_table cannot be reverted.\n";

        return false;
    }
    */
}
