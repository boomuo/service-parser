<?php

use yii\db\Migration;

/**
 * Handles adding alert to table `archive`.
 */
class m171020_065141_add_alert_column_to_archive_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('archive', 'alert', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('archive', 'alert');
    }
}
