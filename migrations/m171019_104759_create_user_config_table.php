<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userConfig`.
 */
class m171019_104759_create_user_config_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_config', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'categories' => $this->integer(),
            'proxy' => $this->string(),
            'minSaveSize' => $this->integer(),
            'countPosters' => $this->integer(),
            'forumCountPosters' => $this->integer(),
            'forumHosting' => $this->string(),
            'forumHostingSize' => $this->string(),
            'siteHosting' => $this->string(),
            'siteHostingSize' => $this->string(),
            'screenshotCols' => $this->integer(),
            'screenshotRows' => $this->integer(),
            'screenshotWidth' => $this->integer(),
            'screenshotHostingSize' => $this->string(),
            'archivesSizeCheck' => $this->integer(),
            'archivesSize' => $this->integer(),
            'createTxt' => $this->string(),
            'templateTxtFiles' => $this->text(),
            'k2s' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_config');
    }
}
