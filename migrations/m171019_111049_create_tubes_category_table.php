<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tubesCategory`.
 */
class m171019_111049_create_tubes_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tubes_category', [
            'id' => $this->primaryKey(),
            'category' => $this->string(),
            'tube' => $this->string(),
            'tubeCategory' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tubes_category');
    }
}
