<?php

use yii\db\Migration;

/**
 * Handles the creation of table `archive`.
 */
class m171019_185244_create_archive_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('archive', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'tube' => $this->string(),
            'studio' => $this->string(),
            'url' => $this->string(),
            'category' => $this->string(),
            'actors' => $this->string(),
            'description' => $this->string(),
            'tags' => $this->string(),
            'fileName' => $this->string(),
            'fileLink' => $this->string(),
            'posterLink' => $this->string(),
            'trailerLink' => $this->string(),
            'size' => $this->string(),
            'duration' => $this->string(),
            'durationSecond' => $this->string(),
            'dimensions' => $this->string(),
            'poster' => $this->string(),
            'screenshot' => $this->string(),
            'trailer' => $this->string(),
            'links' => $this->string(),
            'status' => $this->string(),
            'dateTime' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('archive');
    }
}
