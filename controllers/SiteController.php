<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\Parser\Main;

use Sunra\PhpSimple\HtmlDomParser;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'index', 'about', 'contact', 'captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['login', 'signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'roles'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($test = false)
    {

        if($test)
        {
            for ($i = 1; $i <= 5; $i++)
            {
                echo '<p>буфер</p>'.$i;
                flush();
                @ob_flush();
                sleep(1);
            }
        }
        else
        { 
            $tubes = Main::getTubes();
            return $this->render('index', [
                'tubes' => $tubes,
            ]);
        }
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if($model->load(Yii::$app->request->post()) && $model->login())
        {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Registration new user
     *
     * @return Response
     */
    public function actionSignup()
    {
        if(!Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }

        $model = new SignupForm();
        if($model->load(Yii::$app->request->post()) && $model->signup())
        {
            return $this->redirect('/config');
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        if(Yii::$app->request->isAjax)
            return $this->renderAjax('about', ['test' => 'ddddd']);
        return $this->render('about');
    }

    /**
     * Add and change users roles
     *
     * @return string
     */
    public function actionCreateAdmin()
    {
        // // создаем роль
        $role = $auth->createRole('admin');
        $auth->add($role);
        
        $auth->assign($role, 1);
        return $this->render('index');
    }

    public function actionRoles()
    {
        $auth = Yii::$app->authManager;

        // // создаем роль
        // $role = $auth->createRole('user-pay');
        // $auth->add($role);

        // // создаем разрешение
        // $permission = $auth->createPermission('forum-site');
        // $permission->description = 'Доступ к форумам и сайтам';
        // $auth->add($permission);

        // даём роли разрешение
        // $role = $auth->getRole('admin');
        // $role1 = $auth->getRole('user');
        // $permission = $auth->getPermission('forum-site');
        // $auth->addChild($role, $role1);

        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        $role = $auth->getRole('forum-site');
        echo '<pre>';
        print_r($role);
        echo '</pre>';
        exit();
        $auth->assign($role, 3);

        return $this->render('index');
    }
}
