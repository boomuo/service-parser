<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\ConfigUser;
use app\models\ConfigSite;
use app\models\Categories;
use app\models\Parser\Main;
use app\models\Parser\Publication;

class ConfigController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['site', 'forum', 'index'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['site-create', 'scan-category'],
						'allow' => true,
						'roles' => ['user-pay'],
					],
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}


	/**
	 * Finds the ConfigUser model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return ConfigUser the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	private function getSelect()
	{
		$select['size'] = [
			'small' => 'small 250x250',
			'medium' => 'medium 500x500',
			'large' => 'large 800x800',
			// 'original' => 'original',
		];

		$select['photoHostings'] = [
			'pimpandhost' => 'pimpandhost',
			'imagebam' => 'imagebam',
			'pixhost' => 'pixhost',
			'crazyimg' => 'crazyimg',
			'picporn' => 'picporn',
			'imagevenue' => 'imagevenue',
			// 'picstate' => 'picstate',
			// 'greenpiccs' => 'greenpiccs',
			// 'imagetwist' => 'imagetwist',
		];
		$select['fileHostings'] = [
			'keep2share.cc' => 'keep2share.cc',
			'depfile.com' => 'depfile.com',
			// 'rapidgator.com' => 'rapidgator.com',
		];
		$select['tubes'] = [
			'k2s.cc' => 'k2s.cc',
			'fboom.me' => 'fboom.me',
			'pornolab.net' => 'pornolab.net',
		];
		return $select;
	}


	public function actionIndex()
	{
		Main::init(Yii::$app->user->identity->id); // Инициация пользователя
		// if(!Yii::$app->user->can('admin'))
		// {
		// 	return $this->render('error', [
		// 		'message' => 'Страничка времеено не доступна. Ведутся технические работы. Скоро все заработает.',
		// 		'name' => 'Техничесие работы',
		// 	])
		// }

		if(!$model = ConfigUser::findOne(['user_id' => Main::$user->id]))
			$model = new ConfigUser;

		if($model->load(Yii::$app->request->post()))
		{
			if(empty($model->user_id))
				$model->user_id = Main::$user->id;
			// Проверяем данные аккаунтов и сериализуем

			// Подготовка accountFileHosting
			if(is_array($model->categories))
			{
				$model->categories = serialize($model->categories);
			}
			else
			{
				// $error['danger'] = 'Вы не добавили ниодной категории!';
				// $model->accountFileHosting = null;
			}

			// Подготовка accountFileHosting
			if(is_array($model->accountFileHosting))
			{
				foreach($model->accountFileHosting as $acc)
				{
					if(@$acc['hosting'] && @$acc['login'])
						$accountsFile[] = $acc;
				}
				$model->accountFileHosting = null;
				if(@$accountsFile)
					$model->accountFileHosting = serialize($accountsFile);
			}
			else
			{
				// $error['danger'] = 'Вы не добавили ниодного файлообменника!';
				// $model->accountFileHosting = null;
			}

			// Подготовка accountPhotoHosting
			if(is_array($model->accountPhotoHosting))
			{
				foreach($model->accountPhotoHosting as $acc)
				{
					if(@$acc['hosting'] && @$acc['login'])
						$accountsPhoto[] = $acc;
				}
				$model->accountPhotoHosting = null;
				if(@$accountsPhoto)
					$model->accountPhotoHosting = serialize($accountsPhoto);
			}
			else
			{
				$model->accountPhotoHosting = null;
			}

			// Подготовка accountTubeSite
			if(is_array($model->accountTubeSite))
			{
				foreach($model->accountTubeSite as $acc)
				{
					if(@$acc['hosting'] && @$acc['login'] && @$acc['password'])
						$accountTubeSite[] = $acc;
				}
				$model->accountTubeSite = null;
				if(@$accountTubeSite)
					$model->accountTubeSite = serialize($accountTubeSite);
			}
			else
			{
				$model->accountTubeSite = null;
			}

			// Подготовка categories
			if(is_array($model->categories))
			{
				$model->categories = implode(',', $model->categories);
			}

			// Подготовка categories forums
			if(is_array($model->categoriesForums))
			{
				$model->categoriesForums = serialize($model->categoriesForums);
			}

			// Подготовка tubes
			if(is_array($model->tubes))
			{
				$model->tubes = implode(',', $model->tubes);
			}

			if(!@$error && $model->save())
			{
				$alert['success'] = 'Нaстройки сохранены';
			}
			else
			{
				$alert['danger'] = 'Ошибка при сохранении настроек';
				echo '<pre>';
				print_r(@$alert);
				print_r(@$error);
				print_r($model->errors);
				print_r($model);
				echo '</pre>';
				exit();
			}

			Yii::$app->session->setFlash('alert', $alert);
			return $this->redirect('/config');
		}
		else
		{
			if(empty($model->user_id))
			{
				$alert['info'] = 'Сохраните свои настроки';

				$model = new ConfigUser();
				$model->user_id = Main::$user->id;
				$model->forumPosterCount = 1;
				$model->forumPosterHostingSize = 'large';
				// $model->archivesSize = 5000;
				// $model->archivesSizeCheck = 5100;
				$model->screenshotCols = 3;
				$model->screenshotRows = 3;
				$model->screenshotWidth = 900;
				// $model->trailerCreate = 0;
				// $model->trailerCountSize = '10:2';
				// $model->createTxt = 1;
				$model->md5Change = 1;
				$model->uploadInFolder = 1;
				$model->txtFilesDelimer = '==========';
			}
			else
			{
				$model->accountFileHosting = unserialize($model->accountFileHosting);
				$model->accountPhotoHosting = unserialize($model->accountPhotoHosting);
				$model->accountTubeSite = unserialize($model->accountTubeSite);
				$model->categories = unserialize($model->categories);
				$model->categoriesForums = unserialize($model->categoriesForums);
			}
		}

		if(Yii::$app->session->hasFlash('alert'))
			$alert = Yii::$app->session->getFlash('alert');

		return $this->render('config', [
			'model' => $model,
			'alert' => @$alert,
			'error' => @$error,
			// 'categories' => Main::getCategories(),
			'tubes' => Main::getTubes(),
			'tubesCategories' => Main::getTubesWithCategories(),
			'select' => $this->getSelect(),
		]);
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public function actionSite($id = false, $t = false)
	{
		Main::init(Yii::$app->user->identity->id); // Инициация пользователя
		// $dataProvider = new ActiveDataProvider([
		// 	'query' => ConfigSite::find(),
		// ]);

		// $categories = Main::getCategoriesList();
		$categories = Main::getCategoriesUser();
		if($categoriesForums = Main::getCategoriesForumsUser())
		{
			foreach($categoriesForums as $key => $item)
			{
				$categories[] = (object) ['title' => $item->name];
			}
		}

		if(!$model = ConfigSite::findOne(['user_id' => Main::$user->id, 'id' => $id]))
		{
			$model = new ConfigSite;
		}

		if($model->load(Yii::$app->request->post()))
		{
			$model->user_id = Main::$user->id;
			// $model->urlOnlyDomain();

			// Подготовка categories
			if(is_array($model->categories))
			{
				$model->categories = serialize($model->categories);
			}
			else
				$model->categories = null;

			// Подготовка templates
			if(is_array($model->templates))
			{
				$model->templates = serialize($model->templates);
			}
			else
				$model->templates = null;


			if($model->save())
				$alert['success'] = 'Насйтроки сохранены';
			else
			{
				echo '<pre>';
				print_r($model);
				echo '</pre>';
				$alert['danger'] = 'Насйтроки не сохранены';
			}
		}

		// Список всех сайтов
		$allSite = [];
		foreach(ConfigSite::findAll(['user_id' => Main::$user->id]) as $key => $site)
		{
			$site->site = parse_url($site->site, PHP_URL_HOST);
			$allSite[$site->site] = $site;
		}
		ksort($allSite);

		// Если указан ид сайта
		if($id)
		{
			// Ссылки на сайт
			$links = Publication::getLinks($model);

			// Если достали с базы данные веток
			if($model->categories = unserialize($model->categories))
			{
				foreach($model->categories as $category)
				{
					$cat = $category;
					$cat += [
						'threadLink' => $links['thread'].$category['threadId'],
						'addPostLink' => $links['addPost'].$category['threadId'],
					];
					$categoriesList[] = $cat;
				}
			}
			else
			{
				$categoriesList = false;
			}

			// Если достали с базы данные шаблонов
			if($model->templates = unserialize($model->templates))
			{
				foreach($categories as $key => $category)
				{
					@$templates[$key] = $model->templates[$category->id];
					@$templates[$key]['template'] = $links['template'].$model->templates[$category->id]['forumId'];
					@$templates[$key]['addThread'] = $links['addThread'].$model->templates[$category->id]['forumId'];
					// @$templates[$key]['categoryId'] = $category->id;
					@$templates[$key]['categoryTitle'] = $category->title;

					// if(isset($categoriesList[$category->title]))
					// 	$templates[$key]['style'] = 'bg-success';
				}
			}
			else
			{
				foreach($categories as $key => $category)
				{
					$templates[$category->title] = [
						// 'categoryId' => $category->id,
						'categoryTitle' => $category->title,
						'forumId' => null,
					];
				}
			}

			if($t)
			{
				// Массив с ид форума
				Yii::$app->cache->delete('forumsHtml'.$model->id);
				if(!$forumsHtml = Yii::$app->cache->get('forumsHtml'.$model->id))
				{
					$forumsHtml = Publication::sendData(['url' => $links['sitemap']['link']], 1, 0, 0)['html']->find($links['sitemap']['tag']);
					if(!empty($forumsHtml))
					{
						foreach($forumsHtml as $key => $item)
						{
							unset($forumsHtml[$key]);
							if(!empty($item->href) && !empty($item->plaintext))
							{
								// Если есть ссылка
								if(!empty(@$item->href))
								{	
									// Если найден ИД в ссылке
									if(preg_match('/(\d+)/', $item->href, $match) && !@empty($match[0]))
									{
										$forumsHtml[$key]['id'] = $match[0];
									}
								}

								$forumsHtml[$key] = [
									'href' => $item->href,
									'title' => $item->plaintext,
								];
							}
						}
					}
					Yii::$app->cache->set('forumsHtml'.$model->id, $forumsHtml);
				}
			}
		}

		return $this->render('config-site', [
			// 'dataProvider' => $dataProvider,
			'allSite' => $allSite,
			'forums' => @$forums,
			'templates' => @$templates,
			'links' => @$links,
			'model' => $model,
			'alert' => @$alert,
			'categories' => $categories,
			'categoriesList' => @$categoriesList,
			'select' => $this->getSelect(),
		]);
	}


	/**
	 * Creates a new ConfigSite model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionSiteCreate($item = 'site')
	{
		$model = new ConfigSite();
		$model->templateFullStory = "{poster}

[b]Tags:[/b] {tags}
[b]Duration: [/b]{duration}
[b]Video: [/b]{display}
[b]Size: [/b]{size}

{screenshot}

[b]Download links and watch online:[/b]
[b][url={link_k2s}]{link_k2s}[/url][/b]

[b][url={link_fboom}]{link_fboom}[/url][/b]";

		if($model->load(Yii::$app->request->post()))
		{
			$model->user_id = Yii::$app->user->identity->id;
			// $model->urlOnlyDomain();

			// Подготовка categories
			if(is_array($model->categories))
			{
				// foreach($model->categories as $cat)
				// {
				// 	if(@$cat['categoryId'] && @$cat['thredId'])
				// 		$categories[] = $cat;
				// }
				// if(@$categories)
					$model->categories = serialize($model->categories);
			}
			else
			{
				$model->categories = null;
			}

			// Если успешно сохранены настройки, редирект на главную сраничку настроек сайтов
			if($model->save())
				return $this->redirect(['config/site', 'id' => $model->id]);
			else
			{
				echo '<pre>';
				print_r($model);
				echo '</pre>';
				exit;
			}
		}
		else
		{
			return $this->render('config-site', [
				'model' => $model,
				'allSite' => ConfigSite::findAll(['user_id' => Yii::$app->user->identity->id]),
				'categories' => Main::getCategoriesList(),
				'alert' => @$alert,
				'select' => $this->getSelect(),
			]);
		}
	}

	/**
	 * Deletes an existing ConfigSite model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionSiteDelete($id)
	{
		$this->findModelSite($id)->delete();
		return $this->redirect(['config/site']);
	}

	/**
	 * Finds the ConfigSite model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return ConfigSite the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModelSite($id)
	{
		if(($model = ConfigSite::findOne($id)) !== null)
			return $model;
		else
			throw new NotFoundHttpException('The requested page does not exist.');
	}


	public function actionScanCategory($id = false)
	{
		Main::init(Yii::$app->user->identity->id); // Инициация пользователя
		$tubes = Main::getTubes(); // Инициация пользователя
		echo Yii::$app->session->getFlash('alert');

		foreach($tubes as $tube)
		{
			if($get = Yii::$app->cache->get($tube->id.'-categories'))
				$check = count($get);
			else
				$check = 0;
			echo "<li><a href='/category/scan-category?id=$tube->id'>$tube->name - $check</a></li>";
		}

		if($id)
		{
				$parser = new Parser;
				$parser->configTube = Main::getTubes($id);
				if($categories = $parser->getCategoriesTube())
				{
					// $categories[] = [
					// 	'title' => 'Дефекация / Scat',
					// 	'category' => 1681,
					// 	'url' => 'http://pornolab.net/forum/viewforum.php?f=1681',
					// ];

					Yii::$app->cache->set($id.'-categories', $categories);
					Yii::$app->session->setFlash('alert', 'Успешно отсканировано!');
					$this->redirect('/category/scan-category');
				}

			// Yii::$app->cache->set($tubes[$cat]->id.'-categories', $categories);
			// echo '<pre>';
			// print_r($categories);
			// echo '</pre>';
		}
	}
}