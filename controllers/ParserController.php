<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\Controller;
use app\models\Archive;
use app\models\Options;
use app\models\ConfigSite;
use app\models\Torrents;
use app\models\Parser\Parser;
use app\models\Parser\Publication;
use app\models\Parser\Main;
use app\models\Parser\Index;
use app\models\Parser\Handler;
use app\models\Parser\TorrentClient;

use DiDom\Document;
use Sunra\PhpSimple\HtmlDomParser;

date_default_timezone_set('Europe/Kiev');

class ParserController extends Controller
{	
	/**
	* @var array
	* Сообщения об ошибках и прочее
	*/
	public $alert;
	public $config;
	public $user;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['test'],
						'roles' => ['?'],
					],
					[
						'allow' => true,
						'actions' => ['index', 'download-file', 'open-file', 'check', 'get-chat-id', 'log'],
						'roles' => ['@'],
					],
					[
						'allow' => true,
						'actions' => ['check-proxy', 'delete-file'],
						'roles' => ['user'],
					],
					[
						'allow' => true,
						'actions' => ['open-log', 'publication', 'clear', 'test', 'category'],
						'roles' => ['user-pay'],
					],
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}


	public function actionCategory($id = false)
	{
		Main::init(Yii::$app->user->identity->id); // Инициация пользователя
		$tubes = Main::getTubes(); // Инициация пользователя
		echo Yii::$app->session->getFlash('alert');

		foreach($tubes as $tube)
		{
			if($get = Yii::$app->cache->get($tube->id.'-categories'))
				$check = count($get);
			else
				$check = 0;
			echo "<li><a href='/parser/category?id=$tube->id'>$tube->name - $check</a></li>";
		}

		if($id)
		{
				$parser = new Parser;
				$parser->configTube = Main::getTubes($id);
				if($categories = $parser->getCategoriesTube())
				{
					// $categories[] = [
					// 	'title' => 'Дефекация / Scat',
					// 	'category' => 1681,
					// 	'url' => 'http://pornolab.net/forum/viewforum.php?f=1681',
					// ];

					Yii::$app->cache->set($id.'-categories', $categories);
					Yii::$app->session->setFlash('alert', 'Успешно отсканировано!');
					$this->redirect('/parser/category');
				}

			// Yii::$app->cache->set($tubes[$cat]->id.'-categories', $categories);
			// echo '<pre>';
			// print_r($categories);
			// echo '</pre>';
		}
	}

    public function actionLog($cat)
    {
		Main::init(Yii::$app->user->id); // Инициация пользователя
		$path = Main::getTemp()->temp.Yii::$app->user->id.'_parser_'.$cat.'.log';
		@$content = file_get_contents($path);
    	echo '<pre>';
    	print_r(@$content);
    	echo '</pre>';
    }

    public function actionIndex($start = false, $stop = false)
    {
		Main::init(Yii::$app->user->id); // Инициация пользователя
		
		if($start)
		{
			shell_exec('php '.dirname(__DIR__).'/yii console/daemon '.Yii::$app->user->id.' parser "'.$start.'"  > /dev/null 2>&1 &');
			$this->redirect('/parser');
		}

    	$categories = $this->checkStatus($stop);


		// Инфа для админа
		if(Yii::$app->user->can('user-pay'))
		{
			$freeSize = disk_free_space(__DIR__);
			$info[] = 'Вчера добавлено: '.Main::getCountAdd(date('Y-m-d', time()-(60*60*24)));
			$info[] = 'Сегодня добавлено: '.Main::getCountAdd(date('Y-m-d'));
			$info[] = 'Свободно места: '.Main::size($freeSize);
			if($freeSize < 1024*1024*1024*4)
				$button['Удалить файлы'] = '/parser/clear';
		}

		return $this->render('parser', [
			'alert' => @$this->alert,
			'info' => @$info,
			'categories' => $categories,
		]);
    }

    private function checkStatus($processKill = false)
    {
        if(file_exists(dirname(__DIR__).'/logs/errors.log'))
        {
             $serviceActive['errors'] = file_get_contents(dirname(__DIR__).'/logs/errors.log');
        }

		// Список категорий
		$categories = Main::getCountArhive(); // Список категорий

        $exec = shell_exec('ps -aux');
        $expplode = explode("\n", $exec);

		// Список категорий
		foreach($categories as $key => $category)
		{
			$categories[$key]['status'] = false;

			// Перебор списка процессов построчно
            foreach($expplode as $processString)
            {
            	// Если найден процесс
                if(preg_match('/daemon.+'.$category['name'].'/', $processString, $result))
                {
                	// Если найден ИД
                    if(preg_match('/^\w+\s+(\d+)\s+/', $processString, $result))
                    {
                        $categories[$key]['status'] = $result[1];
                    	if($processKill && strcasecmp($processKill, $category['name']) == 0)
                    	{
							shell_exec('kill '.$categories[$key]['status']);
							$this->redirect('/parser');
                    	}
                    }
                }
            }
        }

        return $categories;
    }


	public function actionIndex1()
	{
		// $all = Archive::findAll(['user_id' => Yii::$app->user->id, 'statusPublicationSite' => null, 'status' => 4]);
		// foreach ($all as $key => $value) {
		// 	echo '<pre>';
		// 	print_r(unserialize($value->poster));
		// 	echo '</pre>';
		// }
		// exit();

		Main::init(Yii::$app->user->id); // Инициация пользователя
		$path = Main::getTemp()->main;

		// // exec('ls -a /', $res);
		// exec('sudo -u root -S {{ service transmission-daemon stop }} < /.sudopass', $res);
		// echo '<pre>';
		// print_r($res);
		// echo '</pre>';
		// exit();
		exec('service --status-all | grep transmission-daemon', $res);
		if(preg_match('/ \+ /', $res[0]))
			$serviceStatus['transmission'] = 1;
		else
			$serviceStatus['transmission'] = false;

		unset($process_id);
		$parser_log = $path.Yii::$app->user->id.'-parser.log';
        if(file_exists($parser_log))
        {
            $fp = fopen($parser_log, 'r');
            $process_id = fgets($fp);
            // $process_id = (integer) $process_id-1;
            $res = shell_exec('ps -fp '.$process_id);
            if(preg_match('/yii/', $res))
            {
                $serviceStatus['parser'] = $process_id;
            }
            else
            	$serviceStatus['parser'] = false;
        }

		unset($process_id);
		$publication_log = $path.Yii::$app->user->id.'-publicationForums.log';
        if(file_exists($publication_log))
        {
            $fp = fopen($publication_log, 'r');
            $process_id = fgets($fp);
            // $process_id = (integer) $process_id-1;
            $res = shell_exec('ps -fp '.$process_id);
            if(preg_match('/yii/', $res))
                $serviceStatus['publication'] = $process_id;
            else
                $serviceStatus['publication'] = false;
        }

		// $error_log = dirname(dirname(__FILE__)).'/logs/'.Yii::$app->user->id.'-errors.log';
		$error_log = $path.Yii::$app->user->id.'-errors.log';
		if(@$command = Yii::$app->request->get()['command'])
		{
			if(Yii::$app->user->can('user-pay'))
			{
				if($command == 'parser-stop')
				{
					shell_exec('kill '.$serviceStatus['parser']);
				}
				elseif($command == 'parser-start')
				{
					if($clearFiles = Main::clearFiles(Main::getTemp()->downloads))
					{
						// Main::echoTxt($clearFiles);
					}

					$torrent = new TorrentClient;
					if($clearTorrent = $torrent->clear())
					{
						// Main::echoTxt($clearTorrent);
					}
					// Удаление файла логов ошибок
					@unlink($error_log);
					shell_exec('php '.dirname(__DIR__).'/yii parser '.Yii::$app->user->id.' > /dev/null 2>&1 &');
				}
				elseif($command == 'transmission-start') // transmission restart
				{
					// Удаление файла логов ошибок
					@unlink($error_log);
					shell_exec('service transmission-daemon start');
				}
				elseif($command == 'transmission-stop') // transmission restart
				{
					// Удаление файла логов ошибок
					@unlink($error_log);
					exec('service stop transmission-daemon', $res);
				}
				elseif($command == 'publication-stop')
				{
					shell_exec('kill '.$serviceStatus['publication']);
				}
				elseif($command == 'publication-start')
				{
					// Удаление файла логов ошибок
					@unlink($error_log);
					shell_exec('php '.dirname(__DIR__).'/yii parser '.Yii::$app->user->id.' publicationForums > /dev/null 2>&1 &');
				}

				sleep(1);
				$this->redirect('/parser1');
			}
			else
			{
				$info[] = 'У вас недостаточно прав';
			}
		}

		// Список категорий
		$categories = Main::getCategoriesUser(); // Список категорий
		// Список категорий
		foreach($categories as $category)
			$categoriesLight[] = $category->title;

		// Список текстовых файлов
		// $filesTxt = Main::getTxtFilesList();

		// $info[] = 'Вами скачано роликов: '.Main::getTotalCountPosts() .'/'.Main::getTotalCountPostsPay() .' Осталось: '. Main::getHavePosts();


		// Инфа для админа
		if(Yii::$app->user->can('user-pay'))
		{
			$freeSize = disk_free_space(__DIR__);
			$info[] = 'Вчера добавлено: '.Main::getCountAdd(date('Y-m-d', time()-(60*60*24)));
			$info[] = 'Сегодня добавлено: '.Main::getCountAdd(date('Y-m-d'));
			$info[] = 'Свободно места: '.Main::size($freeSize);
			if($freeSize < 1024*1024*1024*4)
				$button['Удалить файлы'] = '/parser/clear';
		}

		return $this->render('parser', [
			'info' => @$info,
			'categories' => $categoriesLight,
			// 'categories' => $categories,
			'countArchive' => Main::getCountArhive(),
			'filesTxt' => @$filesTxt,
			'complete' => @$complete,
			'serviceStatus' => $serviceStatus,
			'button' => @$button,
			'parser_log' => @file_get_contents($parser_log),
			'error_log' => @file_get_contents($error_log),
			'worker_log' => @file(Main::getTemp()->temp.'log.txt'),
			'alert' => @$this->alert,
		]);
	}

	public function actionGetLog($file)
	{
		$text = file_get_contents($file);
		return array_reverse(explode(PHP_EOL, $text));
	}

	public function actionPublication()
	{
		Main::init(Yii::$app->user->id); // Инициация пользователя

		$publication_log = dirname(__DIR__).'/logs/'.Yii::$app->user->id.'-publicationForums.log';
        if(file_exists($publication_log))
        {
            $fp = fopen($publication_log, 'r');
            $process_id = fgets($fp);
            // $process_id = (integer) $process_id-1;
            $res = shell_exec('ps -fp '.$process_id);
            if(preg_match('/yii/', $res))
                $publicationActive = 1;
        }

		// $error_log = dirname(dirname(__FILE__)).'/logs/'.Yii::$app->user->id.'-errors.log';
		$error_log = dirname(dirname(__FILE__)).'/logs/errors.log';
		if(@$command = Yii::$app->request->get()['command'])
		{
			if(Yii::$app->user->can('user-pay'))
			{
				if($command == 'stop')
				{
					shell_exec('kill '.$process_id);
				}
				elseif($command == 'start')
				{
					// Удаление файла логов ошибок
					@unlink($error_log);
					shell_exec('php '.dirname(__DIR__).'/yii parser '.Yii::$app->user->id.' publicationForums > /dev/null 2>&1 &');
				}

				sleep(1);
				$this->redirect('/parser/publication');
			}
			else
			{
				$info[] = 'У вас недостаточно прав';
			}
		}

		if(Yii::$app->request->post())
		{
			// Удаление файла логов ошибок
			$error_log = dirname(dirname(__FILE__)).'/logs/'.Yii::$app->user->id.'-errors-publication.log';
			@unlink($error_log);

			exec('php '.dirname(__DIR__).'/yii publication '.Yii::$app->user->id.' > /dev/null 2>&1 &');
			sleep(1);
			$this->redirect('/publication');
		}

		if(!$info = Yii::$app->cache->get('posts-'.Yii::$app->user->id));
			$info = [];

		if(Yii::$app->request->post())
		{

		}

		return $this->render('publication', [
			'info' => $info,
			'error_log' => @file_get_contents($error_log),
			'publication_log' => @file_get_contents($publication_log),
			'publicationActive' => @$publicationActive,
		]);
	}

	public function actionPublication2($cat = false)
	{
		Main::init(Yii::$app->user->identity->id); // Инициация пользователя
		Main::clearFiles(Main::getTemp()->publication);
		// Перебираем все категории
		foreach(Main::getCategoriesUser() as $category)
		{
			if($cat !== false && strtolower($cat) != strtolower($category->title))
			{
				continue;
			}

			$category = (object) $category;
			Main::echoTxt($category->title);
			if(count($item = Archive::findOne(['user_id' => Yii::$app->user->identity->id, 'category' => $category->title, 'status' => 4])) > 0)
			{
				Main::echoTxt(unserialize($item['info'])['title']);
				if(count($sites = ConfigSite::findAll(['user_id' => Yii::$app->user->identity->id])) > 0)
				{
					$data = (object) [];
					$poster = unserialize($item->poster);
					$screenshot = unserialize($item->screenshot);
					$data->links = unserialize($item->links);
					$data->meta = unserialize($item->meta);
					$data->info = unserialize($item->info);
					$result['counts'][$category->title] = 0;

					// Перебираем все сайты
					foreach($sites as $site)
					{
						try
						{
							// Если нет ниодной ветки то пропускаем
							// if($site->enegine != 'phpbb3')
							// 	continue;

							// Main::echoTxt($site->site);
							if(@$site->stataus != 1)
							{
								// Main::echoTxt($site->site.' - Сайт выключен');
								continue;
							}

							// Если dle и статус 2
							if($site->enegine == 'dle')
							{
								continue;
							}
							if($site->id != 30)
							{
								continue;
							}

							// Если нет ниодной ветки то пропускаем
							// if($site->enegine == 'vbulletin' || $site->enegine == 'phpbb' || $site->enegine == 'phpbb3' || $site->enegine == 'smf' || $site->enegine == 'xenforo')
							// {
								if($site->enegine != 'dle')
								{
									// Список добавленных веток
									if(is_array($categories1 = unserialize($site->categories)))
									{
										unset($categories);
										foreach($categories1 as $key => $cat)
										{
											$categories[] = $cat;
										}

										// Поиск нужно ветки
										if(($key = array_search($category->title, array_column($categories, 'categoryTitle'))) === false)
										{
											Main::echoTxt($site->site.' - Нет нужной ветки');
											continue;
										}
										if(!isset($categories[$key]))
										{
											echo 'Нет такой категории<pre>';
											print_r($key);
											print_r($categories[$key]);
											print_r($category);
											print_r($categories);
											echo '</pre>';
											continue;
										}
										$site->categories = $categories[$key]['threadId'];
									}
									else
									{
										Main::echoTxt($site->site.' - Нет добавленных веток');
										continue;
									}
								}
							// }

							$data->posterLinks = $poster[$site->hostingPoster];
							$data->screenshotLinks = $screenshot[$site->hostingScreenshot];
							Main::echoTxt(Publication::addPost($site, $data));
							$result['counts'][$category->title]++;
						}
						catch(Exception $e)
						{
							Main::echoTxt('Исключение контроллера: '.$e->getMessage().PHP_EOL);
						}
						// exit;
					}

					// Обновляем пост как опубликованный
					if($result['counts'][$category->title] > 0)
					{
						$item->status = 5;
						if($item->save())
							Main::echoTxt('Update item '.$item->id);
						else
							Main::echoTxt('No update item '.$item->id);
					}
				}
				else
					Main::echoTxt('Нет сайтов для публикации');
			}
			else
				Main::echoTxt('Нет постов для публикации');

			Main::echoTxt('<hr>', ['disableBR' => 1]);
		}
		Main::echoTxt('Все');
		echo '<pre>';
		print_r(@$result['counts']);
		echo '</pre>';
		// exit;
	}


	public function actionClear($filePath = false)
	{
		Main::init(Yii::$app->user->identity->id); // Инициация пользователя

		if($filePath)
		{
			if($txt = Main::clearFiles($filePath, true))
				Main::echoTxt($txt);
		}
		else
		{
			if($txt = Main::clearFiles(Main::getTemp()->downloads))
				Main::echoTxt($txt);
		}
	}


	// Проверяет прокси
	public function actionCheckProxy($proxy)
	{
		if(Parser::checkProxy($proxy))
		{
			return '<span class="glyphicon glyphicon-ok-circle alert alert-success"></span>';
		}
		return '<span class="glyphicon glyphicon-remove-circle alert alert-danger"></span>';
	}


	public function actionOpenFile($file)
	{
		echo @file_get_contents($file);
	}


	// Удаляет текстовый файл
	public function actionDeleteFile($fileName)
	{
		if(Main::deleteTxtFile($fileName))
			return 'Файл '.$fileName.' удален';
	}


	// Скачивает текстовый файл
	public function actionDownloadFile($fileName)
	{
		Main::setUser(Yii::$app->user->identity->id); // Инициация пользователя
		Main::downloadTxtFile($fileName);
	}


	// Скачивает текстовый файл
	public function actionOpenLog($f)
	{
		$path = dirname(__DIR__).'/logs/'.$f;
		if(file_exists($path))
		{
			echo '<pre>';
			echo file_get_contents($path);
			echo '</pre>';
		}
		else
			echo 'Файла не существует - '.$f;
	}

	function actionCheck()
	{
		$publication_log = dirname(__DIR__).'/logs/7-publicationForums.log';
		$fopen = file_get_contents($publication_log, false, null, 0, 1024*1024);
		echo '<pre>';
		print_r($fopen);
		echo '</pre>';
		exit();
	}


	public function actionTest1()
	{
		$page = 'http://pornolab.net/forum/viewtopic.php?t=2079140';
		$page = 'http://pornolab.net/forum/./viewtopic.php?t=1157967';
		$page = 'http://pornolab.net/forum/./viewtopic.php?t=2157605';
		$page = 'http://pornolab.net/forum/viewtopic.php?t=1515714';
		$page = 'http://pornolab.net/forum/./viewtopic.php?t=1357011';
		$page = 'http://pornolab.net/forum/./viewtopic.php?t=1523725';
		$parser = new Parser;
		$html = $parser->getPageFromCurl($page);
		echo $text = $html->find('.post_body', 0)->plaintext;
		echo '<br>';
		echo '<br>';

		// $res = preg_match("/(?:Модель|Имя актрисы|В ролях|Имя модели)\s*:?\s*([^а-яА-Я]*)/", $text);
		preg_match("/([^а-яА-Я]*)\s*/", $text, $res);
		echo $res[1];
		echo '<br>';
		echo '<br>';
		echo $parser->removeStr($res[1], '/([0-9.,]+)/');
		echo '<pre>';
		// print_r($res);
		echo '</pre>';
		exit();
	}

	public function actionGetChatId()
	{
		$res = json_decode(file_get_contents('https://api.telegram.org/bot455390467:AAH8TcXE16eHBMmm9fMj5hwqD9CcgV36E9Y/getUpdates'));
		echo '<pre>';
		print_r($res);
		echo '</pre>';
		exit();
	}

	public function actionTest2()
	{
		$file = '/home/admin/web/boomuo.example.com/public_html/temp/demo/txt/Anal.txt';

		$login = 'boomuo@gmail.com';
		$pass = '19890202g';
		$api = new \app\models\Parser\Nitroflare($login, $pass);
		echo $res = $api->uploadFile($file);


		// $login = 'boomuo@gmail.com';
		// $pass = '890202';
		// $api = new \app\models\Parser\Turbobit($login, $pass);
		// echo $res = $api->uploadFile($file);

		// $login = 'boomuo@gmail.com';
		// $pass = '890202g';
		// $api = new \app\models\Parser\Rapidgator($login, $pass);
		// echo $res = $api->uploadFile($file);


		// $login = 'boomuo@gmail.com';
		// $pass = '890202g';
		// $login = '1539511';
		// $api = new \app\models\Parser\DepFileApi($login, $pass);
		// echo $res = $api->uploadFile($file);


		// echo '<pre>';
		// print_r($res);
		// echo '</pre>';
	}
}