<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Archive;
use app\models\ConfigSite;
use app\models\Torrents;
use app\models\Parser\Parser;
use app\models\Parser\TorrentClient;
use app\models\Parser\Publication;
use app\models\Parser\Main;

// header( 'Content-Type: text/html; charset=utf-8' );
date_default_timezone_set('Europe/Kiev');

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ConsoleController extends Controller
{
	private $currentJobs = array(); 

	static $userId; 

    /**
     * Демон. Запускает методы в вечные циклы
     * @param int $userId
     * @param str $methd
     */
	public function actionDaemon($userId = false, $method = 'parser', $param = false)
	{		
		if(!$userId)
		{
			echo 'Нужно указать ID пользователя'.PHP_EOL;
			exit;
		}

		// Создаем дочерний процесс весь код после pcntl_fork() будет выполняться двумя процессами: родительским и дочерним
		$pid = pcntl_fork();
		if($pid == -1)
		{
		    // Не удалось создать дочерний процесс
		    error_log('Could not launch new job, exiting');
		    return FALSE;
		} 
		elseif($pid)
		{
		    // Этот код выполнится родительским процессом
		    $this->currentJobs[$pid] = $method;
		} 
		else
		{ 
		    // А этот код выполнится дочерним процессом
		    echo (getmypid()-1).PHP_EOL;
		    exit(); 
		}

		Main::init($userId); // Инициация пользователя
		self::$userId = $userId; // Инициация пользователя
		Main::$consoleMode = 1; // Включен режим вывода сообщений для консоли [1-вывод, 2-нет]
		$path = Main::getTemp()->temp;
		// $path = dirname(__DIR__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;

		// @unlink($path.'errors.log');
		ini_set('error_log', $path.$userId.'-errors.log');
		if(is_resource(STDIN))
		{
			fclose(STDIN);
			$STDIN = fopen('/dev/null', 'r');
		}
		if(is_resource(STDOUT))
		{
			fclose(STDOUT);
			if($param)
				$STDOUT = fopen($path.$userId.'_'.$method.'_'.$param.'.log', 'w');
			else
				$STDOUT = fopen($path.$userId.'_'.$method.'.log', 'w');
		}
		echo getmypid().PHP_EOL;

		while(true)
		{
			$this->{$method}($param);
		}
	}

	public function actionParser($userId = false, $category = false)
	{		
		if(!$userId)
		{
			echo 'Нужно указать ID пользователя'.PHP_EOL;
			exit;
		}

		Main::init($userId); // Инициация пользователя
		self::$userId = $userId; // Инициация пользователя
		Main::$consoleMode = 1; // Включен режим вывода сообщений для консоли [1-вывод, 2-нет]
		$path = Main::getTemp()->logs;

		@unlink($path.'errors.log');

		$this->parser($category);
	}

	private function parser($category)
	{
		$category = Main::getCategoriesUser($category); // Список категорий и их источников из настроек

		// Main::clearFiles(Main::getTemp()->downloads);
		// if($category->title == 'Lab Rus' || $category->title == 'Lab SD' || $category->title == 'Lab HD')
		// 	return false;
		if(!isset($category->tubes))
		{
			return false;
		}

		// $countPosts = Archive::findAll(['category' => $category->title, 'status' => 4, 'user_id' => self::$userId]);
		// if(count($countPosts) >= 10)
		// 	return false;

		$timeStart = time();
		$parser = new Parser;
		$parser->category = $category->title; // Название выбраной категории
		$parser->categories = $category->tubes; // Список источников для выбранной категории
		// $parser->count = $count; // Колличество сколько роликов парсить
		$parser->userId = self::$userId;
		// $parser->proxy = $this->config->proxy;

		Main::echoTxt('############################################################', ['br' => 1]);
		Main::echoTxt('Парсинг категории: '.$parser->category);
		// Main::echoTxt('Необходимое колличество: '.$parser->count);
		
		// Находим ролик
		if($item = $parser->getItem())
		{
			Main::echoTxt('Новость: '.$item['url']);
			Main::echoTxt('Скачивание файла...');
			if($file = $parser->downloadFile($item))
			{
				$item['filePath'] = $file['filePath'];

				if($file['mime'] == 'html')
				{
					Main::echoTxt('Скачан html файл вместо видео или торента');
					$accounts = Yii::$app->cache->get('accounts-'.date('Y:m:d'));
					$accounts[] = $parser->login;
					Yii::$app->cache->set('accounts-'.date('Y:m:d'), $accounts);
					// unlink($file['filePath']);
					$parser->account = false;
					return false;
				}
				// Проверяем если файт - торент
				elseif($file['mime'] == 'x-bittorrent')
				{
					// Скачивание торрент файла
					$torrentClient = new TorrentClient();

					// Добавляем торрент на скачивание
					$torrentFileAdd = $torrentClient->addTorrent($item['filePath'], false, $item['folder']);
					// Получаем ID торрента
					$torrentId = $torrentFileAdd->getId();
					// Получаем всю инфу о торренте
					$torrentInfo = $torrentClient->getTorrent($torrentId);
					// Получаем список файлов
					$files = $torrentInfo->getFiles();

					$maxSizeGb = 3; // Максимальный размер торрента ГБ
					if(($size = $torrentInfo->getSize()) > 1024*1024*1024*$maxSizeGb)
					{
						Main::echoTxt('Слишком большой торрент: '.round($size/1024/1024/1024, 2).'/'.$maxSizeGb.' Гб');
						$torrentInfo->remove();
						Main::clearFiles($item['folder'], true);
						foreach($files as $file)
						{
							@unlink($torrentClient->getFolder().$file->getName());
							@unlink($torrentClient->getFolder().$file->getName().'.part');
						}
						Main::save($item, 6);
						return false;
					}
					
					// Получаем файл торрента
					if(($countFiles = count($files)) > 1)
					{
						$maxFiles = 4;
						Main::echoTxt('В торенте много файлов: '.$countFiles.'. Макс: '.$maxFiles);
						if($countFiles < $maxFiles)
						{
							foreach($files as $key => $itemFile)
							{
							    $sizeItems[$itemFile->getSize()] = $itemFile;
							}
							krsort($sizeItems);
							$first = array_shift($sizeItems);
							$item['download']['video'] = htmlspecialchars($torrentClient->getFolder().$first->getName());
							Main::echoTxt('Качаю все файлы. Выбираю самый крупный');
						}
						else
						{
							Main::echoTxt('Пропускаю');
							$torrentInfo->remove();
							Main::clearFiles($item['folder'], true);
							foreach($files as $file)
							{
								@unlink($torrentClient->getFolder().$file->getName());
								@unlink($torrentClient->getFolder().$file->getName().'.part');
							}
							Main::save($item, 7);
							return false;
						}
					}
					else
						$item['download']['video'] = $torrentClient->getFolder().$files[0]->getName();


					// Ждем пока скачается
					$sleep = 10; // 10 секунд интервал между проверками
					$waitPeers = time()+(60*15); // 15 минут ожидание пиров
					$downloadTime = time()+(60*10); // Максимальное времея на скачивание торрента
					$downloadPercent = 50; // Процент при проверке
					Main::echoTxt('Скачивание файла с торрента: "'.basename($item['download']['video']).'". Размер: '.Main::size($size));
					while(!$torrentClient->getTorrent($torrentId)->isFinished())
					{
						if(time() > $downloadTime)
						{
							$torrentInfo = $torrentClient->getTorrent($torrentId);
							if($torrentInfo->getPercentDone() < $downloadPercent)
							{
								Main::echoTxt('Время на скачивание файла вышло (10мин). Файл не скачан. Пиров: '.count($torrentInfo->getPeers()).'. Скачано '.$torrentInfo->getPercentDone().'%');
								$torrentInfo->remove();
								// Main::clearFiles($item['folder'], true);
								foreach($files as $file)
								{
									@unlink($torrentClient->getFolder().$file->getName());
									@unlink($torrentClient->getFolder().$file->getName().'.part');
								}
								// Main::save($item, 8);
								return false;
							}
						}

						// Ждем время и проверяем наличие пиров
						if(time() > $waitPeers)
						{
							$torrentInfo = $torrentClient->getTorrent($torrentId);
							Main::echoTxt('Ждал 10 минут. Пиров: '.count($torrentInfo->getPeers()).' Скачано: '.$torrentInfo->getPercentDone().'%');
							if(count($torrentInfo->getPeers()) < 1)
							{
								// print_r($torrentInfo);
								sleep($sleep);
								if(count($torrentInfo->getPeers()) < 1)
								{
									Main::echoTxt('Нет пиров. Пропускаю.');
									$torrentInfo->remove();
									Main::clearFiles($item['folder'], true);
									foreach($files as $file)
									{
										@unlink($torrentClient->getFolder().$file->getName());
										@unlink($torrentClient->getFolder().$file->getName().'.part');
									}
									// Main::save($item, 8);
									return false;
								}
							}
							elseif(@$step == 2)
							{
								Main::echoTxt('Файл не скачан. Пропускаю');
								$torrentInfo->remove();
								// Main::clearFiles($item['folder'], true);
								foreach($files as $file)
								{
									@unlink($torrentClient->getFolder().$file->getName());
									@unlink($torrentClient->getFolder().$file->getName().'.part');
								}
								return false;
							}
							Main::echoTxt('Продолжаю скачивать');
							$waitPeers += 60*10; // 10 минут
							$step = 2;
						}
						sleep($sleep);
					}
				}
				else
				{
					$item['download']['video'] = $item['filePath'];
				}

				// Проверка наличия файла
				if(!file_exists($item['download']['video']))
				{
					Main::echoTxt('Файл не существует!');
					// Удаляем торрент с торрент-демона
					if(isset($torrentInfo))
						$torrentInfo->remove();
					return false;
				}

				// Обрабатываем файл
				Main::foreachItem($item);

				// Удаляем торрент с торрент-демона
				if($file['mime'] == 'x-bittorrent')
				{
					if(isset($torrentInfo))
						$torrentInfo->remove();
				}

				// Публикация
				if(Main::getConfig()->nowPublication > 0)
				{
					Main::echoTxt('Публикация на сайты...');
					$this->actionPublication(self::$userId, $category->title, 'site');
				}
			}
			else
			{
				Main::echoTxt('Ролик не скачан');
			}
		}
		else
		{
			Main::echoTxt('Ролик не найден');
		}
		sleep(10);
	}

	//  Only [0-all, 1-forums, 2-sites]
	public function actionPublication($userId = false, $cat = false, $only = 'forum')
	{
		if(!$userId)
		{
			echo 'Нужно указать ID пользователя'.PHP_EOL;
			exit;
		}
		Main::init($userId); // Инициация пользователя
		Main::$consoleMode = 1; // Включен режим вывода сообщений для консоли
		// Main::clearFiles(Main::getTemp()->publication); // Очистка html файлов-отчетов

		if($cat == '-')
		{
			Main::echoTxt('Публикуем все категории по очереди');
			$categories = Main::getCategoriesUser(); // Список всех категорий
		}
		elseif(!empty($cat))
			$categories[] = Main::getCategoriesUser($cat); // Категория
		else
		{
			Main::echoTxt('Не указана категория');
			return false;
		}

		// Перебираем все категории
		foreach($categories as $key => $category)
		{
			// Получение новости
			if($only == 'forum')
			{
				$item = Archive::findOne([
					'user_id' => Main::$user->id, 
					'category' => strtolower($category->title), 
					'status' => 4,
				]);

				$countItems = count((array) $item);
				// Если нет новости
				if($countItems > 0)
					$item->status = 5;

				// Получение списка сайтов/форумов
				$query = "SELECT * FROM `config_site` WHERE `user_id` = ".Main::$user->id." AND `enegine` != 'dle'";
				$sites = ConfigSite::findBySql($query)->all();
			}
			elseif($only == 'site')
			{
				$item = Archive::findOne([
					'user_id' => Main::$user->id, 
					'category' => strtolower($category->title), 
					'status' => 4,
					'statusPublicationSite' => null,
				]);

				$countItems = count((array) $item);
				// Если нет новости
				if($countItems > 0)
					$item->statusPublicationSite = 1;

				// Получение списка сайтов/форумов
				$sites = ConfigSite::findAll(['user_id' => Main::$user->id, 'enegine' => 'dle']);
			}

			// Если нет новости
			if($item != null)
			{
				Main::echoTxt($category->title);
				// Main::echoTxt(unserialize($item['info'])['title']);
				$countSites = count((array) $sites);
				// Если есть сайты/форумы
				if($countSites > 0)
				{
					$data = (object) [];
					$poster = unserialize($item->poster);
					$screenshot = unserialize($item->screenshot);
					$data->links = unserialize($item->links);
					$data->meta = unserialize($item->meta);
					$data->info = unserialize($item->info);
					$result['counts'][$category->title] = 0;

					// Перебираем все сайты
					foreach($sites as $site)
					{
						try
						{
							// Если нет ниодной ветки то пропускаем
							// if($site->enegine != 'phpbb3')
							// 	continue;

							// if($site->site != 'https://pornturbobit.net')
							// 	continue;

							// Если сайт/форум выключен
							if(@$site->stataus != 1)
								continue;
							
							Main::echoTxt('', ['br' => 1]);

							// Список добавленных веток на сайте
							if(is_array($categories1 = unserialize($site->categories)))
							{
								unset($categories);
								foreach($categories1 as $key => $cat1)
								{
									$categories[] = $cat1;
								}

								// Поиск нужно ветки
								if(($key = array_search($category->title, array_column($categories, 'categoryTitle'))) === false)
								{
									Main::echoTxt($site->site.' - Нет нужной ветки');
									continue;
								}
								if(!isset($categories[$key]))
								{
									echo 'Нет такой категории<pre>';
									print_r($categories[$key]);
									print_r($category);
									print_r($categories);
									continue;
								}
								$site->categories = $categories[$key]['threadId'];
							}
							else
							{
								Main::echoTxt($site->site.' - Нет добавленных веток');
								continue;
							}

							if(!array_key_exists($site->hostingPoster, $poster))
							{
								Main::echoTxt($site->site.': Нет нужного постера - '.$site->hostingPoster);

								// if($item->save())
								// 	Main::echoTxt('Update item '.$item->id);
								// else
								// 	Main::echoTxt('No update item '.$item->id);
								continue;
							}
							if(!array_key_exists($site->hostingScreenshot, $poster))
							{
								Main::echoTxt($site->site.': Нет нужного скриншота - '.$site->hostingScreenshot);
								continue;
							}

							$data->posterLinks = @$poster[$site->hostingPoster];
							$data->screenshotLinks = @$screenshot[$site->hostingScreenshot];
							$probe = 1;
							do
							{
								if(Publication::addPost($site, $data))
									break;
								else
								{
									Main::echoTxt('Попытка №: '.$probe);
									$probe++;
								}
							}
							while($probe < 4);
							$result['counts'][$category->title]++;
						}
						catch(Exception $e)
						{
							Main::echoTxt('Исключение контроллера: '.$e->getMessage().PHP_EOL);
						}
					}

					// Обновляем пост как опубликованный
					if($result['counts'][$category->title] > 0)
					{
						if($item->save())
							Main::echoTxt('Update item '.$item->id);
						else
							Main::echoTxt('No update item '.$item->id);
					}
				}
				else
					Main::echoTxt('Нет '.$only.' для публикации');
			}
		}

		@$res = Yii::$app->cache->get('posts-'.$userId);
		@$res[date('Y/m/d')][] = [
			'result' => @$result['counts'],
			'complete-time' => date('Y/m/d H:i'),
		];

		Yii::$app->cache->set('posts-'.$userId, $res);
		// print_r(@$res);
		// print_r(@$result);
	}

	//  Only [0-all, 1-forums, 2-sites]
	public function publicationForums()
	{
		// Время ожидания
		$waitTime = time() + (60*60*24) / 10;


		// Main::clearFiles(Main::getTemp()->publication); // Очистка html файлов-отчетов
		$categories = Main::getCategoriesForumsUser(); // Список категорий

		// Перебираем все категории
		foreach($categories as$category)
		{
			Main::echoTxt($category->name);
			Main::echoTxt('############################################################', ['br' => 1]);

			// Получение новостей
			$items = Archive::findAll([
				'user_id' => self::$userId, 
				'status' => 4,
				'statusPublicationSite' => 1,
			]);

			// Поиск нужной новости по ключу
			foreach($items as $key => $itemOne)
			{
				$keys = explode(',', $category->keys);
				foreach($keys as $key)
				{
					$info = unserialize($itemOne->info);

					// Поиск в тегах
					$tags = $info['tags'];
					if(isset($tags) && !empty($tags) && is_array($tags))
					{
						foreach($tags as $tag)
						{
							if(preg_match('/'.trim($key).'/i', $tag))
							{
								$item = $itemOne;
								break 3;
							}
						}
					}

					// Поиск в заголовке
					if(preg_match('/'.trim($key).'/i', @$info['title']))
					{
						$item = $itemOne;
						break 2;
					}

					// Поиск в описании
					// if(preg_match('/'.trim($key).'/i', @$info['description']))
					// {
					// 	$item = $itemOne;
					// 	break 2;
					// }
				}
			}

			// Если нет новости
			if(!@$item)
			{
				Main::echoTxt('Нк найдено подходящей новости');
				continue;
			}

			// Получение списка форумов
			$query = "SELECT * FROM `config_site` WHERE `user_id` = ".self::$userId." AND `enegine` != 'dle'";
			$sites = ConfigSite::findBySql($query)->all();

			$countItems = count((array) $item);
			// Если нет новости
			if($countItems > 0)
			{
				// Main::echoTxt(unserialize($item['info'])['title']);
				
				$countSites = count((array) $sites);
				// Если есть форумы
				if($countSites > 0)
				{
					$data = (object) [];
					$poster = unserialize($item->poster);
					$screenshot = unserialize($item->screenshot);
					$data->links = unserialize($item->links);
					$data->meta = unserialize($item->meta);
					$data->info = unserialize($item->info);
					$result['counts'][$category->name] = 0;

					$dataSites = Yii::$app->cache->get('dataSites');

					// Перебираем все сайты
					foreach($sites as $site)
					{
						try
						{
							// Если нет ниодной ветки то пропускаем
							// if($site->enegine != 'phpbb3')
							// 	continue;

							// Если форум выключен
							if(@$site->stataus != 1)
								continue;

							Main::echoTxt('', ['br' => 1]);

							// Список добавленных веток на сайте
							if(is_array($categories1 = unserialize($site->categories)))
							{
								unset($categories);
								foreach($categories1 as $key => $cat1)
								{
									$categories[] = $cat1;
								}

								// Поиск нужно ветки
								if(($key = array_search($category->name, array_column($categories, 'categoryTitle'))) === false)
								{
									Main::echoTxt($site->site.' - Нет нужной ветки');
									continue;
								}
								if(!isset($categories[$key]))
								{
									echo 'Нет такой категории<pre>';
									print_r($categories[$key]);
									print_r($category);
									print_r($categories);
									continue;
								}
								$site->categories = $categories[$key]['threadId'];
							}
							else
							{
								Main::echoTxt($site->site.' - Нет добавленных веток');
								continue;
							}

							if(!array_key_exists($site->hostingPoster, $poster))
							{
								Main::echoTxt($site->site.': Нет нужного постера - '.$site->hostingPoster);

								if($item->save())
									Main::echoTxt('Update item '.$item->id);
								else
									Main::echoTxt('No update item '.$item->id);
								break;
							}
							if(!array_key_exists($site->hostingScreenshot, $poster))
							{
								Main::echoTxt($site->site.': Нет нужного скриншота - '.$site->hostingScreenshot);

								if($item->save())
									Main::echoTxt('Update item '.$item->id);
								else
									Main::echoTxt('No update item '.$item->id);
								break;

							}

							$data->category = $category->name;
							$data->posterLinks = @$poster[$site->hostingPoster];
							$data->screenshotLinks = @$screenshot[$site->hostingScreenshot];
							$probe = 1;
							do
							{
								if(isset($dataSites[$site->site]['posts'][date('Y_m_d')]) && $dataSites[$site->site]['posts'][date('Y_m_d')] >= $site->maxPost)
								{
									Main::echoTxt($site->site.': Достигнуто максимальное число постов в сутки - '.$site->maxPost);
									continue 2;
								}

								if(isset($dataSites[$site->site]['delay']) && time() < $dataSites[$site->site]['delay'])
								{
									Main::echoTxt($site->site.': Жду: '.($dataSites[$site->site]['delay']-time()).' секунд');
									while(time() < $dataSites[$site->site]['delay'])
										sleep(1);
								}

								if(Publication::addPost($site, $data))
								{
									if(empty($site->delay))
										$site->delay = 5;
									$dataSites[$site->site]['delay'] = time()+$site->delay;
									if(isset($dataSites[$site->site]['posts'][date('Y_m_d')]))
										$dataSites[$site->site]['posts'][date('Y_m_d')]++;
									else
										$dataSites[$site->site]['posts'][date('Y_m_d')] = 1;
									break;
								}
								else
								{
									Main::echoTxt('Попытка №: '.$probe);
									$probe++;
								}
							}
							while($probe < 4);
							$result['counts'][$category->name]++;
						}
						catch(Exception $e)
						{
							Main::echoTxt('Исключение контроллера: '.$e->getMessage().PHP_EOL);
						}
					}

					Yii::$app->cache->set('dataSites', $dataSites);

					// Обновляем пост как опубликованный
					if($result['counts'][$category->name] > 0)
					{
						$item->status = 5;
						if($item->save())
							Main::echoTxt('Update item '.$item->id);
						else
						{
							Main::echoTxt('Error update item '.$item->id);
							echo '<pre>';
							print_r($item);
							echo '</pre>';
						}

						Main::echoTxt('############################################################', ['br' => 1]);
					}
				}
				else
					Main::echoTxt('Нет '.$only.' для публикации');
			}
			else
				Main::echoTxt('Нет постов для публикации');
		}

		@$res = Yii::$app->cache->get('posts-'.self::$userId);
		@$res[date('Y/m/d')][] = [
			'result' => @$result['counts'],
			'complete-time' => date('Y/m/d H:i'),
		];

		Yii::$app->cache->set('posts-'.self::$userId, $res);
		// print_r(@$res);
		// print_r(@$result);

		Main::echoTxt('Жду до: '.date('Y/m/d H:i:s', $waitTime).PHP_EOL);
		while(time() < $waitTime)
		{
			sleep(10);
		}
		
	}
}