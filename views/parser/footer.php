
		<div class="col-lg-6">
			<? if(@$info) : ?>
				<? foreach($info as $style => $msg) : ?>
					<div class="alert alert-info"><?=@$msg?></div>
				<? endforeach; ?>
				<hr>
			<? endif; ?>

			<? if(@$console) : ?>
				<? foreach($console as $style => $msg) : ?>
					<div><?=@$msg?></div>
				<? endforeach; ?>
				<hr>
			<? endif; ?>
			
			<div id="alert">
			<? if(@$alert) : ?>
				<? foreach($alert as $msg) : ?>
					<? foreach($msg as $style => $text) : ?>
						<? if($style == 'hr') : ?>
							<hr>
						<? else : ?>
							<div class="alert alert-<?=$style?>"><?=@$text?></div>
						<? endif; ?>
					<? endforeach; ?>
				<? endforeach; ?>
			<? endif; ?>

			<? if(@$complete) : ?>
				<audio src="/web/assets/complete.mp3" autoplay></audio>
			<? endif; ?>
			</div>
		</div>

		<div class="col-lg-6">
		    <table class="table table-hover table-responsive table-bordered">
		    	<thead>
		    		<tr>
		    			<td>Файл</td>
		    			<td>Постов</td>
		    			<td><span class="glyphicon glyphicon-trash"></span></td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<? if(@$filesTxt) : ?>
			    		<? foreach($filesTxt as $file) : ?>
			    		<tr>
							<? //Pjax::begin(['timeout' => 5000, 'enablePushState' => false,]); ?>
			    			<td><a href="/download-file/<?=$file['name']?>"><span class="glyphicon glyphicon-download"></span> <?=$file['name']?></a></td>
			    			<td><?=$file['count']?></td>
			    			<td><button type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove del danger" data-file="<?=$file['name']?>"></span></button></td>
			    			<? //Pjax::end(); ?>
			    		</tr>
				    	<? endforeach; ?>
			    	<? endif; ?>
		    	</tbody>
		    </table>
		</div>

<style>
	#alert .alert{
		background: none;
		padding: 0;
		margin: 0;
		border: none;
	} 
</style>
<?php
$js = <<<JS
$('.del').on('click', function(){
	fileName = $(this).attr('data-file');
	if(confirm('Удалить файл '+fileName+'?')){
		tr = $(this).parents('tr');
	    $.ajax({
	        'type' : 'GET',
	        'url' : '/parser/delete-file',
	        'dataType' : 'html',
	        'data' : {
	            'fileName' : fileName,
	        },
	        'success' : function(data){
	        	tr.html('<td class="success" colspan="3" align="center">'+data+'</td>');
	        	// tr.remove();
	        }
	    });
	}
})
JS;
$this->registerJs($js);
?>