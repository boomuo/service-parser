<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\UserConfig */

$this->title = 'Постилка';
?>
<div class="posting">

    <h1><?= Html::encode($this->title) ?></h1>
	<p>Постилка будет работать в автономном режиме и публиковать во все категрии по очереди 10 раз в сутки.</p>

	<div class="row">
		<div class="col-lg-12">
			<? if(@$publicationActive) : ?>
				<div class="alert alert-success">Постилка работает <a href="/parser/publication?command=stop" class="btn btn-danger">Stop</a></div>
			<? else : ?>
				<div class="alert alert-danger">Постилка не работает <a href="/parser/publication?command=start" class="btn btn-primary">Start</a></div>
			<? endif ?>
		</div>

		<div class="col-lg-2">
			<!-- Nav tabs -->
			<ul class="nav nav-navbar">
				<? foreach($info as $key => $item) : ?>
					<li><a href="#item-<?=@++$i?>" data-toggle="tab"><?=$key?></a></li>
				<? endforeach ?>
			</ul>
		</div>

		<? if($error_log) : ?>
		<div class="col-lg-12">
			<h3>Ошибки</h3>
			<div class="log" id="log-error">
				<pre>
					<?=$error_log?>
				</pre>
			</div>
		</div>
		<? endif ?>

		<? if($publication_log) : ?>
		<div class="col-lg-12">
			<h3>Лог работы постилки</h3>
			<div class="log" id="log-application">
				<pre>
					<?=$publication_log?>
				</pre>
			</div>
		</div>
		<? endif ?>

		
		<div class="col-lg-10">
			<!-- Tab panes -->
			<div class="tab-content">
				<? foreach($info as $key => $item) : ?>
					<div class="tab-pane" id="item-<?=@++$ii?>">
						<pre>
							<? print_r($item) ?>
						</pre>
					</div>
				<? endforeach ?>
			</div>
		</div>
	</div>
</div>

<style>
	#alert .alert{
		background: none;
		padding: 0;
		margin: 0;
		border: none;
	} 
	.log{
		width: 100%;
		height: 100%;
		/*overflow: auto;
		border: 1px solid #ddd;*/
		padding: 5px;
	}
	.log pre{
		width: 100%;
		max-height: 380px;
		overflow: auto;
	}
	.log#log-application pre{
		background: #fff3d3 !important;
	}
	.log#log-error pre{
		background: #fbe5e5 !important;
	}
</style>

<?php
$js = <<<JS
$('#log-application pre').scrollTop($('#log-application pre')[0].scrollHeight);
JS;
$this->registerJs($js);
?>