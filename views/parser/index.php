<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\Models\UserConfig */

$this->title = 'Parser';
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="site-about">

	<h2>Доступные парсеры:</h2>
	<h3>Тубы</h3>
	<ul>
		<? foreach($tubes as $item) : ?>
			<li><a href="<?= $item->url; ?>" target="_blank"><?= $item->name; ?></a> <?= $item->descriptionTube; ?></li>
		<? endforeach; ?>
	</ul>
	<h3>Торренты</h3>
	<ul>
		<? foreach($torrents as $item) : ?>
			<li><a href="<?= $item->url; ?>" target="_blank"><?= $item->name; ?></a> <?= $item->descriptionTube; ?></li>
		<? endforeach; ?>
	</ul>
	<pre>
		<?print_r(@$data)?>
	</pre>

</div>
