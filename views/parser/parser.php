<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\UserConfig */

$this->title = 'Парсер';
?>
<div class="parser">


	<div class="row">

		<div class="col-lg-6">
		    <h1><?= Html::encode($this->title) ?></h1>
			<p>Парсер будет работать в автономном режиме.</p>

			<? if(@$info) : ?>
			<div class="row">
			<? foreach($info as $style => $msg) : ?>
				<div class="col-lg-6">
					<div class="alert alert-info"><?=@$msg?></div>
				</div>
			<? endforeach; ?>
			</div>
			<? endif; ?>

			<? if(@$button) : ?>
			<? foreach($button as $text => $href) : ?>
				<div><a href="<?=@$href?>" class="btn btn-primary" target="_blank"><?=@$text?></a></div>
			<? endforeach; ?>
			<hr>
			<? endif; ?>

			<div id="alert">
			<? if(@$alert) : ?>
			<? foreach($alert as $msg) : ?>
				<? foreach($msg as $style => $text) : ?>
					<? if($style == 'hr') : ?>
						<hr>
					<? else : ?>
						<div class="alert alert-<?=$style?>"><?=@$text?></div>
					<? endif; ?>
				<? endforeach; ?>
			<? endforeach; ?>
			<? endif; ?>

			<? if(@$complete) : ?>
			<audio src="/web/assets/complete.mp3" autoplay></audio>
			<? endif; ?>
			</div>


			<? if(@$worker_log) : ?>
			<div class="col-lg-12">
				<h3>Изменения</h3>
				<div class="log" id="log">
					<? foreach($worker_log as $text) : ?>
					<div>
						<?=$text?>
					</div>
					<? endforeach ?>
				</div>
			</div>
			<? endif ?>
		</div>

		<div class="col-lg-6">
		    <table class="table table-hover table-responsive table-bordered">
		    	<thead>
		    		<tr>
		    			<td>Категория</td>
		    			<td>Статус</td>
		    			<td>Лог</td>
		    			<td>Для сайтов</td>
		    			<td>Для форумов</td>
		    			<td>Всего</td>
		    			<!-- <td><span class="glyphicon glyphicon-trash"></span></td> -->
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<? if(@$categories) : ?>
			    		<? foreach($categories as $category) : ?>
			    		<tr>
							<? //Pjax::begin(['timeout' => 5000, 'enablePushState' => false,]); ?>
			    			<td><?=$category['name']?></td>
			    			<td>
			    				<?if($category['status']) : ?>
			    					<a href="/parser?stop=<?=$category['name']?>" class="btn btn-warning">Stop</a>
			    				<? else : ?>
			    					<a href="/parser?start=<?=$category['name']?>" class="btn btn-success">Start</a>
			    				<? endif ?>
			    			</td>
			    			<td><a href="/parser/log?cat=<?=$category['name']?>" target="_blank">Лог</a></td>
			    			<td><?=$category['countToSite']?></td>
			    			<td><?=$category['countToForum']?></td>
			    			<td><?=$category['countAll']?></td>
			    			<!-- <td><button type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove del danger" data-file="<?=$category['name']?>"></span></button></td> -->
			    			<? //Pjax::end(); ?>
			    		</tr>
				    	<? endforeach; ?>
			    	<? endif; ?>
		    	</tbody>
		    </table>
		</div>

	</div>

	<div class="row">
		<? if(@$error_log) : ?>
		<div class="col-lg-12">
			<h3>Ошибки</h3>
			<div class="log" id="log-error">
				<pre>
					<?=$error_log?>
				</pre>
			</div>
		</div>
		<? endif ?>

		<? if(@$parser_log) : ?>
		<div class="col-lg-12">
			<h3>Лог работы парсера</h3>
			<div class="log" id="log-application">
				<pre>
					<?=$parser_log?>
				</pre>
			</div>
		</div>
		<? endif ?>
	</div>
</div>
<style>
	#alert .alert{
		background: none;
		padding: 0;
		margin: 0;
		border: none;
	} 
	.log{
		width: 100%;
		height: 100%;
		/*overflow: auto;
		border: 1px solid #ddd;*/
		padding: 5px;
	}
	.log textarea{
		width: 100%;
		min-height: 380px;
	}
	.log pre{
		width: 100%;
		max-height: 380px;
		overflow: auto;
	}
	.log#log-application pre{
		background: #fff3d3 !important;
	}
	.log#log-error pre{
		background: #fbe5e5 !important;
	}
</style>
<?php
$js = <<<JS
$('#log-application pre').scrollTop($('#log-application pre')[0].scrollHeight);

$('.del').on('click', function(){
	fileName = $(this).attr('data-file');
	if(confirm('Удалить файл '+fileName+'?')){
		tr = $(this).parents('tr');
	    $.ajax({
	        'type' : 'GET',
	        'url' : '/parser/delete-file',
	        'dataType' : 'html',
	        'data' : {
	            'fileName' : fileName,
	        },
	        'success' : function(data){
	        	tr.html('<td class="success" colspan="3" align="center">'+data+'</td>');
	        	// tr.remove();
	        }
	    });
	}
})
JS;
$this->registerJs($js);
?>