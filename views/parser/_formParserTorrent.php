<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\UserConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-config-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-lg-4">
        <?= $form->field($model, 'sourceid')->dropDownList($tubes, ['required' => true]) ?>
        </div>

        <div class="col-lg-4">
        <?= $form->field($model, 'forumid')->textInput(['type' => 'number', 'value' => 1820]) ?>
        </div>

        <div class="col-lg-4">
        <?= $form->field($model, 'count')->textInput(['required' => true, 'type' => 'number', 'value' => 2]) ?>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <?= Html::submitButton('Старт', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
