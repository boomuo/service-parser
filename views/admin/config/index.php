<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Configs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-config-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User Config', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'user_id',
            // 'categories',
            'proxy',
            'minSaveSize',
            // 'countPosters',
            // 'forumCountPosters',
            // 'forumHosting',
            // 'forumHostingSize',
            // 'siteHosting',
            // 'siteHostingSize',
            // 'screenshotCols',
            // 'screenshotRows',
            // 'screenshotWidth',
            // 'screenshotHostingSize',
            // 'archivesSizeCheck',
            // 'archivesSize',
            // 'createTxt',
            // 'templateTxtFiles:ntext',
            // 'txtFilesDelimer',
            'k2s',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
