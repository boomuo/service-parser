<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Models\UserConfig */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Configs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-config-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'categories',
            'proxy',
            'minSaveSize',
            'countPosters',
            'forumCountPosters',
            'forumHosting',
            'forumHostingSize',
            'siteHosting',
            'siteHostingSize',
            'screenshotCols',
            'screenshotRows',
            'screenshotWidth',
            'screenshotHostingSize',
            'archivesSizeCheck',
            'archivesSize',
            'createTxt',
            'templateTxtFiles:ntext',
            'txtFilesDelimer',
            'k2s',
        ],
    ]) ?>

</div>
