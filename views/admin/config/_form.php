<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\UserConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-config-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'categories')->textInput() ?>

    <?= $form->field($model, 'proxy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minSaveSize')->textInput() ?>

    <?= $form->field($model, 'countPosters')->textInput() ?>

    <?= $form->field($model, 'forumCountPosters')->textInput() ?>

    <?= $form->field($model, 'forumHosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'forumHostingSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siteHosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siteHostingSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'screenshotCols')->textInput() ?>

    <?= $form->field($model, 'screenshotRows')->textInput() ?>

    <?= $form->field($model, 'screenshotWidth')->textInput() ?>

    <?= $form->field($model, 'screenshotHostingSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'archivesSizeCheck')->textInput() ?>

    <?= $form->field($model, 'archivesSize')->textInput() ?>

    <?= $form->field($model, 'createTxt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'templateTxtFiles')->textarea(['rows' => 20]) ?>

    <?= $form->field($model, 'txtFilesDelimer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uploadInFolder')->radio(['maxlength' => 'true', 'no' => 1]) ?>

    <?= $form->field($model, 'k2s')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
