<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\Tubes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tubes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descriptionTube')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mainPaginationPage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'searchPaginationPage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoriesPaginationPage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoriesListPage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoriesListLinkTag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'articlesListLinkTag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'titleTag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fileLink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'posterLink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'allInfo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tagsTag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descriptionTag')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'actor')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'studio')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'format')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'audio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dataAuth')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'trailerLink')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'dateAdd')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'translateRusEng')->checkbox() ?>

    <?= $form->field($model, 'posting')->checkbox() ?>

    <?= $form->field($model, 'postingOnline')->checkbox() ?>

    <?= $form->field($model, 'txt')->checkbox() ?>

    <?//= $form->field($model, 'language')->radioList(['rus' => 'Rus', 'eng' => 'Eng']) ?>

    <?= $form->field($model, 'type')->radioList(['tube' => 'Tube', 'torrent' => 'Torrent']) ?>

    <?= $form->field($model, 'status')->radioList([1 => 'Вкл', 0 => 'Выкл']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
