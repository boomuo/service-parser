<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tubes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tubes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tubes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'descriptionTube:ntext',
            'url:url',
            'mainPaginationPage',
            'searchPaginationPage',
            'categoriesPaginationPage',
            'categoriesListPage',
            'categoriesListLinkTag',
            'articlesListLinkTag',
            'allInfo',
            'titleTag',
            'fileLink',
            'posterLink',
            'tagsTag',
            'actor',
            'studio',
            'year',
            'video',
            'format',
            'audio',
            'descriptionTag',
            'translateRusEng',
            'dataAuth',
            'trailerLink',
            'posting',
            'postingOnline',
            'txt',
            'language',
            'type',
            'status',
            'dateAdd',
        ],
    ]) ?>

</div>