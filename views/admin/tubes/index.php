<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tubes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tubes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tubes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'name',
            // 'descriptionTube:ntext',
            'url:url',
            // 'mainPaginationPage',
            // 'searchPaginationPage',
            // 'categoriesPaginationPage',
            // 'categoriesListPage',
            // 'categoriesListLinkTag',
            // 'articlesListLinkTag',
            // 'allInfo',
            // 'titleTag',
            // 'fileLink',
            // 'posterLink',
            // 'tagsTag',
            // 'actor',
            // 'studio',
            // 'year',
            // 'video',
            // 'format',
            // 'audio',
            // 'descriptionTag',
            // 'translateRusEng',
            // 'dataAuth',
            // 'trailerLink',
            'posting',
            // 'postingOnline',
            'txt',
            // 'language',
            'type',
            'status',
            'dateAdd',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>