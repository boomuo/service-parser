<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-config-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'categories')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'proxy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minSaveSize')->textInput() ?>

    <?= $form->field($model, 'forumPosterCount')->textInput() ?>

    <?= $form->field($model, 'forumPosterHosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'forumPosterHostingSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'forumScreenshotHosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'forumScreenshotHostingSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sitePosterCount')->textInput() ?>

    <?= $form->field($model, 'sitePosterHosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sitePosterHostingSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siteScreenshotHosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siteScreenshotHostingSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'screenshotCols')->textInput() ?>

    <?= $form->field($model, 'screenshotRows')->textInput() ?>

    <?= $form->field($model, 'screenshotWidth')->textInput() ?>

    <?= $form->field($model, 'trailerCreate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trailerCountSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trailerOpenloadAccount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'archivesSizeCheck')->textInput() ?>

    <?= $form->field($model, 'archivesSize')->textInput() ?>

    <?= $form->field($model, 'createTxt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'templateTxtFiles')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'txtFilesDelimer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uploadInFolder')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accountFileHosting')->text(['maxlength' => true]) ?>

    <?= $form->field($model, 'accountPhotoHosting')->text(['maxlength' => true]) ?>

    <?= $form->field($model, 'accountSitePosting')->text(['maxlength' => true]) ?>

    <?= $form->field($model, 'templateSitePosting')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
