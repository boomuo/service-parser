<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Config Sites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Config Site', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'site',
            'login',
            'password',
            // 'hostingPoster',
            // 'hostingScreenshot',
            // 'templateTitle',
            // 'templateShortStory:ntext',
            // 'templateFullStory:ntext',
            // 'dublicateShort',
            // 'xfields',
            // 'categories',
            // 'stataus',
            // 'enegine',
            // 'proxy',
            // 'publicateNow',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
