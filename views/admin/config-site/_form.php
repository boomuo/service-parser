<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConfigSite */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-site-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hostingPoster')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hostingScreenshot')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'templateTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'templateShortStory')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'templateFullStory')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'dublicateShort')->textInput() ?>

    <?= $form->field($model, 'xfields')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categories')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stataus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'enegine')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'proxy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'publicateNow')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
