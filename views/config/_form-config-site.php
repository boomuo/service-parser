<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConfigSite */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-site-form">
<div class="row">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
    
        <div class="col-lg-12">
        <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-6">
        <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-lg-6">
        <?//= $form->field($model, 'xfields')->textarea(['rows' => 6]) ?>
        </div>
        
        <div class="col-lg-6">
        <?//= $form->field($model, 'categories')->textarea(['rows' => 6]) ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'hostingPoster')->dropDownList($select['photoHostings']) ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'hostingScreenshot')->dropDownList($select['photoHostings']) ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'proxy')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'enegine')->dropDownList(['dle' => 'DLE', 'vbulletin' => 'vBulletin', 'phpbb' => 'phpBB', 'phpbb3' => 'phpBB 3', 'smf' => 'SMF', 'xenforo' => 'XenForo - not work']) ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'maxPost')->textInput() ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'delay')->textInput() ?>
        </div>
        
        <div class="col-lg-6">
        <?//= $form->field($model, 'publicateNow')->checkbox() ?>
        </div>

        <div class="col-lg-12">
        <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>
        </div>
        
        <div class="col-lg-6">
        <?= $form->field($model, 'stataus')->checkbox(['label' => 'Enabled']) ?>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="col-lg-6">
            <button class="btn btn-primary" data-toggle="modal" data-target=".templateVars" onclick="return false">Template vars</button>
        </div>
        
        <div class="col-lg-12">
        <?= $form->field($model, 'templateTitle')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-lg-12">
        <?= $form->field($model, 'templateShortStory')->textarea(['rows' => 3]) ?>
        </div>

        <div class="col-lg-12">
        <?= $form->field($model, 'templateFullStory')->textarea(['rows' => 14]) ?>
        </div>
    </div>

    <div class="col-lg-12">
        
        <ul class="nav nav-tabs">
            <li class="active"><a href="#categories" data-toggle="tab">Threads</a></li>
            <!-- <li><a href="#templates" data-toggle="tab">Templates</a></li> -->
            <!-- <li><a href="#templates2" data-toggle="tab">Templates 2</a></li> -->
            <!-- <li><a href="#templates3" data-toggle="tab">Templates 3</a></li> -->
            <li><a href="#templates4" data-toggle="tab">Templates 4</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="categories">
                <table class="table" data-id="categories" data-form="ConfigSite">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Thread ID</th>
                            <!-- <th>Count</th> -->
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if(@$categoriesList) : ?>
                        <? foreach($categoriesList as $key => $categorySelect) : ?>
                        <tr class="disabled">
                            <td><?=@++$i;?></td>
                            <td class="disabled-td">
                                <select class="form-control" name="ConfigSite[categories][<?=$key?>][categoryTitle]">
                                    <option></option>
                                    <? foreach($categories as $category) : ?>
                                        <option value="<?=ucfirst($category->title)?>" <?=(@$categorySelect['categoryTitle']==ucfirst($category->title)) ? 'selected' : ''; ?>><?=ucfirst($category->title)?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigSite[categories][<?=$key?>][threadId]" value="<?=@$categorySelect['threadId'];?>"></td>
                            <td>
                                <a class="btn btn-primary" target="_blank" href="<?=$categorySelect['threadLink'];?>">Thread</a>
                                <a class="btn btn-primary" target="_blank" href="<?=$categorySelect['addPostLink'];?>">Post</a>
                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <? endforeach; ?>
                        <? endif; ?>

                        <tr class="hidden">
                            <td></td>
                            <td>
                                <select class="form-control" data-id="categoryTitle">
                                    <? foreach($categories as $category) : ?>
                                        <option value="<?=ucfirst($category->title)?>"><?=ucfirst($category->title)?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <td><input type="text" class="form-control" data-id="threadId"></td>
                            <!-- <td class="disabled-td"><input type="text" class="form-control" data-id="count"></td> -->
                            <td>
                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <tr>
                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                            <td></td>
                            <!-- <td></td> -->
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
<? /*
            <div class="tab-pane" id="templates">
                <table class="table" data-id="templates" data-form="ConfigSite">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Forum ID</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if(!empty($model->templates)) : ?>
                        <? foreach($model->templates as $key => $templateSelect) : 

                        $threadCreate = '';
                        foreach($model->categories as $category)
                        {
                            if($category['categoryId'] == $templateSelect['categoryId'])
                                $threadCreate = 'bg-success';
                        }

                        ?>
                        <tr class="disabled <?=@$threadCreate;?>">
                            <td><?=@++$ii;?></td>
                            <td class="disabled-td">
                                <select class="form-control" name="ConfigSite[templates][<?=$key?>][categoryId]">
                                    <? foreach($categories as $category) : ?>
                                        <option value="<?=$category->id?>" <?=(@$templateSelect['categoryId']==$category->id) ? 'selected' : ''; ?>><?=$category->title?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigSite[templates][<?=$key?>][forumId]" value="<?=@$templateSelect['forumId'];?>"></td>
                            <td>
                                <a class="btn btn-primary" target="_blank" href="<?=$templateSelect['templateLink'];?>">Forum</a>
                                <a class="btn btn-primary" target="_blank" href="<?=$templateSelect['addThreadLink'];?>">Add thread</a>
                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <? endforeach; ?>
                        <? endif; ?>

                        <tr class="hidden">
                            <td></td>
                            <td>
                                <select class="form-control" data-id="categoryId">
                                    <? foreach($categories as $category) : ?>
                                        <option value="<?=$category->id?>"><?=$category->title?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <td><input type="text" class="form-control" data-id="forumId"></td>
                            <td>
                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <tr>
                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                            <td></td>
                            <!-- <td></td> -->
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>


            <div class="tab-pane" id="templates2">
                <table class="table" data-id="templates2" data-form="ConfigSite">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Forum ID</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? foreach($categories as $key => $category) :
                            $threadCreate = false;
                            if(@isset($model->templates[$key]['categoryId']))
                                $threadCreate = 'bg-success';

                        ?>
                        <tr class="disabled <?=@$threadCreate;?>">
                            <input type="hidden" name="ConfigSite[templates][][categoryId]" value="<?=@$category->id;?>">
                            <td><?=@++$iii;?></td>
                            <td><?=$category->title;?></td>
                            <td><input type="text" class="form-control" name="ConfigSite[templates][<?=$key?>][forumId]" value="<?=@$model->templates[$key]['forumId'];?>"></td>
                            <td>
                                <? if(@$threadCreate) : ?>
                                <a class="btn btn-primary" target="_blank" href="<?=$model->templates[$key]['templateLink'];?>">Forum</a>
                                <a class="btn btn-primary" target="_blank" href="<?=$model->templates[$key]['addThreadLink'];?>">Add thread</a>
                                <? endif; ?>
                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <? endforeach; ?>

                        <tr class="hidden">
                            <td></td>
                            <td>
                                <select class="form-control" data-id="categoryId">
                                    <? foreach($categories as $category) : ?>
                                        <option value="<?=$category->id?>"><?=$category->title?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <td><input type="text" class="form-control" data-id="forumId"></td>
                            <td>
                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <tr>
                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                            <td></td>
                            <!-- <td></td> -->
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane" id="templates3">
                <table class="table" data-id="templates" data-form="ConfigSite">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Forum</th>
                            <th>ID</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if(@$forums) : ?>
                        <? foreach($forums as $key => $forum) :
                            $threadCreate = false;
                            //     $threadCreate = 'bg-success';

                        ?>
                        <tr class="<?=@$threadCreate;?>">
                            <input type="hidden" name="ConfigSite[templates][<?=$key?>][forumId]" value="<?=@$forum['forumId'];?>">
                            <td><?=@++$iii;?></td>
                            <td>
                                <select class="form-control" name="ConfigSite[templates][<?=$key?>][categoryId]">
                                    <option></option>
                                    <? foreach($categories as $category) : 

                                        $selected = false;
                                        if(@$forum['categoryId'] == $category->id)
                                        {
                                            $selected = 'selected';
                                        }

                                    ?>
                                        <option value="<?=$category->id?>" <?=@$selected;?>><?=$category->title?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>

                            <td><?=@$forum['title'];?></td>
                            <td><?=@$forum['forumId'];?></td>
                            <td>
                                <a class="btn btn-primary" target="_blank" href="<?=@$forum['link'];?>">Forum</a>
                                <a class="btn btn-primary" target="_blank" href="<?=@$forum['addThreadLink'];?>">Add thread</a>
                            </td>
                        </tr>
                        <? endforeach; ?>
                        <? endif; ?>

                        <tr class="hidden">
                            <td></td>
                            <td>
                                <select class="form-control" data-id="categoryId">
                                    <option></option>
                                    <? foreach($categories as $category) : ?>
                                        <option value="<?=$category->id?>"><?=$category->title?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <select class="form-control" data-id="forumId">
                                    <option></option>
                                    <? foreach($forums as $forum) : ?>
                                        <option value="<?=$forum['forumId']?>"><?=$forum['title'];?> | <?=$forum['forumId'];?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <td></td>
                            <td>
                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <tr>
                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                            <td></td>
                            <!-- <td></td> -->
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
*/ ?>

            <div class="tab-pane" id="templates4">
                <table class="table" data-id="templates" data-form="ConfigSite">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Forum</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if(@$templates) : ?> 
                        <? foreach($templates as $key => $template) : ?>
                        <tr class="<?=@$template['style'];?>">
                            <input type="hidden" name="ConfigSite[templates][<?=$key?>][categoryId]" value="<?=@$template['categoryId'];?>">
                            <input type="hidden" name="ConfigSite[templates][<?=$key?>][categoryTitle]" value="<?=@$template['categoryTitle'];?>">
                            <td><?=@++$iii;?></td>
                            <td><?=@$template['categoryTitle'];?></td>
                            <? if(@$forums) : ?>
                            <td class="forumId">
                                <select class="form-control" name="ConfigSite[templates][<?=$key?>][forumId]">
                                    <option></option>
                                    <? foreach($forums as $forum) :
                                        $selected = false;
                                        if(@$forum['id'] == @$template['forumId'])
                                            $selected = 'selected';
                                    ?>
                                    <option value="<?=$forum['id']?>" <?=@$selected;?>><?=$forum['title']?> | <?=$forum['id']?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                            <? else : ?>
                            <td><input type="text" class="form-control" name="ConfigSite[templates][<?=$key?>][forumId]" value="<?=@$template['forumId']?>"></td>
                            <? endif; ?>
                            <td>
                                <a class="btn btn-primary template" target="_blank" href="<?=@$links['template'].@$template['forumId'];?>">Forum</a>
                                <a class="btn btn-primary addThread" target="_blank" href="<?=@$links['addThread'].@$template['forumId'];?>">Add thread</a>
                            </td>
                        </tr>
                        <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
        </div> 

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <? if(!$model->isNewRecord) : ?>
            <?= Html::a('Delete', ['site-delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],]) ?>
            <? endif; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
<?= $this->render('modal-template-vars') ?>
<? $this->registerJsFile('@web/js/button.js', ['depends' => 'yii\web\JqueryAsset']); ?>