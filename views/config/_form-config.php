<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
// use yii\jui\DatePicker;
use yii\jui\Sortable;
use kartik\sortinput\SortableInput;

/* @var $this yii\web\View */
/* @var $model app\Models\ConfigUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-config-form">

    <? 

    $template = "Title {category} | {size}
{poster}

[b]Tags:[/b] {tags}
[b]Duration: [/b]{duration}
[b]Video: [/b]{display}
[b]Size: [/b]{size}

{screenshot}

[b]Links:[/b]
K2S:
[b][url={link_k2s}]{link_k2s}[/url][/b]
Fboom:
[b][url={link_fboom}]{link_fboom}[/url][/b]";
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <!-- Категории -->
        <div class="col-lg-12" id="block-categories">
            <div class="panel panel-default">
                <div class="panel-heading">Категории</div>
                <br>
                <div class="col-lg-10">
                    <div class="alert alert-info">Выбирайте категории только те, где можно скачивать видео ролики. Парсер качает те торренты, где по 1 ролику и рамзер не прывышает 3Гб</div>
                </div>
                <div class="col-lg-2">
                    <a href="/config/scan-category" target="_blank" class="btn btn-primary">Скан категорий</a>
                </div>
                <table class="table" data-id="categories" data-form="ConfigUser">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Источники</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if($model->categories) : ?>
                        <? $key = 0; ?>
                        <? foreach($model->categories as $category) : ?>
                        <tr class="disabled">
                            <td>
                                <input type="text" class="form-control" name="ConfigUser[categories][<?=$key?>][title]" value="<?=@$category['title'];?>">
                            </td>
                            <td>
                                <?
// echo '<pre>';
// print_r($tubesCategories);
// print_r($category['tubes']);
// echo '</pre>';
// exit();
                                ?>
                                <table class="table" data-form="ConfigUser[categories][<?=$key?>][tubes]">
                                    <thead>
                                        <tr>
                                            <th>Категория</th>
                                            <th>Ключевые слова</th>
                                            <th>Макс страничек</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? if(@$category['tubes']) : ?>
                                        <? foreach($category['tubes'] as $key1 => $tubeItem) : ?>
                                        <tr class="disabled">
                                            <td class="disabled-td">
                                                <select class="form-control" name="ConfigUser[categories][<?=$key?>][tubes][<?=$key1?>][category]">
                                                    <? foreach($tubesCategories as $tube) : ?>
                                                        <option value="<?=@$tube->name?>|" <?="$tube->name|" == $tubeItem['category'] ? 'selected' : ''; ?>><?=@$tube->name?></option>
                                                        <? if(isset($tube->categories) && is_array(@$tube->categories)) : ?>
                                                        <? foreach($tube->categories as $categ) : ?>
                                                            <option 
                                                                value="<?=$tube->name.'|'.$categ['category']?>"
                                                                <?="$tube->name|$categ[category]" == $tubeItem['category'] ? 'selected' : ''; ?>
                                                            ><?=@$tube->name?> | <?=@$categ['title']?></option>
                                                        <? endforeach; ?>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td class="disabled-td">
                                                <input type="text" class="form-control" name="ConfigUser[categories][<?=$key?>][tubes][<?=$key1?>][keys]" value="<?=@$tubeItem['keys'];?>">
                                            </td>
                                            <td class="disabled-td">
                                                <input type="text" class="form-control" name="ConfigUser[categories][<?=$key?>][tubes][<?=$key1?>][maxPaginationPage]" value="<?=@$tubeItem['maxPaginationPage'];?>">
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <? endforeach; ?>
                                        <? endif; ?>

                                        <tr class="hidden">
                                            <td class="">
                                                <select class="form-control" data-id="category">
                                                    <? foreach($tubesCategories as $tube) : ?>
                                                        <option value="<?=@$tube->name?>|"><?=@$tube->name?></option>
                                                        <? if(isset($tube->categories) && is_array(@$tube->categories)) : ?>
                                                        <? foreach($tube->categories as $categ) : ?>
                                                            <option value="<?=$tube->name.'|'.$categ['category']?>"><?=@$tube->name?> | <?=@$categ['title']?></option>
                                                        <? endforeach; ?>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td class="">
                                                <input type="text" class="form-control" data-id="keys">
                                            </td>
                                            <td class="">
                                                <input type="text" class="form-control" data-id="maxPaginationPage">
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button type="button" class="btn btn-success add-row-cat"><span class="glyphicon glyphicon-plus"></span></button>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <? $key++; endforeach; ?>
                        <? endif; ?>
                        
                        <tr class="hidden">
                            <td>
                                <input type="text" class="form-control" data-id="title">
                            </td>
                            <td>
                                <!-- <table class="table" data-id="categories" data-form="ConfigUser">
                                    <thead>
                                        <tr>
                                            <th>Категория</th>
                                            <th>Ключевые слова</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="hidden">
                                            <td class="">
                                                <select class="form-control" data-id="category">
                                                    <? foreach($tubes as $tube) : ?>
                                                        <option value="<?=@$tube->name?>"><?=@$tube->name?></option>
                                                        <? if(isset($tube->categories) && is_array(@$tube->categories)) : ?>
                                                        <? foreach($tube->categories as $categ) : ?>
                                                            <option value="<?=$tube->name.'|'.$categ['category']?>"><?=@$tube->name?> | <?=@$categ['title']?></option>
                                                        <? endforeach; ?>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td class="">
                                                <input type="text" class="form-control" data-id="keys">
                                            </td>
                                            <td class="">
                                                <input type="text" class="form-control" data-id="maxPaginationPage">
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button type="button" class="btn btn-success add-row-cat"><span class="glyphicon glyphicon-plus"></span></button>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table> -->
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Категории для форумов -->
        <div class="col-lg-12" id="block-categories-forums">
            <div class="panel panel-default">
                <div class="panel-heading">Категории для форумов</div>
                <table class="table" data-id="categoriesForums" data-form="ConfigUser">
                    <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Ключевый слова</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if($model->categoriesForums) : ?>
                        <? foreach($model->categoriesForums as $key => $category) : ?>
                        <tr class="disabled">
                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigUser[categoriesForums][<?=$key?>][name]" value="<?=$category['name'];?>"></td>
                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigUser[categoriesForums][<?=$key?>][keys]" value="<?=$category['keys'];?>"></td>
                            <td>
                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <? endforeach; ?>
                        <? endif; ?>
                        <tr class="hidden">
                            <td class="disabled-td"><input type="text" class="form-control" data-id="name"></td>
                            <td class="disabled-td"><input type="text" class="form-control" data-id="keys"></td>
                            <td>
                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                        </tr>
                        <tr>
                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <!-- Левая панель -->
        <div class="col-lg-8">
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Постеры</div>
                        
                        <? if(Yii::$app->user->can('toSite')) : ?>
                        <div class="col-lg-12">
                        <?= $form->field($model, 'sitePosterCount')->textInput(['type' => 'number', 'placeholder' => 1]) ?>
                        </div>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'sitePosterHosting')->dropDownList($select['photoHostings']) ?>
                        </div>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'sitePosterHostingSize')->dropDownList($select['size']) ?>
                        </div>
                        <hr>
                        <? endif; ?>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'forumPosterCount')->textInput(['type' => 'number', 'placeholder' => 1]) ?>
                        </div>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'forumPosterHosting')->dropDownList($select['photoHostings']) ?>
                        </div>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'forumPosterHostingSize')->dropDownList($select['size']) ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Скриншоты</div>
                        <div class="col-lg-12">
                        <?= $form->field($model, 'screenshotCols')->textInput(['type' => 'number', 'placeholder' => 3]) ?>
                        </div>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'screenshotRows')->textInput(['type' => 'number', 'placeholder' => 3]) ?>
                        </div>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'screenshotWidth')->textInput(['type' => 'number', 'placeholder' => 900]) ?>
                        </div>
                        <hr>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'forumScreenshotHosting')->dropDownList($select['photoHostings']) ?>
                        </div>

                        <div class="col-lg-12">
                        <?= $form->field($model, 'forumScreenshotHostingSize')->dropDownList($select['size']) ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Трейлер</div>
                                <div class="col-lg-4">
                                <?= $form->field($model, 'trailerCreate')->checkbox(['disabled' => 'disabled']) ?>
                                </div>

                                <div class="col-lg-4">
                                <?= $form->field($model, 'trailerCountSize')->textInput(['placeholder' => 'count:size', 'disabled' => 'disabled']) ?>
                                </div>

                                <div class="col-lg-4">
                                <?= $form->field($model, 'trailerOpenloadAccount')->textInput(['placeholder' => 'login:pass', 'disabled' => 'disabled']) ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Загрузка на ФО</div>
                                <br>
                                <!-- <div class="col-lg-6">
                                <?= $form->field($model, 'archivesSizeCheck')->textInput(['type' => 'number', 'disabled' => true]) ?>
                                </div>

                                <div class="col-lg-6">
                                <?= $form->field($model, 'archivesSize')->textInput(['type' => 'number', 'disabled' => true]) ?>
                                </div>

                                <div class="col-lg-12">
                                <?= $form->field($model, 'uploadInFolder')->checkbox() ?>
                                </div> -->

                                <div class="col-lg-6">
                                <?= $form->field($model, 'md5Change')->checkbox() ?>
                                </div>

                                <div class="col-lg-6">
                                <?= $form->field($model, 'nowPublication')->checkbox() ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-lg-12" id="block-file-hosting">
                            <div class="panel panel-default">
                                <div class="panel-heading">Авторизация в файлообменниках для загрузки</div>
                                <table class="table" data-id="accountFileHosting" data-form="ConfigUser">
                                    <thead>
                                        <tr>
                                            <th>Хостинг</th>
                                            <th>Логин</th>
                                            <th>Пароль</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? if($model->accountFileHosting) : ?>
                                        <? foreach($model->accountFileHosting as $key => $account) : ?>
                                        <tr class="disabled">
                                            <td class="disabled-td">
                                                <select class="form-control" name="ConfigUser[accountFileHosting][<?=$key?>][hosting]">
                                                    <? foreach($select['fileHostings'] as $hosting) : ?>
                                                        <option value="<?=$hosting?>" <?=($account['hosting']==$hosting) ? 'selected' : ''; ?>><?=$hosting?></option>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigUser[accountFileHosting][<?=$key?>][login]" value="<?=$account['login'];?>"></td>
                                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigUser[accountFileHosting][<?=$key?>][password]" value="<?=@$account['password'];?>"></td>
                                            <td>
                                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <? endforeach; ?>
                                        <? endif; ?>
                                        <tr class="hidden">
                                            <td class="disabled-td">
                                                <select class="form-control" data-id="hosting">
                                                    <? foreach($select['fileHostings'] as $hosting) : ?>
                                                        <option value="<?=$hosting?>"><?=$hosting?></option>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td class="disabled-td"><input type="text" class="form-control" data-id="login"></td>
                                            <td class="disabled-td"><input type="text" class="form-control" data-id="password"></td>
                                            <td>
                                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-lg-12" id="block-tube-site">
                            <div class="panel panel-default">
                                <div class="panel-heading">Премиум и прочие аккаунты для парсинга и/или скачивания</div>
                                <table class="table" data-id="accountTubeSite" data-form="ConfigUser">
                                    <thead>
                                        <tr>
                                            <th>Хостинг</th>
                                            <th>Логин</th>
                                            <th>Пароль</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? if($model->accountTubeSite) : ?>
                                        <? foreach($model->accountTubeSite as $key => $account) : ?>
                                        <tr class="disabled">
                                            <td class="disabled-td">
                                                <select class="form-control" name="ConfigUser[accountTubeSite][<?=$key?>][hosting]">
                                                    <? foreach($select['tubes'] as $hosting) : ?>
                                                        <option value="<?=$hosting?>" <?=($account['hosting']==$hosting) ? 'selected' : ''; ?>><?=$hosting?></option>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigUser[accountTubeSite][<?=$key?>][login]" value="<?=$account['login'];?>"></td>
                                            <td class="disabled-td"><input type="text" class="form-control" name="ConfigUser[accountTubeSite][<?=$key?>][password]" value="<?=@$account['password'];?>"></td>
                                            <td>
                                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <? endforeach; ?>
                                        <? endif; ?>
                                        <tr class="hidden">
                                            <td class="disabled-td">
                                                <select class="form-control" data-id="hosting">
                                                    <? foreach($select['tubes'] as $hosting) : ?>
                                                        <option value="<?=$hosting?>"><?=$hosting?></option>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td class="disabled-td"><input type="text" class="form-control" data-id="login"></td>
                                            <td class="disabled-td"><input type="text" class="form-control" data-id="password"></td>
                                            <td>
                                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <?
                        /*
                        <div class="col-lg-12" id="block-photo-hosting">
                            <div class="panel panel-default">
                                <div class="panel-heading">Авторизация на фотохостингах</div>
                                <table class="table" data-id="accountPhotoHosting">
                                    <thead>
                                        <tr>
                                            <th>Хостинг</th>
                                            <th>Логин</th>
                                            <th>Пароль</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? if($select['photoHosting']) : ?>
                                        <? foreach($select['photoHosting'] as $key => $account) : ?>
                                        <tr class="disabled">
                                            <td>
                                                <select class="form-control" name="ConfigUser[accountPhotoHosting][<?=$key?>][hosting]">
                                                    <? foreach($photoHostings as $hosting) : ?>
                                                        <option value="<?=$hosting?>" <?=($account['hosting']==$hosting) ? 'selected' : ''; ?>><?=$hosting?></option>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" name="ConfigUser[accountPhotoHosting][<?=$key?>][login]" value="<?=$account['login'];?>"></td>
                                            <td><input type="password" class="form-control" name="ConfigUser[accountPhotoHosting][<?=$key?>][password]" value="<?=@$account['password'];?>"></td>
                                            <td>
                                                <button type="button" class="btn btn-primary edit-row"><span class="glyphicon glyphicon-edit"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <? endforeach; ?>
                                        <? endif; ?>
                                        <tr class="hidden">
                                            <td>
                                                <select class="form-control" data-id="hosting">
                                                    <? foreach($photoHostings as $hosting) : ?>
                                                        <option value="<?=$hosting?>"><?=$hosting?></option>
                                                    <? endforeach; ?>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" data-id="login"></td>
                                            <td><input type="password" class="form-control" data-id="password"></td>
                                            <td>
                                                <button type="button" class="btn btn-success edit-row"><span class="glyphicon glyphicon-ok"></span></button>
                                                <button type="button" class="btn btn-danger remove-row"><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><button type="button" class="btn btn-success add-row"><span class="glyphicon glyphicon-plus"></span></button></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        */
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">Создание тектовых файлов</div>
                    <div class="col-lg-6">
                    <?//= $form->field($model, 'createTxt')->checkbox() ?>
                    </div>

                    <div class="col-lg-6">
                        <button class="btn btn-primary" data-toggle="modal" data-target=".templateVars" onclick="return false">Переменные шаблона</button>
                    </div>

                    <div class="col-lg-12">
                    <? $templateTxtFiles = $model->templateTxtFiles ? $model->templateTxtFiles : $template; ?>
                    <?= $form->field($model, 'templateTxtFiles')->textarea(['rows' => 20, 'required' => true, 'placeholder' => $template, 'value' => $templateTxtFiles]) ?>
                    </div>

                    <div class="col-lg-12">
                    <?= $form->field($model, 'txtFilesDelimer')->textInput(['maxlength' => true, 'required' => true, 'placeholder' => '==========', 'tooltip' => 'tooltiptooltip']) ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success center-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $this->render('modal-template-vars') ?>
<? $this->registerJsFile('@web/js/button.js', ['depends' => 'yii\web\JqueryAsset']); ?>