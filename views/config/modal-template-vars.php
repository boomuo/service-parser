<div class="modal fade templateVars" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">Переменные шаблона</h4>
            </div>
            <div class="modal-body">
                <code>
                    <p><span class="text-success">{title}</span> - Заголовок (с иточника)</p>
                    <p><span class="text-success">{poster}</span> - BB код потера [URL=http://site/image/123123][IMG]http://site/thumbs/91b35b634456373.jpg[/IMG][/URL]</p>
                    <p><span class="text-success">{k2s_video}</span> - BB код видео [IFRAME]http://k2s.cc/preview/5e6f9dc632e26[/IFRAME]</p>
                    <p><span class="text-success">{description}</span> - Описание (с источника)</p>
                    <p><span class="text-success">{actors}</span> - Актеры (с источника)</p>
                    <p><span class="text-success">{studio}</span> - Студия (с источника)</p>
                    <p><span class="text-success">{year}</span> - Год выпуска (с источника)</p>
                    <p><span class="text-success">{tags}</span> - Теги (с источника)</p>
                    <p><span class="text-success">{video}</span> - Видео (с источника)</p>
                    <p><span class="text-success">{audio}</span> - Аудио (с источника)</p>
                    <p><span class="text-success">{duration}</span> - Продолжительность видео 01:59:59</p>
                    <p><span class="text-success">{display}</span> - Разрешение видео 1024x720</p>
                    <p><span class="text-success">{qulity}</span> - Качество видео Full HD, HD, SD</p>
                    <p><span class="text-success">{size}</span> - Размер видео файла 299.99 MB</p>
                    <p><span class="text-success">{category}</span> - Категория (какую парсили)</p>
                    <p><span class="text-success">{date}</span> - Текущая дата 1999/12/31</p>
                    <p><span class="text-success">{time}</span> - Текушее время 23:59:59</p>
                    <p><span class="text-success">{screenshot}</span> - BB код скриншота [URL=http://site/image/123123][IMG]http://site/thumbs/a1629e634456253.jpg[/IMG][/URL]</p>
                    <p><span class="text-success">{link_k2s}</span> - Cсылка на файл K2S http://k2s.cc/file/5e6f9dc632e26</p>
                    <p><span class="text-success">{link_fboom}</span> - Cсылка на файл Fileboom http://fboom.me/file/5e6f9dc632e26</p>
                    <p><span class="text-success">{link_depfile}</span> - Cсылка на файл Depfile http://kyc.pm/Et6TdSJ5G</p>
                </code>
            </div>
        </div>
    </div>
</div>