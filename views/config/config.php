<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\UserConfig */

$this->title = 'Настройки';
?>
<div class="user-config-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <? if($error) : ?>
        <? foreach($error as $style => $text) : ?>
            <div class="alert alert-<?=$style?>"?><?=$text?></div>
        <? endforeach; ?>
    <? endif; ?>

    <? if($alert) : ?>
    	<? foreach($alert as $style => $text) : ?>
    		<div class="alert alert-<?=$style?>"?><?=$text?></div>
    	<? endforeach; ?>
    <? endif; ?>

    <?= $this->render('_form-config', [
        'model' => $model,
        // 'categories' => $categories,
        'tubes' => $tubes,
        'tubesCategories' => $tubesCategories,
        'select' => @$select,
    ]) ?>

</div>
