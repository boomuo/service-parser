<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sites on publication';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Add site', ['site'], ['class' => 'btn btn-success']) ?>
    </p>

    <? if($alert) : ?>
        <? foreach($alert as $style => $text) : ?>
            <div class="alert alert-<?=$style?>"?><?=$text?></div>
        <? endforeach; ?>
    <? endif; ?>

<?php Pjax::begin(); ?>
    <div class="col-lg-3">
        <!-- <select name="" id="" size="10"> -->
        <ol class="sites-list">
        <? foreach($allSite as $site) :
            if(is_array($cat = unserialize($site->categories)))
                $countCat = count($cat);
            else
                $countCat = 0;

            $class = '';
            if($site->id == @$_GET['id'])
                $class = 'bg-success';
            elseif($site->stataus == 0)
                $class = 'bg-danger';
        ?>
            <!-- <option value="<?= $site->id; ?>"><?= $site->site; ?></option> -->
            <li class="<?=@$class;?>">
                <a href="/config/site?id=<?= $site->id; ?>"><?= $site->site; ?></a>
                <span class="cms-logo">
                    <img src="/images/cms/<?=$site->enegine;?>.png">
                </span>
                <span class="count-cat"><?=$countCat?></span>
            </li>
        <? endforeach; ?>
        </ol>
        <!-- </select> -->
    </div>

    <div class="col-lg-9">
    <?= $this->render('_form-config-site', [
        'allSite' => $allSite,
        'forums' => @$forums,
        'templates' => @$templates,
        'links' => @$links,
        'model' => $model,
        'categories' => $categories,
        'categoriesList' => $categoriesList,
        'select' => @$select,
    ]) ?>
    </div>
<?php Pjax::end(); ?>

</div>
