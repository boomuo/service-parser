<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Adult Parser',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            Yii::$app->user->can('admin') ? ['label' => 'Скан категории', 'url' => ['/parser/category']] : '',
            Yii::$app->user->can('user-pay') ? ['label' => 'Постинг', 'url' => ['/parser/publication']] : '',
            Yii::$app->user->isGuest ? '' : ['label' => 'Парсер', 'url' => ['/parser']],
            Yii::$app->user->isGuest ? '' : [
                'label' => 'Настройки', 
                'items' => [
                    !Yii::$app->user->isGuest ? ['label' => 'Настройки парсера', 'url' => ['/config']] : '',
                    Yii::$app->user->can('user-pay') ? ['label' => 'Настрйоки постинга', 'url' => ['/config/site']] : '',
                ],
            ],
            // ['label' => 'Описание', 'url' => ['/about']],
            ['label' => 'Контакты', 'url' => ['/contact']],
            Yii::$app->user->can('admin') ? [
                'label' => 'Admin', 
                'items' => [
                    ['label' => 'Юзеры', 'url' => ['/admin/users']],
                    ['label' => 'Настройки юзеров', 'url' => ['/admin/user-config']],
                    ['label' => 'Настройки сайтов', 'url' => ['/admin/config-site']],
                    ['label' => 'Тубы', 'url' => ['/admin/tubes']],
                    ['label' => 'Архив', 'url' => ['/admin/archive']],
                    ['label' => 'Категории', 'url' => ['/admin/categories']],
                ],
            ] : '',
            Yii::$app->user->isGuest ? ['label' => 'Регистрация', 'url' => ['/signup']] : '',
            Yii::$app->user->isGuest ? ['label' => 'Вход', 'url' => ['/login']] : (
                '<li>'
                . Html::beginForm(['/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Parser Service <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
