$(document).on('click', 'button.edit-row', function() {
	if($(this).find('span').hasClass('glyphicon-edit'))
	{
		$(this).parent().parent().removeClass('disabled');
		$(this).removeClass('btn-primary').addClass('btn-success');
		$(this).find('span').removeClass('glyphicon-edit').addClass('glyphicon-ok');
	}
	else
	{
		$(this).parent().parent().addClass('disabled');
		$(this).removeClass('btn-success').addClass('btn-primary');
		$(this).find('span').removeClass('glyphicon-ok').addClass('glyphicon-edit');
	}
});
$(document).on("click", "button.remove-row", function() {
	$(this).parent().parent().remove();
});
$(document).on("click", "button.add-row", function() {
	table = $(this).parent().parent().parent().parent();
	attr = table.data('id');
	form = table.data('form');
	tbody = table.children('tbody');
	clone = tbody.children('tr.hidden').clone().removeClass('hidden');
	count = tbody.children('tr').length-2;
	// id = $(this);

	console.log(tbody);

	clone.find('*[data-id=name]').attr('name', form+'['+attr+']['+(count)+'][name]');

	clone.find('*[data-id=hosting]').attr('name', form+'['+attr+']['+(count)+'][hosting]');
	clone.find('*[data-id=login]').attr('name', form+'['+attr+']['+(count)+'][login]');
	clone.find('*[data-id=password]').attr('name', form+'['+attr+']['+(count)+'][password]');

	clone.find('*[data-id=categoryId]').attr('name', form+'['+attr+']['+(count)+'][categoryId]');
	clone.find('*[data-id=categoryTitle]').attr('name', form+'['+attr+']['+(count)+'][categoryTitle]');
	clone.find('*[data-id=threadId]').attr('name', form+'['+attr+']['+(count)+'][threadId]');
	clone.find('*[data-id=forumId]').attr('name', form+'['+attr+']['+(count)+'][forumId]');
	clone.find('*[data-id=count]').attr('name', form+'['+attr+']['+(count)+'][count]');

	clone.find('*[data-id=title]').attr('name', form+'['+attr+']['+(count)+'][title]');
	clone.find('*[data-id=tube]').attr('name', form+'['+attr+']['+(count)+'][tube]');
	clone.find('*[data-id=tube-category]').attr('name', form+'['+attr+']['+(count)+'][tube-category]');
	clone.find('*[data-id=keys]').attr('name', form+'['+attr+']['+(count)+'][keys]');
	clone.find('*[data-id=maxPaginationPage]').attr('name', form+'['+attr+']['+(count)+'][maxPaginationPage]');

	tbody.children('tr.hidden').before(clone);
});
   
$(document).on("click", "button.add-row-cat", function() {
    table = $(this).parent().parent().parent().parent();
    // attr = table.data('id');
    form = table.data('form');
    tbody = table.find('tbody');
    clone = tbody.find('tr.hidden').clone().removeClass('hidden');
    count = tbody.find('tr').length;

    // console.log(id.value());

    clone.find('*[data-id=tube]').attr('name', form+'['+(count)+'][tube]');
    clone.find('*[data-id=category]').attr('name', form+'['+(count)+'][category]');
    clone.find('*[data-id=keys]').attr('name', form+'['+(count)+'][keys]');
    clone.find('*[data-id=maxPaginationPage]').attr('name', form+'['+(count)+'][maxPaginationPage]');

    tbody.find('tr.hidden').before(clone);
});

// $('.forumId select, .forumId input').on('change', function(){
//     id = this.value;
//     parent = $(this).parents('tr');

//     var href1 = parent.find('input[name="links-template"]').val();
//     var href2 = parent.find('input[name="links-addThread"]').val();
//     parent.find('a.template').attr('href', href1+id);
//     parent.find('a.addThread').attr('href', href2+id);
// });

// $('#templates4 tr').each(function() {
//     id = $(this).find('option:selected').val();
//     var href1 = $(this).find('input[name="links-template"]').val();
//     var href2 = $(this).find('input[name="links-addThread"]').val();
//     if(id)
//     {
// 	    $(this).find('a.template').attr('href', href1+id);
// 	    $(this).find('a.addThread').attr('href', href2+id);
// 	}
// 	else
//     {
// 	    $(this).find('a.template').attr('href', '#');
// 	    $(this).find('a.addThread').attr('href', '#');
// 	}
// });