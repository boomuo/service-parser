<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'site',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'CBWFySwLIWPar5GZYUDKOdkOvtFHPogW',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\UserIdentity',
            'enableAutoLogin' => true,
            'loginUrl' => ['login'],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'baseUrl'=> '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                // 'check-proxy/<proxy>' => 'parser/check-proxy',
                // 'parser' => 'parser/tubes',
                // 'torrent' => 'parser/torrents',
                'download-file/<fileName>' => 'parser/download-file',
                'login' => 'site/login',
                'logout' => 'site/logout',
                'signup' => 'site/signup',
                'about' => 'site/about',
                'contact' => 'site/contact',
                // 'admin' => 'admin/users', // Главная админки
                // 'admin/app' => 'admin/users', // Главная админки
                '<controller>/<action>' => '<controller>/<action>',
                // '<controller>/<action>/<category:\w+>/<count:\d+>' => '<controller>/<action>',
                // '<controller:[\w-]+>/<id:\d+>'        => '<controller>/view',
                // 'parser/get/<category:[\w-]+>/<count:\d+>'        => 'parser/get',
            ],
        ],
        
    ],
    // 'as beforeRequest' => [
    //     'class' => 'yii\filters\AccessControl',
    //     'rules' => [
    //         [
    //             'allow' => true,
    //             'actions' => ['login', 'signup'],
    //             'roles' => ['?'],
    //         ],
    //     ],
    // ],
    'params' => $params,
    'homeUrl' => '/',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'panels' => [
            'httpclient' => [
                'class' => 'yii\\httpclient\\debug\\HttpClientPanel',
            ],
        ],
        // uncomment the following to add your IP if you are not connecting from localhost.
        // 'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '37.214.31.251'],
        // 'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => \yii\gii\Module::className(),
        'allowedIPs' => ['*'],
        // uncomment the following to add your IP if you are not connecting from localhost.
        // 'allowedIPs' => ['127.0.0.1', '::1', '192.168.*.*', '37.214.*.*'],
    ];
}

return $config;
