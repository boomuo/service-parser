<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tubes".
 *
 * @property integer $id
 * @property string $name
 * @property string $descriptionTube
 * @property string $url
 * @property string $mainPaginationPage
 * @property string $searchPaginationPage
 * @property string $categoriesPaginationPage
 * @property string $categoriesListPage
 * @property string $categoriesListLinkTag
 * @property string $articlesListLinkTag
 * @property string $allInfo
 * @property string $titleTag
 * @property string $fileLink
 * @property string $posterLink
 * @property string $tagsTag
 * @property string $actor
 * @property string $studio
 * @property string $year
 * @property string $video
 * @property string $format
 * @property string $audio
 * @property string $descriptionTag
 * @property string $translateRusEng
 * @property string $dataAuth
 * @property string $trailerLink
 * @property string $posting
 * @property string $postingOnline
 * @property string $txt
 * @property string $language
 * @property string $type
 * @property string $status
 * @property string $dateAdd
 */
class Tubes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tubes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'descriptionTube', 'url', 'mainPaginationPage', 'searchPaginationPage', 'categoriesPaginationPage', 'categoriesListPage', 'categoriesListLinkTag', 'articlesListLinkTag', 'allInfo', 'titleTag', 'fileLink', 'posterLink', 'tagsTag', 'actor', 'studio', 'year', 'video', 'format', 'audio', 'descriptionTag', 'translateRusEng', 'trailerLink', 'posting', 'postingOnline', 'txt', 'language', 'type', 'status', 'dateAdd'], 'string', 'max' => 255],
            [['dataAuth'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'descriptionTube' => 'Description Tube',
            'url' => 'Url',
            'mainPaginationPage' => 'Main Pagination Page',
            'searchPaginationPage' => 'Search Pagination Page',
            'categoriesPaginationPage' => 'Categories Pagination Page',
            'categoriesListPage' => 'Categories List Page',
            'categoriesListLinkTag' => 'Categories List Link Tag',
            'articlesListLinkTag' => 'Articles List Link Tag',
            'allInfo' => 'All Info',
            'titleTag' => 'Title Tag',
            'fileLink' => 'File Link',
            'posterLink' => 'Poster Link',
            'tagsTag' => 'Tags Tag',
            'actor' => 'Actor',
            'studio' => 'Studio',
            'year' => 'Year',
            'video' => 'Video',
            'format' => 'Format',
            'audio' => 'Audio',
            'descriptionTag' => 'Description Tag',
            'translateRusEng' => 'Translate Rus Eng',
            'dataAuth' => 'Data Auth',
            'trailerLink' => 'Trailer Link',
            'posting' => 'Posting',
            'postingOnline' => 'Posting Online',
            'txt' => 'Txt',
            'language' => 'Language',
            'type' => 'Type',
            'status' => 'Status',
            'dateAdd' => 'Date Add',
        ];
    }
}