<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $category
 * @property string $tube
 * @property string $tubeCategory
 */
class CategoriesTube extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_tube';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'tube', 'tubeCategory'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'tube' => 'Tube',
            'tubeCategory' => 'Tube Category',
        ];
    }
}
