<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config_site".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $site
 * @property string $login
 * @property string $password
 * @property string $hostingPoster
 * @property string $hostingScreenshot
 * @property string $templateTitle
 * @property string $templateShortStory
 * @property string $templateFullStory
 * @property integer $dublicateShort
 * @property string $xfields
 * @property string $categories
 * @property string $templates
 * @property string $stataus
 * @property string $enegine
 * @property string $proxy
 * @property string $notes
 * @property integer $publicateNow
 */
class ConfigSite extends \yii\db\ActiveRecord
{
    // public $site;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['templateShortStory', 'templateFullStory', 'xfields', 'categories', 'templates', 'notes'], 'string'],
            [['user_id', 'dublicateShort', 'publicateNow', 'maxPost', 'delay'], 'integer'],
            [['site', 'login', 'password', 'hostingPoster', 'hostingScreenshot', 'templateTitle', 'stataus', 'enegine', 'proxy'], 'string', 'max' => 255],
            // [['site'], 'urlOnlyDomain'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'site' => 'Url',
            'login' => 'Login',
            'password' => 'Password',
            'hostingPoster' => 'Hosting Poster',
            'hostingScreenshot' => 'Hosting Screenshot',
            'templateTitle' => 'Template Title',
            'templateShortStory' => 'Template Short Story',
            'templateFullStory' => 'Template Full Story',
            'dublicateShort' => 'Dublicate Short',
            'xfields' => 'Xfields',
            'categories' => 'Categories',
            'templates' => 'Templates',
            'stataus' => 'Stataus',
            'enegine' => 'Enegine',
            'proxy' => 'Proxy',
            'publicateNow' => 'Publicate Now',
            'notes' => 'Notes',
            'maxPost' => 'Max Post',
            'delay' => 'Delay',
        ];
    }

    public function urlOnlyDomain()
    {
        return $this->site = parse_url($this->site, PHP_URL_HOST);
    }
}