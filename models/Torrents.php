<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Options is the model behind the options parser.
 */
class Torrents extends Model
{
    public $torrentID;
    public $forumID;
    public $count;
    public $category;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count'], 'integer', 'min' => 1, 'max' => 100],
            [['forumID', 'torrentID'], 'integer'],
            [['category'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'count' => 'Колличество',
            'forumId' => 'ID форума',
            'category' => 'Категория',
            'torrent' => 'Торрент',
        ];
    }
}
