<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $authKey
 * @property string $accessToken
 * @property string $datetime
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime'], 'safe'],
            [['pay'], 'integer'],
            [['username', 'password', 'email', 'authKey', 'accessToken'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'datetime' => 'Datetime',
            'pay' => 'Pay',
        ];
    }

    /**
     * Set parssword md5
     */
    public function setPassword($password)
    {
        return $this->password = md5($password);
    }

    /**
     * Set parssword md5
     */
    public function generateAuthKey($email)
    {
        return $this->authKey = md5('authKey'.$email);
    }

    /**
     * Set parssword md5
     */
    public function generateAccessToken($email)
    {
        return $this->accessToken = md5('accessToken'.$email);
    }
}
