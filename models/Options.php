<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Options is the model behind the options parser.
 */
class Options extends Model
{
    public $count;
    public $category;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count'], 'integer', 'min' => 1, 'max' => 100],
            [['category'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'count' => 'Колличество',
            'category' => 'Категория',
        ];
    }
}
