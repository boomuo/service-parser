<?php

namespace app\models\Parser;

use Yii;
use yii\httpclient\Client;
use Sunra\PhpSimple\HtmlDomParser;

class Publication
{
	public static $site;

    /**
     * Добавляет новость на сайт
     *
     * @param array $site [site info]
     * @param array $data [post data]
     * @return str response
     */	
	public static function addPost($site, $data, $need = 1, $try = 0)
	{
		self::$site = $site;
		Main::echoTxt($site->site.' | '.$site->login, ['disableBR' => 1]);
		$dataAuth = self::getAuthData($site);
		if($cookieFile = Main::autorization($dataAuth, $need))
		{
			Main::echoTxt(' Авторизация прошла успешно', ['disableBR' => 1]);
		}
		else
		{
			Main::echoTxt(' Авторизация не удалась', ['disableBR' => 1]);
			return false;
		}

		if(!$postData = self::getPostData($site, $data, $cookieFile))
		{
			Main::echoTxt(' Не получены данные для постинга', ['disableBR' => 1]);
			return false;
		}

		// Постинг
		$result = self::sendData($postData, false, false, 1);
		// if(Main::$consoleMode)
		// 	Main::echoTxt(' Result status - '.$result['code'].': '.@$result['text']);
		// else
		// 	Main::echoTxt($result['code']." - <a target='_blank' href='/parser/open-file?file=$result[file]'>html</a>", ['disableBR' => 1]);
			Main::echoTxt(" <a target='_blank' href='/parser/open-file?file=$result[file]'>$result[code]</a>: ".$result['text'], ['disableBR' => 1]);

		if($result['code'] == 200)
			return true;
		
		return false;
	}

	// Получить данные для авторизации
	private static function getAuthData($site)
	{
		$data['site'] = $site->site;
		$data['proxy'] = $site->proxy;
		switch($site->enegine)
		{
			case 'dle':
				$data['url'] 							= $site->site.'/admin.php';
				$data['login'] 							= $site->login;
				$data['postData']['username'] 			= $site->login;
				$data['postData']['password'] 			= $site->password;
				$data['postData']['subaction'] 			= 'dologin';
				$data['postData']['selected_language'] 	= 'Russian';
				break;
			case 'vbulletin':
				$data['url'] 							= $site->site.'/login.php?do=login';
				$data['login'] 							= $site->login;
				$data['postData']['vb_login_username'] 	= $site->login;
				$data['postData']['vb_login_password'] 	= $site->password;
				$data['postData']['cookieuser'] 		= 1;
				$data['postData']['do'] 				= 'login';
				break;
			case 'phpbb':
				$data['url'] 							= $site->site.'/login.php';
				$data['login'] 							= $site->login;
				$data['postData']['username'] 			= $site->login;
				$data['postData']['password'] 			= $site->password;
				$data['postData']['autologin'] 			= 'on';
				$data['postData']['login'] 				= 'Log in';
				$data['postData']['redirect'] 			= '';
				break;
			case 'phpbb3':
				$data['url'] 							= $site->site.'/ucp.php?mode=login';
				$data['login'] 							= $site->login;
				$data['postData']['username'] 			= $site->login;
				$data['postData']['password'] 			= $site->password;
				$data['postData']['autologin'] 			= 'on';
				$data['postData']['login'] 				= 'Log in';
				$data['postData']['redirect'] 			= '';
				break;
			case 'smf':
				$data['url'] 							= $site->site.'/login2/';
				$data['login'] 							= $site->login;
				$data['postData']['user'] 				= $site->login;
				$data['postData']['passwrd'] 			= $site->password;
				$data['postData']['submit'] 			= '';
				break;
		}
		return $data;
	}

	// получить данные для отправки поста
	private static function getPostData($site, $data, $cookieFile)
	{
		$data = (array) $data;
		$return['cookieFile'] = $cookieFile;
	    $return['proxy'] = $site->proxy;
	    $return['login'] = $site->login;
	    if(@$data['category'])
	    	$return['category'] = $data['category'];
	    else
	    	$return['category'] = $data['info']['category'];
	    $return['enegine'] = $site->enegine;
		switch($site->enegine)
		{
			case 'dle':
				$return['url'] 							= $site['site'].'/admin.php?mod=addnews&action=addnews';
				$return['postData']['title'] 			= self::tempalteHandle($data, $site->templateTitle, 1, ' / ');
				$return['postData']['short_story'] 		= self::tempalteHandle($data, $site->templateShortStory, 1);
				$return['postData']['full_story'] 		= self::tempalteHandle($data, $site->templateFullStory, 1);
				$return['postData']['category'] 		= [$site->categories];
				$return['postData']['tags'] 			= substr(implode(', ',  self::getTags($data)), 0, 250);
				$return['postData']['meta_title'] 		= $return['postData']['title'];
				$return['postData']['keywords'] 		= $return['postData']['tags'];
				$return['postData']['descr'] 			= @substr($data['info']['description'], 0, 198);
				$return['postData']['allow_date'] 		= 'yes';
				$return['postData']['new_author'] 		= $site['login'];
				$return['postData']['approve'] 			= 1; // Опубликованно
				$return['postData']['mod'] 				= 'addnews';
				$return['postData']['action'] 			= 'doaddnews';
				$return['postData']['allow_main'] 		= 1;
				$return['postData']['allow_comm'] 		= 1;
				$return['postData']['allow_rating'] 	= 1;
				$return['postData']['select_poster'] 	= $data['posterLinks'][0];
				$return['postData']['xfield']['k2s_prev_video']= @$data['links']['preview.keep2share.cc'];
				// $return['postData']['select_poster'] = $this->getImg($data);
				if(!$return['postData']['full_story'] || empty($return['postData']['full_story']))
					return false;
				break;
			case 'vbulletin':
				$return['url'] 							= $site->site.'/newreply.php?do=newreply&noquote=1&t='.$site->categories;
				if($token = self::sendData($return, 1, false, false)['html'])
					$return['postData']['securitytoken']= $token->find('input[name="securitytoken"]', 0)->value;
				$return['postData']['do'] 				= 'postreply';
				$return['postData']['t'] 				= $site->categories;
				$return['postData']['title'] 			= self::tempalteHandle($data, $site->templateTitle);
				$return['postData']['sbutton'] 			= 'Submit Reply';
				if(!$return['postData']['message'] 		= self::tempalteHandle($data, $site->templateFullStory))
					return false;
				break;
			case 'phpbb':
				$return['url'] 						= $site->site.'/posting.php?mode=reply&t='.$site->categories;
				$token = self::sendData($return, 1, false, false)['html'];
				if(!empty($token) && is_object($token))
					$return['postData']['sid'] 		= @$token->find('input[name="sid"]', 0)->value;
				else
					echo 'Не получен токен формы отправки';
				$return['postData']['post'] 		= 'Submit';
				$return['postData']['mode'] 		= 'reply';
				$return['postData']['t'] 			= $site->categories;
				$return['postData']['subject'] 		= self::tempalteHandle($data, $site->templateTitle);
				if(!$return['postData']['message'] 	= self::tempalteHandle($data, $site->templateFullStory))
					return false;
				break;
			case 'phpbb3':
				$return['url'] 								= $site->site.'/posting.php?mode=reply&f=1&t='.$site->categories;
				if($token = self::sendData($return, 1, false, false)['html'])
					$return['postData']['form_token'] 			= $token->find('input[name="form_token"]', 0)->value;
				$return['postData']['post'] 				= 'Submit';

				$return['postData']['mode'] 				= 'reply';
				$return['postData']['fileupload'] 			= '';
				$return['postData']['filecomment'] 			= '';
				$return['postData']['topic_cur_post_id'] 	= 2117870;
				$return['postData']['lastclick'] 			= time();
				$return['postData']['creation_time']		= time();

				$return['postData']['t'] 					= $site->categories;
				$return['postData']['subject'] 				= self::tempalteHandle($data, $site->templateTitle);
				if(!$return['postData']['message'] 			= self::tempalteHandle($data, $site->templateFullStory))
					return false;
				break;
			case 'smf':
				$return['url'] 							= $site->site.'/index.php?action=post2&topic='.$site->categories;
				$return['postData']['topic'] 			= $site->categories;
				if($token = self::sendData($return, 1, false, false)['html'])
				{
					if(!$inputs = $token->find('input[type="hidden"]'))
						return false;
					foreach($inputs as $key => $value)
					{
						$return['postData'][$value->name] 	= $value->value;
					}
				}
				$return['postData']['subject'] 			= self::tempalteHandle($data, $site->templateTitle);
				if(!$return['postData']['message'] 		= self::tempalteHandle($data, $site->templateFullStory))
					return false;
				break;
		}
		return $return;
	}

    /**
     * Отправляет данные на сервер
     *
     * @param array $data [loginUrl, cookieFile, postData]
     * @return str cookieFile
     */
	public static function sendData($data, $getHtml = false, $viewContent = false, $saveToFile = true)
	{
		try {
			$client = new Client([
				'transport' => 'yii\httpclient\CurlTransport'
			]);

			$request = $client->createRequest()
				->setUrl($data['url'])
				->setOptions([
					CURLOPT_COOKIEFILE => @$data['cookieFile'],
					CURLOPT_FOLLOWLOCATION => 1,
				    CURLOPT_SSL_VERIFYPEER => 0,
				    CURLOPT_SSL_VERIFYHOST => 0,
				    // CURLOPT_MAXREDIRS => 1,
				    CURLOPT_REFERER => $data['url'],
				    // CURLOPT_AUTOREFERER => 1,
				    // CURLOPT_ENCODING => '',
					'proxy' => @$data['proxy'],
			    	'userAgent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
				]);
			if(@isset($data['postData']))
			{
				$request->setMethod('post');
				$request->setData($data['postData']);
			}
			if(@isset($data['headers']))
			{
				$request->setHeaders($data['headers']);
			}
			if(@$data['file']['fileAttr'] && @$data['file']['filePath'])
			{
				$request->addFile(@$data['file']['fileAttr'], @$data['file']['filePath']);
			}

			$response = $request->send();
			if(!is_object($response))
				return false;

			if($saveToFile)
			{
				$domain = parse_url($data['url'], PHP_URL_HOST);
				$result['file'] = Main::getTemp()->publication.$domain.'['.$data['login'].']_'.$data['category'].'.html';
				file_put_contents($result['file'], $response->content);
			}

	        $result['html'] = HtmlDomParser::str_get_html($response->content);;
	        $result['text'] = self::getStatusText($data['enegine'], $result['html']);
			$result['code'] = $response->getStatusCode();

			if($viewContent)
				echo $response->content;

			if(!$response->isOk)
			{
				// echo '<pre>';
				// print_r($data);
				// echo '</pre>';
				// echo @$file;
				// echo $response->content;
			}
		}
		catch(yii\httpclient\Exception $e)
		{
			Main::echoTxt('Исключение Publication: '.$e->getMessage().PHP_EOL);
			return false;
		}

		return $result;
	}

	private static function getStatusText($enegine, $html)
	{
		switch($enegine)
		{
			case 'dle':
		    	if($element = $html->find('.box .box-content', 0))
		        {
		        	return trim(preg_replace('/\s{2,}/', ' ', $element->plaintext)); // .box-content
		        }
		        break;
			case 'vbulletin':
		    	if($element = $html->find('.blockrow', 0))
		        {
		        	return trim($element->plaintext);
		        }
		    	elseif($element = $html->find('.panelsurround', 0))
		        {
		        	return trim($element->plaintext);
		        }
		        break;
			case 'phpbb':
		    	if($element = $html->find('.blockrow', 0))
		        {
		        	return trim($element->plaintext);
		        }
		        break;
			case 'phpbb3':
		    	if($element = $html->find('.blockrow', 0))
		        {
		        	return trim($element->plaintext);
		        }
		        break;
			case 'smf':
		    	if($element = $html->find('.blockrow', 0))
		        {
		        	return trim($element->plaintext);
		        }
		        break;
			case 'xenforo':
		    	if($element = $html->find('.blockrow', 0))
		        {
		        	return trim($element->plaintext);
		        }
		        break;
		}
	}

	// Получить данные для авторизации
	public static function getLinks($site)
	{
		$data['site'] = $site->site;
		switch($site->enegine)
		{
			case 'dle':
				$data['auth'] 							= $site->site.'/admin.php';
				$data['thread'] 						= $site->site.'/';
				$data['template'] 						= $site->site.'/';
				$data['addPost'] 						= $site->site.'/admin.php?mod=addnews&action=addnews';
				$data['addThread'] 						= $site->site.'/';
				$data['sitemap']['link'] 				= $site->site.'/sitemap/';
				$data['sitemap']['tag'] 				= '#content a';
				break;
			case 'vbulletin':
				$data['auth'] 							= $site->site.'/login.php?do=login';
				$data['thread'] 						= $site->site.'/showthread.php?t=';
				$data['template'] 						= $site->site.'/forumdisplay.php?f=';
				$data['addPost'] 						= $site->site.'/newreply.php?do=newreply&noquote=1&t=';
				$data['addThread'] 						= $site->site.'/newthread.php?do=newthread&f=';
				// $data['sitemap']['link'] 				= $site->site.'/sitemap/';
				// $data['sitemap']['tag'] 				= '#content a';
				$data['sitemap']['link'] 				= $site->site;
				$data['sitemap']['tag'] 				= 'table td.alt1Active a';
				break;
			case 'phpbb':
				$data['auth'] 							= $site->site.'/login.php';
				$data['thread'] 						= $site->site.'/viewtopic.php?t=';
				$data['template'] 						= $site->site.'/viewforum.php?f=';
				$data['addPost'] 						= $site->site.'/posting.php?mode=reply&t=';
				$data['addThread'] 						= $site->site.'/posting.php?mode=newtopic&f=';
				$data['sitemap']['link'] 				= $site->site;
				$data['sitemap']['tag'] 				= '#content a';
				break;
			case 'phpbb3':
				$data['auth'] 							= $site->site.'/ucp.php?mode=login';
				$data['thread'] 						= $site->site.'/viewtopic.php?t=';
				$data['template'] 						= $site->site.'/viewforum.php?t=';
				$data['addPost'] 						= $site->site.'/posting.php?mode=reply&f=1&t=';
				$data['addThread'] 						= $site->site.'/posting.php?mode=post&f=';
				$data['sitemap']['link'] 				= $site->site;
				$data['sitemap']['tag'] 				= '#content a';
				break;
			case 'smf':
				$data['auth'] 							= $site->site.'/login2/';
				$data['thread'] 						= $site->site.'/index.php?topic=';
				$data['template'] 						= $site->site.'/index.php?board=';
				$data['addPost'] 						= $site->site.'/index.php?action=post2&topic=';
				$data['addThread'] 						= $site->site.'/index.php?action=post&board=';
				$data['sitemap']['link'] 				= $site->site;
				$data['sitemap']['tag'] 				= '#content a';
				break;
		}
		return $data;
	}

	// Обработчик шаблонов
	private static function tempalteHandle($data, $tempalte, $br = false, $delimer = ', ')
	{
		// Обработка условий в шаблоне
		$tempalte = str_replace(array('[*','*]'), array('<?','?>'), $tempalte);
		$category = strtolower(trim($data['info']['category']));
		ob_start();
		error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
		eval('?>'.$tempalte.'<?');
		error_reporting(E_ALL);
		$tempalte = ob_get_contents();
		ob_end_clean();

		$data = (array) $data;
		$result['{title}'] = @$data['info']['title']; // Заголовок
		$result['{description}'] = @$data['info']['description']; // Описание
		$result['{category}'] = @ucfirst($data['category']); // Категория
		$result['{tags}'] = @implode($delimer, self::getTags($data)); // Теги
		$result['{actors}'] = @implode($delimer, $data['info']['actors']); // Актрисы
		$result['{studio}'] = @implode($delimer, $data['info']['studio']); // Студия
		$result['{year}'] = @$data['info']['year']; // Описание
		$result['{video}'] = @$data['info']['video'] ?: @$data['meta']['video']; // Описание
		$result['{audio}'] = @$data['info']['audio'] ?: @$data['meta']['audio']; // Описание
		$result['{duration}'] = @$data['meta']['duration']; // Продолжительность
		$result['{display}'] = @$data['meta']['display']; // Разрешение
		$result['{quality}'] = @$data['meta']['quality']; // Качество
		$result['{size}'] = @$data['meta']['size']; // Размер
		$result['{date}'] = date('Y/m/d'); // Текущая дата
		$result['{time}'] = date('H:i:s'); // Текущее время

		// Постер
		if(@!empty($data['posterLinks']))
		{
			$result['{poster}'] = '';
			foreach($data['posterLinks'] as $poster)
			{
				$result['{poster}'] .= $poster;
				if(!end($data['posterLinks']))
					$result['{poster}'] .= "\n";
			}
		}

		// Скриншот
		if((@!empty($data['screenshotLinks'])))
		{
			$result['{screenshot}'] = '';
			foreach($data['screenshotLinks'] as $screenshot)
			{
				$result['{screenshot}'] .= $screenshot;
				if(!end($data['screenshotLinks']))
					$result['{screenshot}'] .= "\n";
			}
		}

		$domain = parse_url(self::$site->site, PHP_URL_HOST);

		$result['{k2s_video}'] = '[IFRAME]'.@$data['links']['preview.keep2share.cc'].'[IFRAME]'; // Видео
		$result['{link_k2s}'] = @$data['links']['keep2share.cc'].'?site='.$domain;
		$result['{link_fboom}'] = @$data['links']['fileboom.me'].'?site='.$domain;
		$result['{link_depfile}'] = @$data['links']['depfile.com'].'?site='.$domain;

		if($br)
			$result[PHP_EOL] = '<br>';
		return str_replace(array_keys($result), array_values($result), $tempalte);
	}

	// Получить заголовок поста
	private static function getTitle($data, $template)
	{
		return self::tempalteHandle($data, $titleTempalte);
	}

	// Получить краткую ноость
	private static function getShortStory($data, $template)
	{
		return self::tempalteHandle($data, $shortTemplate);
	}

	// Получить полную новость
	private static function getFullStory($data, $template)
	{
		return self::tempalteHandle($data, $fullTemplate);
	}

	// Получить ID категорий
	private static function getCategory($data)
	{
		$result[] = 1;
		return $result;
	}

	// Получить теги (приплюсовать актрис, студию, год, разрешение, качество)
	private static function getTags($data)
	{
		if(empty($data['info']['tags']))
			$tags = [];
		else
			$tags = $data['info']['tags'];

		if(@!empty($data['info']['studio']))
			$tags = array_merge($tags, $data['info']['studio']);

		if(@!empty($data['info']['actors']))
			if(!is_array($data['info']['actors']))
			{
				$actors = $data['info']['actors'];
				unset($data['info']['actors']);
				$data['info']['actors'][] = $actors;
			}
			$tags = array_merge($tags, $data['info']['actors']);

		if(@!empty($data['info']['year']))
			$tags[] = $data['info']['year'];

		if(@!empty($data['meta']['quality']))
			$tags[] = $data['meta']['quality'];

		return $tags;
	}
	
	// Создает текстовый файл готовый для публикации
	// Возвращает [массив] статус выолнения и инфу о файле
	public static function createFileToForum($item, $folderTxt, $delimer, $template)
	{
		$category = $item['info']['category'];
		@mkdir($folderTxt, 0775, 1);
		$file = $folderTxt.$category.'.txt';

		if(!empty($text = self::tempalteHandle($item, $template)))
		{
			$text .= "\n".$delimer."\n";
			if(file_put_contents($file, $text, FILE_APPEND | LOCK_EX))
				return $category.'.txt';
		}
		return false;
	}
}