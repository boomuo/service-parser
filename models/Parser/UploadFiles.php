<?php

namespace app\models\Parser;

use app\models\Parser\Keep2ShareAPI;

// Класс загрузки файлов на  файлообменники + архивация файлов
class UploadFiles
{
	public $uploadCount = 2;
	public $tempFolder;
	public $archivesSizeCheck;
	public $archivesSize;
	public $uploadInFolder = true;
	public $archivationActive = false;

	public $k2s_user;
	public $k2s_pass;

	public $openload_login;
	public $openload_key;

	public $rarPath;

	/**
	* @var array
	* Массив с ошибками
	*/
	public $error;

	function __construct()
	{
		$this->rarPath =  dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'program-files'.DIRECTORY_SEPARATOR.'rar.exe';
	}


	// Загрузка масива файлов на файлообменник
	// function multiUploadFile($files, $category)
	// {
	// 	$folder_id = $this->k2s->getCategoryId($category);
	// 	$result['id'] = $this->k2s->multiUploadFile($files, $folder_id, $this->uploadCount);
	// 	if(($countResult = count($result['id'])) < ($countAll = count($files)))
	// 	{
	// 		$result['error'] = "Закачано не все! $countResult/$countAll";
	// 	}
	// 	return $result;
	// }

    // Создает архивы (видео файлы разбитые на части)
    public function createArhive($video_file)
    {
        $cmd = "\"$this->rarPath\" a -m0 -ep -v".$this->archivesSize."m \"$video_file.rar\" \"$video_file\"";
        exec($cmd, $output, $error);
        if(!$error || $error == 0)
        {
			$dir = dirname($video_file);
			$scandir = scandir($dir);

			foreach($scandir as $file)
			{
				$fileInfo = pathinfo($dir.$file);
				if(!empty($fileInfo['extension']) && $fileInfo['extension'] == 'rar')
				{
					$return[] = $dir.'/'.$file;
				}
			}
			if(!isset($return))
			{
				return false;
			}
	        return $return;
	    }
	    else
	    {
	    	echo ('Ошибка при архивации файла: ' . $video_file . '<br>Ошибка: ' . $error . '<br>Команда: ' . $cmd);
	    	return false;
	    }
    }

	// Проверка файлообменника на звгружку файлов
	public function checkAndUpload($files, $categoryTitle, $attrResult = 'links', $attrFind = 'filePath')
	{
		$result = [];
		$this->k2s = new Keep2ShareAPI();
		$this->k2s->username = $this->k2s_user;
		$this->k2s->password = $this->k2s_pass;
		$this->k2s->tempFolder = $this->tempFolder;

		$categoryId = $this->k2s->getCategoryId($categoryTitle);
		$categoryFiles = $this->k2s->filesList($categoryId, 1000);

		foreach($files as $key => $file)
		{
			$md5 = md5_file($file['filePath']);
			if($found = $this->k2s->findFile($md5))
			{
				foreach($categoryFiles as $filek2s)
				{
					if($filek2s['md5'] == $md5)
					{
						$result[$key]['links'] =  $this->k2s->getUrlLinks($filek2s['id']);
						// unset($files[$key]);
					}
				}
			}
		}

		if(true)
		{
			return $this->multiUploadFile($files, $categoryTitle, $attrResult, $attrFind);
		}
	}

	// Загрузка масива файлов на файлообменник
	// public function multiUploadFile($files, $category, $attrResult = 'links', $attrFind = 'filePath')
	// {
	// 	$result = [];
	// 	$this->k2s = new Keep2ShareAPI($this->k2s_user, $this->k2s_pass, $this->tempFolder);
		
	// 	// Загружать файлы в папки названия категории
		
	// 	// ИД папки категории
	// 	if($this->uploadInFolder === 1)
	// 		$folderId = $this->k2s->getCategoryId($category);
	// 	else
	// 		$folderId = '/';
	// 	// Список файлов в папке категории
	// 	$filesInHosting[] = $this->k2s->filesList($folderId, 1000);
	// 	// Список файлов в общей папке
	// 	// $filesInHosting[] = $this->k2s->filesList('/', 1000);
	// 	// ИД папки общее
	// 	// $folderAllId = $this->k2s->getCategoryId('All');
	// 	// Список файлов в дополнительной папке (ID папки All)
	// 	// $filesInHosting[] = $this->k2s->filesList($folderAllId, 1000);

	// 	// Перебираем список скачаных файлов
	// 	foreach($files as $key => $file)
	// 	{
	// 		// Архивация файла
	// 		if($attrFind == 'filePath' && filesize($file['filePath']) > ($this->archivesSizeCheck * 1024 * 1024))
	// 		{
	// 			// Создаем архиф
	// 			$archives = $this->createArhive($file['filePath']);
				
	// 			// Удаляем видео после архивации
	// 			if(is_array($archives))
	// 			{
	// 				@unlink($file['filePath']);
	// 			}

	// 			// Готовим массив для заливки на ФО
	// 			$i = 0;
	// 			foreach($archives as $arhive)
	// 			{
	// 				// Добавляем в массив с ключем разделенным на части, путь к файлу
	// 				$upload[$key.'|||'.$i++] = $arhive;
	// 			}
	// 		}
	// 		else
	// 		{					
	// 			// Ищем файл на ФО
	// 			if((isset($filesInHosting) && !empty($filesInHosting)))
	// 			{
	// 				if($foundFileId = $this->searchFile($file, $filesInHosting))
	// 				{
	// 					// Получаем ссылку на файл и записываем в массив резльтата
	// 					echo 'Файл уже залит на ФО';
	// 					$result[$key]['links'] = $this->k2s->getUrlLinks($foundFileId);
	// 					continue;
	// 				}
	// 			}

	// 			// Есди есть что загружать на ФО
	// 			if(isset($file[$attrFind]))
	// 			{
	// 				echo '<pre>';
	// 				print_r($file);
	// 				echo '</pre>';
	// 				exit();
	// 				// Изменение MD5 и закрытие fopen
	// 				if(isset($this->md5Change))
	// 				{
	// 					fopen($file[$attrFind], 'a');
	// 					fwrite($file[$attrFind], "\n".$this->md5Change);
	// 					fclose($fopen[$key]);
	// 				}

					

	// 				// Добавляем в массив путь к файлу
	// 				$upload[$key] = $file[$attrFind];
	// 			}
	// 		}
	// 	}

	// 	// Если есть что загружать на ФО
	// 	if(isset($upload) && !empty($upload))
	// 	{
	// 		$post = $this->k2s->getPostFields($upload, $folderId);
	// 		if(!is_array($post))
	// 		{
	// 			throw new Exception('Ошибка при формировании POST запроса для загрузки файлов<br>Ответ: '.$post);
	// 		}

	// 		if($return = $this->multiUploadFiles($post))
	// 		{
	// 			$result += $this->k2s->callbackResult($return, $attrResult);
	// 		}
	// 	}

	// 	return $result;
	// }

	// Загрузка масива файлов на файлообменник
	public function multiUploadFileOne($file, $category)
	{
		$result = [];
		$this->k2s = new Keep2ShareAPI($this->k2s_user, $this->k2s_pass, $this->tempFolder);
		
		// Загружать файлы в папки названия категории
		
		// ИД папки категории
		$folderId = null;
		if($this->uploadInFolder)
			$folderId = $this->k2s->getCategoryId($category);

		// Список файлов в папке категории
		// $filesInHosting[] = $this->k2s->filesList($folderId, 1000);
		// Список файлов в общей папке
		// $filesInHosting[] = $this->k2s->filesList('/', 1000);
		// ИД папки общее
		// $folderAllId = $this->k2s->getCategoryId('All');
		// Список файлов в дополнительной папке (ID папки All)
		// $filesInHosting[] = $this->k2s->filesList($folderAllId, 1000);

		// Архивация файла
		if(@$this->archivationActive == true && filesize($file) > (1024 * 1024 * $this->archivesSizeCheck))
		{
			// Создаем архиф
			if($archives = $this->createArhive($file))
			{
				// Удаляем видео после архивации (чтобы не занимало место)
				if(is_array($archives))
				{
					@unlink($file);
				}

				// Готовим массив для заливки на ФО
				$i = 0;
				foreach($archives as $arhive)
				{
					// Добавляем в массив с ключем разделенным на части, путь к файлу
					if($return = $this->k2s->uploadFile($arhive, $folderId))
						$upload[] = $this->k2s->callbackResultOne($return);
					else
						echo 'Ошибка загрузки файла на ФО: '.$arhive;
				}

				if(@$upload)
					return $upload;
			}
		}
		else
		{
			// Ищем файл на ФО
			if((isset($filesInHosting) && !empty($filesInHosting)))
			{
				if($foundFileId = $this->searchFile($file, $filesInHosting))
				{
					// Получаем ссылку на файл и записываем в массив резльтата
					return [
						'links' => $this->k2s->getUrlLinks($foundFileId)
					];
				}
			}
		}

		if($return = $this->k2s->uploadFile($file, $folderId))
			return $this->k2s->callbackResultOne($return);
		else
		{
			echo 'Ошибка загрузки файла на ФО: '.$file;
			return false;
		}
	}

	// Загрузка масива файлов на файлообменник
	// public function multiUploadFile2($files, $category, $attrResult = 'links', $attrFind = 'filePath')
	// {
	// 	$result = [];
	// 	$folderId = false;
	// 	$this->k2s = new Keep2ShareAPI($this->k2s_user, $this->k2s_pass, $this->tempFolder);
				
	// 	// ИД папки категории
	// 	$folderId = $this->k2s->getCategoryId($category);
	// 	// Список файлов в папке категории
	// 	$filesInHosting[] = $this->k2s->filesList($folderId, 1000);
	// 	// Список файлов в общей папке
	// 	$filesInHosting[] = $this->k2s->filesList('/', 1000);
	// 	// ИД папки общее
	// 	$folderAllId = $this->k2s->getCategoryId('All');
	// 	// Список файлов в дополнительной папке (ID папки All)
	// 	$filesInHosting[] = $this->k2s->filesList($folderAllId, 1000);

	// 	// Перебираем список скачаных файлов
	// 	foreach($files as $key => $file)
	// 	{
	// 		// Если загружаем видео файлы и размер файла больше чем в настройках для архивации
	// 		if($attrFind == 'filePath' && filesize($file['filePath']) > ($this->archivesSizeCheck * 1024 * 1024))
	// 		{
	// 			// Создаем архиф
	// 			$archives = $this->createArhive($file['filePath']);
				
	// 			// Удаляем видео после архивации
	// 			if(is_array($archives))
	// 			{
	// 				@unlink($file['filePath']);
	// 			}

	// 			// Готовим массив для заливки на ФО
	// 			$i = 0;
	// 			foreach($archives as $arhive)
	// 			{
	// 				// Добавляем в массив с ключем разделенным на части, путь к файлу
	// 				$upload[$key.'|||'.$i++] = $arhive;
	// 			}
	// 		}
	// 		else
	// 		{
	// 			// Ищем файл на ФО
	// 			if((isset($filesInHosting) && !empty($filesInHosting)) && $foundFileId = $this->searchFile($file, $filesInHosting))
	// 			{
	// 				// Получаем ссылку на файл и записываем в массив резльтата
	// 				$result[$key]['links'] = $this->k2s->getUrlLinks($foundFileId);
	// 				continue;
	// 			}

	// 			// Есди есть что загружать на ФО
	// 			if(isset($file[$attrFind]))
	// 			{
	// 				// Добавляем в массив путь к файлу
	// 				$upload[$key] = $file[$attrFind];
	// 			}
	// 		}
	// 	}

	// 	// Если есть что загружать на ФО
	// 	if(isset($upload) && !empty($upload))
	// 	{
	// 		$post = $this->k2s->getPostFields($upload, $folderId);
	// 		if(!is_array($post))
	// 		{
	// 			throw new Exception('Ошибка при формировании POST запроса для загрузки файлов<br>Ответ: '.$post);
	// 		}

	// 		if($return = $this->multiUploadFiles($post))
	// 		{
	// 			$result += $this->k2s->callbackResult($return, $attrResult);
	// 		}
	// 	}

	// 	return $result;
	// }

	// Поиск файла на ФО
	function searchFile($file, $filesInHosting)
	{
		// МД5 видео файла
		$md5 = md5_file($file['filePath']);
		// Если файл найден в списке ФО
		if($found = $this->k2s->findFile($md5))
		{
			if(!empty($filesInHosting))
			{
				// Перебираем список файлов ФО
				foreach($filesInHosting as $folderFiles)
				{
					foreach($folderFiles as $file)
					{
						// Если мд5 файла равен мд5 файлу с ФО
						if($file['md5'] == $md5)
						{
							return $file['id'];
						}
					}
				}
			}
		}
	}

  //   function multiUploadFiles($data)
  //   {
		// $rollingCurl = new RollingCurl();

  //       $mainOptions = [
  //       	CURLOPT_RETURNTRANSFER => 1,
  //       	CURLOPT_FOLLOWLOCATION => 1,
  //       	CURLOPT_TIMEOUT => 60*60,
  //       	CURLOPT_CONNECTTIMEOUT => 60,
  //       ];

		// foreach($data as $key => $options)
		// {
		//     $request = new Request($options['uploadURL'], 'POST');
		//     $request->setOptions($mainOptions);
		//     $request->setPostData($options);
		//     $rollingCurl->add($request);
		// }

		// $rollingCurl->setSimultaneousLimit($this->uploadCount);
		// $rollingCurl->setCallback(array($this, 'callback'));
	 //    $rollingCurl->execute();

  //       if(isset($this->result) && !empty($this->result))
  //       {
  //       	return $this->result;
  //       }
  //       else
  //       {
  //       	return false;
  //       }
  //   }

 //    function callback(Request $request, RollingCurl $rollingCurl)
	// {
	// 	$result = json_decode($request->getResponseText());
	// 	if($result->status_code == 200)
	// 	{
	// 		$this->result[$request->getPostData()['key']] = $result;
	// 	}
	// 	else
	// 	{
	// 		throw new Exception('Ошибка загрузки файлов<br>Ответ: <pre>'.$result.'</pre>');
	// 	}
 //    }

	// Загрузка 1-го файла на файлообменник
	// function uploadFile($file, $folderId)
	// {
	// 	if(empty($file))
	// 		return false;

	// 	$res = $this->k2s->uploadFile($file, $folderId);
		
	// 	// Если успешно загружено, получаем его id
	// 	if($res->status_code == 200 && $res->success == 1)
	// 		return $res->user_file_id;
	// 	else
	// 		return false;
	// }

	function uploadTrailer($data)
	{
		$dataGetUrl['url'] = 'https://api.openload.co/1/file/ul?login=19ce4027a735f77d&key=SQvMYTDs';
		$requestGeyUrl = $this->curl($dataGetUrl);
		$sendData['url'] = $requestGeyUrl->result->url;

		$result = [];
		foreach ($data as $key => $value)
		{
			if(isset($value['trailerPath']) && !empty($value['trailerPath']))
			{
				$sendData['post']['file'] = new CURLFile($value['trailerPath']);
				$sendData['post']['file']->setPostFilename(basename($value['trailerPath'], '.mp4'));
				if($reqest = $this->curl($sendData))
				{
					$result[$key]['trailerHosingLink'] = 'https://openload.co/embed/' . $reqest->result->id;
				}
			}
		}
		return $result;
	}
}