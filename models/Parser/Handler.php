<?php

namespace app\models\Parser;


class Handler
{
	/**
	* @var integer
	* по сколько секунд куски
	*/
	public $trailerTimePart = 2; // Сколько секунд вырезать кусок

	/**
	* @var integer
	*Сколько кусков делать к треллеру
	*/
	public $trailerPart = 10; // Сколько кусков

	/**
	* @var array
	* Массив с ошибками
	*/
	public $error;

	/**
	* @var array
	* Путь к бинарным файлам
	*/
	public $ffmpegPath;
	public $ffprobePath;

	function __construct()
	{
		$ffmpeg = 'ffmpeg';
		$ffprobe = 'ffprobe';
		if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
		{
			$ffmpeg .= '.exe';
			$ffprobe .= '.exe';
		}

		$this->ffmpegPath = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'program-files'.DIRECTORY_SEPARATOR.$ffmpeg;
		$this->ffprobePath = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'program-files'.DIRECTORY_SEPARATOR.$ffprobe;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get meta
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function getMeta($data)
	{
		foreach($data as $key => $file)
		{
			if(!file_exists($file['filePath']))
			{
				// throw new Exception('Файла не существует');
				$this->error = 'Файла не существует: '.$file['filePath'];
				continue;
			}
			if($res = $this->getMetaFile($file))
			{
				$meta[$key] = $res;
			}
		}
		if(!isset($meta) && empty($meta))
		{
			$this->error = 'Не получены метадата. Файлов было: '.count($data);
			return false;
		}
		return $meta;
	}

	public function getMetaFile($file)
	{
		$data['size'] = Main::size(filesize($file));

		$ffprobe = \FFMpeg\FFProbe::create([
			'ffmpeg.binaries'  => $this->ffmpegPath,
			'ffprobe.binaries'  => $this->ffprobePath,
		]);

		$streams = $ffprobe->streams($file);
		if(empty($streams->audios()->first()))
			return false;
		$data['audio'] = $streams->audios()->first()->get('channel_layout');
		$data['duration'] = date('H:i:s', $streams->videos()->first()->get('duration'));
		$width = $streams->videos()->first()->get('width');
		$height = $streams->videos()->first()->get('height');
		$data['display'] = $width.'x'.$height;
		$data['quality'] = $this->getQuality($height);
		return $data;
	}

	public function getQuality($height)
	{
		if($height >= 1920)
		{
			$category = '4K';
		}
		elseif($height >= 1080)
		{
			$category = 'FullHD 1080p';
		}
		elseif($height >= 720)
		{
			$category = 'HD 720p';
		}
		else
		{
			$category = 'SD';
		}
		return $category;
	}
}