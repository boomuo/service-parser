<?php

namespace app\models\Parser;

use Yii;
use yii\web\Controller;
use app\models\ConfigUser;
use app\models\Archive;
use app\models\Tubes;
use app\models\Categories;
use app\models\UserIdentity;
use app\models\ConfigSite;
use yii\httpclient\Client;
use Sunra\PhpSimple\HtmlDomParser;

class Main
{
	static $viewConsoleLog;
	static $bufer = 0;
	static $consoleMode = false;

	static $config; 
	static $user; 
	static $alert; 

	// Возвращает спсок тубов
	public static function init($id = false)
	{
		if($id)
		{
			self::$user = UserIdentity::findIdentity($id);
		}
		else
		{
			self::$user = Yii::$app->user->identity;
		}

		// Если нет сохраненных настроек, то редирект на страничку создания настроек
		if(!self::$config = ConfigUser::findOne(['user_id' => self::$user->id]))
		{
			return false;;
		}
	}


	// Получить категории пользователя
	public static function getCategoriesUser($category = false)
	{
		// return self::array_to_object(unserialize(self::$config->categories));
		foreach(unserialize(self::$config->categories) as $id => $item)
		{
			if($category && strcasecmp($category, $item['title']) === 0)
				return (object) $item;
			$result[] = (object) $item;
		}

		if(!$category)
			return $result;
	}


	// Получить категории форумов пользователя
	public static function getCategoriesForumsUser()
	{
		if(empty(self::$config->categoriesForums))
			return false;
		// return self::array_to_object(unserialize(self::$config->categories));
		foreach(unserialize(self::$config->categoriesForums) as $id => $item)
		{
			$result[] = (object) $item;
		}
		return $result;
	}

	public static function array_to_object($data)
	{
	    if(is_array($data) || is_object($data))
	    {
	        $result = (object) array();
	        foreach($data as $key => $value)
	        {
	            $result->{$key} = self::array_to_object($value);
	        }
	        return $result;
	    }
	    return $data;
	}

	public static function getTubesWithCategories($type = false, $toStr = false)
	{
		if($type)
			$tubesObj = Tubes::findAll(['type' => $type]);
		else
			$tubesObj = Tubes::find()->all();

		if($toStr)
		{
			foreach($tubesObj as $key => $item)
			{
				$tubes[$item->id] = $item->name;
			}
		}
		else
		{
			foreach($tubesObj as $key => $item)
			{
				$tubes[$item->id]['id'] = $item->id;
				$tubes[$item->id]['name'] = $item->name;
				
				$categories = false;
				if($categories = Yii::$app->cache->get($item->id.'-categories'))
				{
					$tubes[$item->id]['categories'] = $categories;
				}
				$tubes[$item->id] = (object) $tubes[$item->id];
			}
		}

		return (object) $tubes;
	}

	public static function getTubes($id = false)
	{
		if($id)
			return Tubes::findOne(['id' => $id]);
		else
			return Tubes::find()->all();
	}

	// Возвращает спсок названий категорий публикаций и колличество роликов в них
	public static function getCountArhive()
	{
		foreach(self::getCategoriesUser() as $category)
		{
			$countToSite = $countToForum = $complete = 0;
			$all = Archive::findAll(['user_id' => self::$user->id, 'category' => strtolower($category->title)]);
			foreach($all as $item)
			{
				if($item->status == 4 || $item->status == 2)
					if($item->statusPublicationSite == null)
						$countToSite++;
					else
						$countToForum++;
			}

			$categories[] = [
				'name' => $category->title,
				'countToSite' => $countToSite,
				'countToForum' => $countToForum,
				'countAll' => count($all),
			];
		}

		return $categories;
	}

	// Возвращает спсок торрентов
	public static function getCategoriesList($id = false)
	{
		if($id)
			return Categories::findOne(['id' => $id]);
		return Categories::find()->orderBy(['title'=>SORT_ASC])->all();
	}

	// Возвращает спсок торрентов
	public static function getCategories2()
	{
		$categoriesList = self::getCategoriesList();
		foreach($categoriesList as $category)
		{
			$result[$category->id] = [
				'content' => $category->title,
				'options' => [
					'all' => $category->categories,
				],
			];
		}
		return $result;
	}

	// Возвращает спсок торрентов
	public static function getCategories()
	{
		return self::getCategoriesList();
		// $categoriesList = self::getCategoriesList();
		// foreach($categoriesList as $category)
		// {
		// 	$result[$category->id] = $category->title;
		// 	// $result[$category->id] = $category->title.' ['.$category->categories.']';
		// }
		// return $result;
	}

	// Возвращает конфиг юзера
	public static function getConfig()
	{
		// Если нет сохраненных настроек, то редирект на страничку создания настроек
		if(!$config = ConfigUser::findOne(['user_id' => self::$user->id]))
		{
			return false;;
		}
		$config->proxy = 'mN6xp2:RiT6MZfZnw@5.188.156.30:3000';
		return $config;
	}

	// Возвращает список путей к папкам TEMP
	public static function getTemp()
	{
		$folder = new \StdClass;
		// Пути
		$userFolder = self::$user->email;
		// if(is_a(Yii::$app, 'yii\console\Application'))
			
		$temp = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
		$tempUser = $temp.$userFolder.DIRECTORY_SEPARATOR;
		$return = (object) [
			'downloads' => $temp.'downloads'.DIRECTORY_SEPARATOR,
			// 'downloads' => '/var/lib/transmission-daemon/Downloads'.DIRECTORY_SEPARATOR,
			'temp' => $temp,
			'main' => $tempUser,
			'txt' => $tempUser.'txt'.DIRECTORY_SEPARATOR,
			'video' => $tempUser.'video'.DIRECTORY_SEPARATOR,
			'publication' => $tempUser.'publication'.DIRECTORY_SEPARATOR,
			'logs' => $tempUser.'logs'.DIRECTORY_SEPARATOR,
		];

		if(!is_dir($return->txt))
			@mkdir($return->txt, 0777, 1);
		if(!is_dir($return->video))
			@mkdir($return->video, 0777, 1);
		if(!is_dir($return->downloads))
			@mkdir($return->downloads, 0777, 1);
		if(!is_dir($return->publication))
			@mkdir($return->publication, 0777, 1);

		return $return;
	}

	// Удаляет текстовый файл
	public static function deleteTxtFile($fileName)
	{
		if(unlink(self::getTemp()->txt.$fileName))
			return true;
	}

	// Скачивает текстовый файл
	public static function downloadTxtFile($fileName)
	{
		$file = self::getTemp()->txt.$fileName;
		if(file_exists($file))
		{
			header("Content-Length: ".filesize($file));
			header("Content-Disposition: attachment; filename=".$fileName); 
			header("Content-Type: application/x-force-download; name=\"".$file."\"");
			readfile($file);
		}
	}

	// Получает список текстовых файлов
	public static function getTxtFilesList()
	{
		$folder = self::getTemp();
		// $categories = self::getCountArhive();
		// $files = glob($folder->txt.'*.txt');

		if(!empty($folder->txt))
		{
			foreach(glob($folder->txt.'*.txt') as $item)
			{
				$fopen = fopen($item, 'r');
				$content = fread($fopen, filesize($item));
				$explode = explode(self::$config->txtFilesDelimer, $content);
				$files[] = [
					'name' => basename($item),
					'path' => $item,
					'count' => count($explode)-1,
				];
			}
			return @$files;
		}
	}

	// Получает список текстовых файлов
	public static function getTotalCountPosts()
	{
		return count(Archive::findAll(['user_id' => self::$user->id, 'status' => 2]));
	}

	// Получает список текстовых файлов
	public static function getTotalCountPostsPay()
	{
		return self::$user->pay;
	}

	// Получает список текстовых файлов
	public static function getHavePosts()
	{
		return self::getTotalCountPostsPay() - self::getTotalCountPosts();
	}

	// Добавляет запись как игнор
	public static function addIgnore($item, $error = '', $code = 3)
	{
		$archive = new Archive();
		$links = [
			@$item['fileLink'],
			@$item['posterLink'],
		];
		$archive->url = $item['url'];
		$archive->category = $item['info']['category'];
		$archive->tube = $item['tube'];
		$archive->info = serialize($item['info']);
		$archive->downloadLinks = serialize($links);
		$archive->alert = $error;
		$archive->status = $code;
		$archive->user_id = self::$user->id;
		if(@$archive->save())
			Main::echoTxt('Видео добавлено в игнор - '.$item['url'].': '.$error);
		else
			Main::echoTxt('Ошибка добавления в бд - '.$item['url'].': '.$error);
	}

	// Удаляет файлы и папки
	public static function clearFiles($path, $removeSelf = false)
	{
		if(is_file($path))
		{
			@unlink($path);
			return 'Файл удален! - '.basename($path);
		}
		else
		{
			$items = glob($path."*");
			foreach($items as $item)
			{
			    self::removeDirectory($item);
			}
			if($removeSelf)
				@rmdir($path);
			return 'Файлы в папке и папка удалены! - '.basename($path);
        }
	}

	// Удаляет все в папке рекурсивно
	public static function removeDirectory($path)
	{
		if(is_file($path))
		{
			unlink($path);
		}
		if($items = glob($path."/*"))
		{
			foreach($items as $item)
			{
				is_dir($item) ? self::removeDirectory($item) : unlink($item);
			}
		}
		@rmdir($path);
	}

    /**
     * Авторизируется и сохраняет куки файл
     *
     * @param array $data [url, proxy, cookieFile, postData]
     * @return str cookieFile
     */
	public static function autorization($data, $need = false, $saveToFile = false)
	{
		$domain = parse_url($data['url'], PHP_URL_HOST);
		$cookieFile = self::getTemp()->main.$domain.'['.current($data['postData']).']';

		if(file_exists($cookieFile) && !$need)
			return $cookieFile;

		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport'
		]);
		$response = $client->post($data['url'], $data['postData'], [], [
			CURLOPT_COOKIEJAR => $cookieFile,
			CURLOPT_FOLLOWLOCATION => 1,
		    CURLOPT_SSL_VERIFYPEER => 0,
		    CURLOPT_SSL_VERIFYHOST => 0,
		    CURLOPT_REFERER => $data['url'],
		    CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
		    'proxy' => @$data['proxy'],
		])->send();

        // echo $response->content; exit();

		if($saveToFile)
		{
			$pathSaveFile = self::getTemp()->publication.$response->getStatusCode().'-'.$domain.'['.current($data['postData']).']-auth.html';
			file_put_contents($pathSaveFile, $response->content);
			// exit();
		}

		if(@$response->isOk && file_exists($cookieFile))
		{
			// echo $response->content; exit;
			// Если куки прошлого дня, то пересоздаем
			// if(date("Ymd", filemtime($cookieFile)) < date("Ymd"))
			// 	self::autorization($data, true);
			return $cookieFile;
		}
		else
		{
			Main::echoTxt('Autorization error: '.$response->getStatusCode().' - '.$data['url']);
			// echo $response->content; //exit;
		}
		return false;
	}

    public static function getPageFromCurl($url, $cookieFile = false, $returnHTMLDOM = false)
    {
        $headers = [
            'Referer:'.$url,
            'User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
        ];

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport',
        ]);

        $requests = $client->createRequest()
            ->setUrl($url)
            ->setHeaders($headers)
            ->setOptions([
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_VERBOSE => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_COOKIEFILE => @$cookieFile,
            ])
            ->send();

        if(@$requests->isOk)
        {
            if($returnHTMLDOM)
                return HtmlDomParser::str_get_html($requests->content);
            return $requests->content;
        }
        else
            return false;
    }

	// Найти текст на страничке по clss или id
	public static function getText($html, $class)
	{
		return trim(@$html->find($class, 0)->plaintext);
	}

	// Ввод капчи
	// public function captcha()
	// {
	// 	$set = "http://rucaptcha.com/in.php?key=494779a4f1905ffed3bdc61c7f3ae115&method=userrecaptcha&googlekey=6LeY4gsUAAAAANITYkv2gPI8eEu8am3TCOr4B6j7&json=1&pageurl=ma.brazzers.com";
	// 	$ch = curl_init($set);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	// 	$requestSet = json_decode(curl_exec($ch));
	// 	curl_close($ch);

	// 	do
	// 	{
	// 		$get = "http://rucaptcha.com/res.php?key=494779a4f1905ffed3bdc61c7f3ae115&action=get&json=1&id=".$requestSet->request;
	// 		$ch = curl_init($get);
	// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	// 		$requestGet = json_decode(curl_exec($ch));
	// 		curl_close($ch);
	// 		sleep(1);
	// 	}
	// 	while($requestGet->status !== 1);
	// 	return $requestGet->request;
	// }

    /**
     * Устанавливает прокси
     *
     * @param string $proxy
     * @return array self::proxy
     */
	// public function setProxy($proxy)
	// {
	// 	if(self::checkProxy($proxy))
	// 	{
	// 		$explode = explode('@', $proxy);
	// 		if(isset($explode[1]))
	// 		{
	// 			self::proxy['url'] = $explode[1];
	// 			self::proxy['auth'] = $explode[0];
	// 		}
	// 		else
	// 		{
	// 			self::proxy['url'] = $explode[0];
	// 		}
	// 	}
	// 	return false;
	// }


    /**
     * Проверяет прокси
     *
     * @param string $proxy
     * @return bool
     */
	public static function checkProxy($proxy)
	{
		$proxy1 = explode('@', $proxy); 
		$pr = @$proxy1[1] ?: @$proxy1[0];
		$splited = explode(':', $pr);
		if($con = @fsockopen($splited[0], $splited[1], $error_code, $error_str, 10)) 
		{
			fclose($con);
			return true;
		}
		return false;
	}


    /**
     * test
     *
     * @param string $txt
     * @param array[disableBR, ]
     * @return int
     */
	public static function echoTxt($txt = '', $params = array())
	{
		if(self::$consoleMode == 1)
		{
			if(@$params['br'])
			{
				echo $txt.PHP_EOL;
				return;
			}
			echo date('Y/m/d H:i:s').' - '.$txt.PHP_EOL;
		}
		elseif(self::$consoleMode == 2)
		{
			
		}
		else
		{
			if(@$params['disableBR'])
				$echo = $txt;
			else
				$echo = $txt.'<br>';

			if(@!self::$viewConsoleLog)
			{
				if(self::$bufer < 500)
				{
					echo $echo;
					echo str_repeat(' ',1024*1024*4);
					flush();
					@ob_flush();
					self::$bufer++;
				}
				else
					echo $echo;
			}
			else
			{
				Yii::$app->session->addFlash('alert', $echo);
			}
		}
	}

	/**
	 * Преобразует размер файла
	 *
	 * @param integer $size
	 * @return size
	 */
	public static function size($size)
	{
		$filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
		return $size ? round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
	}


    /**
     * dirSize
     *
     * @param string $dirname
     * @return int
     */
	public static function dirSize($dirname)
	{
        $totalsize=0;
        if($dirstream = @opendir($dirname))
        {
	        while(false !== ($filename = readdir($dirstream)))
	        {
	            if ($filename!="." && $filename!="..")
	            {
	                if(is_file($dirname."/".$filename))
	                $totalsize+=filesize($dirname."/".$filename);
	      
	                if(is_dir($dirname."/".$filename))
	                	$totalsize+=self::dirSize($dirname."/".$filename);
                }
            }
        	closedir($dirstream);
        }
        return $totalsize;
	}


    /**
     * test
     *
     * @param string $link
     * @return int
     */
	public static function test()
	{
		// $data['url'] = 'http://pimpandhost.com';
		// $data['postData']['LoginForm[loginId]'] = 'boomuo';
		// $data['postData']['LoginForm[password]'] = '890202g';
		// $data['postData']['LoginForm[rememberMe]'] = '1';
		// $data['postData']['_csrf'] = 'ZHZCZVV0ajM8HSgfOBYMYAglGAlnQFJDLz4FJGMWWn9TMAEXZAwaWg==';

		// $client = new Client([
		// 	'transport' => 'yii\httpclient\CurlTransport'
		// ]);
		// echo $cookieFile = self::getTemp()->main.'cookie[test].txt';
		// $response = $client->post($data['url'], $data['postData'], [], [CURLOPT_COOKIEJAR => $cookieFile])->send();

		// echo $response->content;

	}



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	// Перебор по всем методам
	public static function foreachItem($item)
	{
		// Yii::$app->cache->delete('item');

		Main::echoTxt('Получение метадата...');
		if(!$item['meta'] = self::getMeta($item))
		{
			Main::echoTxt('Метадата не получены');
			self::save($item, 3);
			return false;
		}

		Main::echoTxt('Получение постера...');
		if(!$item['poster']['path'] = self::getPoster($item))
		{
			Main::echoTxt('Постер не получен');
			self::save($item, 3);
			return false;
		}

		Main::echoTxt('Загрузка постера на ФХ...');
		if(!$item['poster']['upload'] = self::uploadPoster($item))
		{
			Main::echoTxt('Постер не загружен');
			self::save($item, 3);
			return false;
		}

		Main::echoTxt('Создание скриншота...');
		if(!$item['screenshot']['path'] = self::getScreenshot($item))
		{
			Main::echoTxt('Cкриншот не создан');
			self::save($item, 3);
			return false;
		}

		Main::echoTxt('Загрузка скриншота на ФХ...');
		if(!$item['screenshot']['upload'] = self::uploadScreenshot($item))
		{
			Main::echoTxt('Cкриншот не получен');
			self::save($item, 3);
			return false;
		}

		Main::echoTxt('Загрузка видео файла на файлообменники...');
		if(!$item['links'] = self::uploadFile($item))
		{
			print_r($item);
			self::save($item, 3);
			return false;
		}

		self::save($item);

		Main::echoTxt('Удаление файлов...');
		self::clearFilesComplete(@$item['download']['video']);
		// self::clearFiles(@$item['folder']);
		self::clearFilesComplete();
	}


	// Получить мета дата
	public static function getMeta($item)
	{
		$handler = new Handler;
		$result = $handler->getMetaFile($item['download']['video']);
		if(!@$result || @$handler->error)
		{
			self::$alert[]['danger'] = $handler->error;
			return false;
		}
		return $result;
	}


	// Создать постеры
	public static function getPoster($item, $count = 1)
	{
		if(!empty($item['posterLink']))
		{
			$file = [
				'fileName' => 'poster',
				'folder' => $item['folder'],
				'fileLink' => $item['posterLink'],
			];

			$config = Main::getConfig();
			$parser = new Parser;
			// $parser->poxy = self::$config->proxy;
			if($res = $parser->downloadFile($file, false))
			{
				if($res['mime'] == 'html')
				{
					Main::echoTxt('Скачан html файл вместо изрбражения');
				}
				else
				{
					$img1[] = $res;
					Main::echoTxt('Постер скачан');
					return $img1;
				}
			}
			else
				Main::echoTxt('Ошибка при скачивании постера');
		}

		$images = new Images;
		$images->sufix = 'poster'; // Название изображения
		$images->folder = $item['folder']; // Куда сохранять изображения
		$img2 = $images->createPosters($item['download']['video'], $count);
		Main::echoTxt('Постер создан');
		return $img2;
	}


	// Загрузить постер на фотохостинг
	public static function uploadPoster($item)
	{
		// Опции для форумов
		$options[self::$config->forumPosterHosting] = [
			'hosting' => self::$config->forumPosterHosting,
			'size' => self::$config->forumPosterHostingSize, // размер постера для хостинга
			'hyperlink' => 0, // постер как ссылка или нет
		];
		
		// Если есть сайты
		if($sites = ConfigSite::findAll(['user_id' => self::$user->id]))
		{
			foreach($sites as $site)
			{
				//Если такого фотохостинга еще нет
				if(!array_key_exists($site->hostingPoster, $options))
				{
					// Опции для сайтов
					$options[$site->hostingPoster] = [
						'hosting' => $site->hostingPoster,
						'size' => 'large',// размер постера для хостинга
						'hyperlink' => 0, // постер как ссылка или нет ($site->hyperlinkPoster)
					];
				}
			}
		}

		foreach($options as $hosting => $option)
			if($res = self::uploadImage($item['poster']['path'], $option))
				$result[$hosting] = $res;
			else
			{
				self::echoTxt('Не загружен постер на фохтинг - '.$hosting);
				exit();
			}

		return @$result;
	}


	// Создать скриншоты
	public static function getScreenshot($item, $count = 1)
	{
		$images = new Images;
		$images->sufix = 'screenshot';
		$images->folder = $item['folder'];
		$images->rows = self::$config->screenshotRows;
		$images->cols = self::$config->screenshotCols;
		$images->width = self::$config->screenshotWidth;
		return $images->getScreenshotOld($item['download']['video'], $count);
	}


	// Загрузить скриншоты на фотохостинг
	public static function uploadScreenshot($item)
	{
		// Опции для форумов
		$options[self::$config->forumScreenshotHosting] = [
			'hosting' => self::$config->forumScreenshotHosting,
			'size' => self::$config->forumScreenshotHostingSize, // размер постера для хостинга
			'hyperlink' => 1, // постер как ссылка или нет
		];

		// Если есть сайты
		if($sites = ConfigSite::findAll(['user_id' => self::$user->id]))
		{
			foreach($sites as $site)
			{
				//Если такого фотохостинга еще нет
				if(!array_key_exists($site->hostingScreenshot, $options))
				{
					// Опции для сайтов
					$options[$site->hostingScreenshot] = [
						'hosting' => $site->hostingScreenshot,
						'size' => 'medium',// размер постера для хостинга
						'hyperlink' => 1, // постер как ссылка или нет ($site->hyperlinkPoster)
					];
				}
			}
		}

		foreach($options as $hosting => $option)
			if(@$res = self::uploadImage($item['screenshot']['path'], $option))
				$result[$hosting] = $res;

		return @$result;
	}


	// Загрузить изображения на фотохостинг
	public static function uploadImage($imgPath, $options)
	{
		$images = new Images;
		$images->hosting = $options['hosting']; // Хостинг
		$images->size = $options['size']; // Размер превью
		$images->hyperlink = $options['hyperlink']; // Кликабельность картинки

		// Загружаем изображения на фоохостинг
		if(@$result = $images->upload($imgPath))
		{
			Main::echoTxt('Изображение загружено на хостинг: '.$options['hosting']);
			return $result;
		}
	}


	// Загружает файлы на файлообменник
	public static function uploadFile($item)
	{
		$temp = Main::getTemp();
		if(empty($accounts = unserialize(self::$config->accountFileHosting)))
		{
			Main::echoTxt('У вас нет ниодного файлообменника!');
			return false;
		}
		if(!is_array($accounts))
		{
			Main::echoTxt('У вас нет ниодного файлообменника!');
			return false;
		}

		// Изменение MD5 и закрытие fopen
		if(self::$config->md5Change)
		{
			if(is_writable($item['download']['video']))
			{
				$fopen = fopen($item['download']['video'], 'a');
				fwrite($fopen, "\n".crc32(self::$user->email));
				fclose($fopen);
				Main::echoTxt('MD5 файла изменен');
			}
			else
			{
				Main::echoTxt('md5 не изменен! Нет доступа к файлу: '.$item['download']['video']);
				return false;
			}
		}

		foreach($accounts as $account)
		{
			if($account['hosting'] == 'keep2share.cc')
			{
				$api = new Keep2ShareAPI($account['login'], $account['password'], $temp->main);

				Main::echoTxt('Загрузка файла на файлообменник: '.$account['hosting']);
				$probe = 1;
				while($probe < 4)
				{
					$res = $api->uploadFile($item['download']['video']);
					if(isset($res->user_file_id))
					{
						$result['preview.keep2share.cc'] = 'https://k2s.cc/preview/'.$res->user_file_id;
						$result['keep2share.cc'] = 'http://k2s.cc/file/'.$res->user_file_id;
						$result['fileboom.me'] = 'http://fboom.me/file/'.$res->user_file_id;
						break;
					}
					else
					{
						Main::echoTxt('Ошибка загрузки файла. Проба '.$probe);
						$probe++;
					}
				}
			}
			elseif($account['hosting'] == 'depfile.com')
			{
				Main::echoTxt('Загрузка файла на файлообменник: '.$account['hosting']);
				$api = new DepFileApi($account['login']);
				if(!@empty($api->uploadURL))
				{
					$probe = 1;
					while($probe < 4)
					{
						if($res = $api->uploadFile($item['download']['video']))
						{
							$result[$account['hosting']] = $res;
							break;
						}
						else
						{
							Main::echoTxt('Ошибка загрузки файла. Проба '.$probe);
							$probe++;
						}
					}
				}
				else
					Main::echoTxt('Сервис не работает');
			}
			// elseif($account['hosting'] == 'depfile.com')
			// {
			// 	Main::echoTxt('Загрузка файла на файлообменник: '.$account['hosting']);
			// 	$api = new DepFileApi($account['login'], $account['password']);
			// 	$probe = 1;
			// 	while($probe < 4)
			// 	{
			// 		if($res = $api->uploadFile($item['download']['video']))
			// 		{
			// 			$result[$account['hosting']] = $res;
			// 			break;
			// 		}
			// 		else
			// 		{
			// 			Main::echoTxt('Ошибка загрузки файла. Проба '.$probe);
			// 			$probe++;
			// 		}
			// 	}
			// }
		}
		return @$result;
	}


	//Создает текстовый файл
	public static function createTxt($item)
	{
		$item['posterLinks'] = $item['poster']['upload'][self::$config->forumPosterHosting];
		$item['screenshotLinks'] = $item['screenshot']['upload'][self::$config->forumScreenshotHosting];

		if($createTxt = Publication::createFileToForum($item, Main::getTemp()->txt, self::$config->txtFilesDelimer, self::$config->templateTxtFiles))
		{
			return true;
		}
		else
		{
			Main::echoTxt('Ошибка записи в текстовый файл');
			return false;
		}
	}


	// Публикаци на сайты
	public static function publication($item)
	{
		if(count($sites = ConfigSite::findAll(['user_id' => self::$user->id])) > 0)
		{
			foreach($sites as $site)
			{
				if(@!$site->stataus || $site->stataus === 0)
					continue;
				$item['posterLinks'] = $item['poster']['upload'][$site->hostingPoster];
				$item['screenshotLinks'] = $item['screenshot']['upload'][$site->hostingScreenshot];
				Main::echoTxt(Publication::addPost($site, $item));
			}
		}
		else
			Main::echoTxt('Нет сайтов для публикации');
	}


	/* status
		1 - 
		2 - опубликовано на сайтах
		3 - ignore all errors
		4 - ok
		5 - опубликовано на форумах
		6 - big torrent file
		7 - more torrent files
		8 - no peers
		9 - no title or actors or studio
	*/
	// Сохраняет в БД
	public static function save($item, $status = 4, $alert = null)
	{
		self::echoTxt('Сохранение данных в бд...');

		$links = [
			@$item['fileLink'],
			@$item['posterLink'],
		];

		$archive = new Archive();
		$archive->user_id = self::$user->id;
		$archive->url = $item['url'];
		$archive->category = $item['info']['category'];
		$archive->tube = $item['tube'];
		$archive->downloadLinks = serialize($links);
		if(@$item['info'])
			$archive->info = serialize($item['info']);
		if(@$item['meta'])
			$archive->meta = serialize($item['meta']);
		if(@$item['poster']['upload'])
			$archive->poster = serialize($item['poster']['upload']);
		if(@$item['screenshot']['upload'])
			$archive->screenshot = serialize(@$item['screenshot']['upload']);
		if(@$item['links'])
			$archive->links = serialize(@$item['links']);
		$archive->status = $status;
		$archive->dateTime = date('Y-m-d H:i:s');
		if(!@$archive->save())
		{
			self::$alert[]['danger'] = 'Ошибка при сохранении в базу данных';
			Main::echoTxt('Ошибка при сохранении в базу данных');
			echo '<pre>';
			print_r($archive->errors);
			print_r($archive);
			echo '</pre>';
			exit('exit');
			return false;
		}

		// Если статус == 3 (ошибка), удаляем все файлы
		if($status == 3)
		{
			if(@$item['download']['video'])
				self::clearFilesComplete($item['download']['video']);
			self::clearFilesComplete();
		}
		return true;
	}


	// Удаляет файлы
	public static function clearFilesComplete($filePath = false)
	{
		if($filePath)
		{
			if($txt = Main::clearFiles($filePath, true))
				Main::echoTxt($txt);
		}
		else
		{
			if($txt = Main::clearFiles(Main::getTemp()->video))
				Main::echoTxt($txt);
		}
	}


	// Проверяет прокси
	public static function actionCheckProxy($proxy)
	{
		if(Parser::checkProxy($proxy))
		{
			return '<span class="glyphicon glyphicon-ok-circle alert alert-success"></span>';
		}
		return '<span class="glyphicon glyphicon-remove-circle alert alert-danger"></span>';
	}

	public static function getCountAdd($day)
	{
		$rows = Archive::find()
			->where(['status' => 4])
			->andWhere(['like', 'dateTime', $day])
			->all();
		return count($rows);
	}
}