<?php

class Wordpress
{
	public $siteDomain;
	public $siteLogin;
	public $sitePassword;
	public $template;
	public $category;

	public $tempFolder;

	function __construct()
	{
		require_once("IXR_Library.php");
	}


	function addPosts($posts)
	{
		foreach($posts as $key => $data)
		{
			$result[$key] = $this->addPost($data);
		}
		return @$result;
	}

	function addPost($data)
	{
		// Create the client object 
		$client = new IXR_Client('http://'.$this->siteDomain.'/xmlrpc.php');

		$username = $this->siteLogin; 
		$password = $this->sitePassword; 

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$poster = $this->getPoster($data); // Путь к картинке
		// $poster = $this->getPosterPath($data); // Путь к картинке
		// $fh = fopen($poster, 'r'); // Открыли картинку
		// $fs = filesize($poster); // Размер картинки
		// $theData = fread($fh, $fs); // Считали картинку
		// fclose($fh); // Закрыли картинку

		// $client->debug = false; //Set it to false in Production Environment 
		// $params = array('name' => basename($poster), 'type' => 'image/jpg', 'bits' => new IXR_Base64($theData), 'overwrite' => true);
		// $res = $client->query('wp.uploadFile',1, $username, $password, $params);
		// $clientResponse = $client->getResponse();

		// //echo URL & image id 
		// $thumbnail_image =  $clientResponse['url'];  
		// $thumbnail_id = $clientResponse['id'];
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// $client->debug = true; //Set it to false in Production Environment 
		$title = $this->getTitle($data);
		$body = $this->getFullStory($data, $poster); // $body will insert your blog content (article content)
		$category = $this->getCatgory($data);
		$keywords = $this->getTags($data);
		$customfields = $this->getCustomFields($data); // Insert your custom values like this in Key, Value format 

		$title = htmlentities($title,ENT_NOQUOTES); 
		$keywords = htmlentities($keywords,ENT_NOQUOTES);
		$link = strtolower(substr($data['fileName'], 0, 40));

		$content1 = array( 
			'title' => $title, 
			'description' => $body, 
			'mt_allow_comments' => 1, // 1 to allow comments 
			'mt_allow_pings' => 1, // 1 to allow trackbacks 
			'post_type' => 'post',
			'link' => $link,
			'permaLink' => $link,
			'mt_keywords' => $keywords, 
			'categories' => $category,
			'custom_fields' => $customfields,
		    // 'wp_post_thumbnail' => $thumbnail_id, //thumbnail id of the picture, this will be marked as featured.
		);

		$params = array(0,$username,$password,$content1,true); // Last parameter is 'true' which means post immediately, to save as draft set it as 'false' 

		// Run a query for PHP 
		if(!$client->query('metaWeblog.newPost', $params))
		{ 
		    die('Something went wrong - '.$client->getErrorCode().' : '.$client->getErrorMessage()); 
		}
		else
		{
			return "Article Posted Successfully";
		}
	}

	function getCatgory($data)
	{
		$width = explode('x', $data['dimensions']);
		if($width[0] >= 1920)
		{
			$category[] = 'Full HD';
		}
		elseif($width[0] >= 1280)
		{
			$category[] = 'HD';
		}
		elseif($width[0] >= 640)
		{
			$category[] = 'SD';
		}
		elseif($width[0] < 640)
		{
			$category[] = 'MP4';
		}
		else
		{
			$category[] = 'no-category';
		}
		return $category;
	}

	function getTitle($data)
	{
		$tags = explode(', ', $data['tags']);
		switch($this->template)
		{
			case 1:
				if(isset($tags[0]) && !empty($tags[0]))
				{
					shuffle($tags);
					$tag = ' | ' . $tags[0];
				}
				else
				{
					$tag = ' | ' . $data['category'];
				}
				$title = $data['title'] . $tag . ' | [k2s, fboom]';
				break;
			case 2:
				$title = $data['title'] . ' [' . $data['category'] . ' / [k2s, fboom] / ' . date('Y') . ']';
				break;
			case 3:
				$title = $data['title'] . ' / [k2s, fboom]' . ' / ' . $data['category'];
				break;
			default:
				$title = $data['title'];
				break;
		}
		return $title;
	}

	function getPosterPath($data)
	{
		$rand = array_rand($data['posters']['postersPath'], 1);
		return $data['posters']['postersPath'][$rand];
	}

	function getPoster($data)
	{
		$rand = array_rand($data['posters']['posters_site'], 1);
		return $data['posters']['posters_site'][$rand]['url_img'];
	}

	function getScreenshot($data)
	{
		return htmlspecialchars_decode($data['screenshots']['screenshots_site'][0]);
	}

	function getTags($data)
	{
		return $data['tags'];
	}

	function getCustomFields($data)
	{
		// if(!empty($data['size']))
		// {
		// 	$res[] = array(
		// 		'key' => 'size',
		// 		'value' => $data['size']
		// 	);
		// }
		// if(!empty($data['duration']))
		// {
		// 	$res[] = array(
		// 		'key' => 'duration',
		// 		'value' => $data['duration']
		// 	);
		// }
		// if(!empty($data['dimensions']))
		// {
		// 	$res[] = array(
		// 		'key' => 'dimensions',
		// 		'value' => $data['dimensions']
		// 	);
		// }
		if(!empty($data['trailerHosingLink']))
		{
			$res[] = array(
				'key' => 'trailer',
				'value' => $data['trailerHosingLink']
			);
		}
		if(!empty($data['screenshots']['screenshots_site']))
		{
			$res[] = array(
				'key' => 'screenshot',
				'value' => $data['screenshots']['screenshots_site'][0]['html_prev_and_img'],
				// 'value' => '<a href="'.$data['screenshots']['screenshots_site'][0]['html_prev_and_img'].'" target="_blank" class="screenshot"><img src="'.$data['screenshots']['screenshots_site'][0]['thumb'].'"></a>'
			);
			// $res[] = array(
			// 	'key' => 'screenshot-link',
			// 	'value' => $data['screenshots']['screenshots_site'][0]['url_prev']
			// );
		}
		// if(!empty($data['links']['k2s']))
		// {
		// 	$res[] = array(
		// 		'key' => 'k2s',
		// 		'value' => $data['links']['k2s']
		// 	);
		// }
		// if(!empty($data['links']['fboom']))
		// {
		// 	$res[] = array(
		// 		'key' => 'fboom',
		// 		'value' => $data['links']['fboom']
		// 	);
		// }

		return $res;
	}

	function getFullStory($data, $img)
	{
		$txt = '<img src='.$img.' alt="poster-'.$data['title'].'" class="poster">';
		$txt .= '<br> ';
		$txt .= '<!--more-->';
		// $txt .= '<img src='.$img.' alt="poster-'.$data['title'].'" class="poster">';
		if(isset($data['description']) && !empty($data['description']))
		{
			$txt .= '<p><strong>Description:</strong> ' . $data['description'].'</p>';
		}
		if(isset($data['actors']) && !empty($data['actors']))
		{
			$txt .= '<strong>Actors:</strong> ' . $data['actors'].'<br>';
		}

		// $txt .= '<strong>Photoset info:</strong><br>';
		// $txt .= '<strong>Count photo:</strong> ' . $data['countImagesPack'].'<br>';
		// $txt .= '<strong>Size:</strong> ' . $data['sizeImagesPack'].' mb<br><br>';
		// $txt .= '<strong>Download porn photoset:</strong><br>';
		// $txt .= "<strong>[url=".$data['imgPackLinks']['k2s']."]Keep2share Download Photoset Link[/url]</strong><br>";
		// $txt .= "<strong>[url=".$data['imgPackLinks']['fboom']."]FileBoom Download Photoset Link[/url]</strong><br><br>";

		$txt .= '<br>';
		$txt .= '<br><strong>Video file info:</strong><br>';
		$txt .= '<strong>File name:</strong> ' . $data['fileName'].'<br>';
		$txt .= '<strong>Dimensions:</strong> ' . $data['dimensions'].'<br>';
		$txt .= '<strong>Format:</strong> MPEG-4<br>';
		$txt .= '<strong>Duration:</strong> ' . $data['duration'].'<br>';
		$txt .= '<strong>Size:</strong> ' . $data['size'].'<br>';
		$txt .= '<br>';
		
		// if(isset($data['trailerHosingLink']))
		// {
		// 	$txt .= '<div id="trailer-video"><strong>Trailer</strong></div>';
		// 	$txt .= '<br>';
		// }

		if(isset($data['screenshots']['screenshots_site']))
		{
			if(is_array($data['screenshots']['screenshots_site']))
			{
				foreach ($data['screenshots']['screenshots_site'] as $screenshot)
				{
					$txt .= htmlspecialchars_decode($screenshot['html_prev_and_img']).'<br>';
				}
			}
			else
			{
				$txt .= htmlspecialchars_decode($data['screenshots']['screenshots_site']['html_prev_and_img']).'<br>';
			}
		}

		$txt .= '<blockquote>';
		$txt .= '<h2>Download Keep2share && FileBoom '.$data['category'].' porn video: '.$data['title'].'</h2><br>';
		
		// k2s
		if(is_array($data['links']['k2s']))
		{
			$i = 1;
			foreach ($data['links']['k2s'] as $link)
			{
				$txt .= "<strong><a href=".$link.">Keep2share Download Link Part ".$i."</a></strong><br>";
			}
		}
		else
		{
			$txt .= "<strong><a href=".$data['links']['k2s'].">Keep2share Download Link</a></strong><br>";
		}

		// fboom
		if(is_array($data['links']['fboom']))
		{
			$i = 1;
			foreach ($data['links']['fboom'] as $link)
			{
				$txt .= "<strong><a href=".$link.">FileBoom Download Link Part ".$i."</a></strong><br>";
			}
		}
		else
		{
			$txt .= "<strong><a href=".$data['links']['fboom'].">FileBoom Download Link</a></strong><br>";
		}
		$txt .= '</blockquote>';

		return $txt;
	}
}