<?php

namespace app\models\Parser;

use Yii;
use app\models\Archive;
use app\models\ConfigSite;

set_time_limit(60*60*6);

class Index
{
	public $config; 
	public $user; 


	// Получить категории пользователя
	public function getCategories()
	{
		return unserialize($this->config->categories);
	}


	// Перебор по всем методам
	public function foreachItem($item)
	{
		// Yii::$app->cache->delete('item');

		Main::echoTxt('Получение метадата...');
		$item['meta'] = $this->handler($item);

		Main::echoTxt('Получение постера...');
		$item['poster']['path'] = $this->getPoster($item);
		Main::echoTxt('Загрузка постера на ФХ...');
		if(!$item['poster']['upload'] = $this->uploadPoster($item))
			return false;
		else
		{
			Main::echoTxt('Сохранение данных в бд...');
			$this->save($item, 3);

		}

		Main::echoTxt('Создание скриншота...');
		if($item['screenshot']['path'] = $this->getScreenshot($item))
		{
			Main::echoTxt('Загрузка скриншота на ФХ...');
			$item['screenshot']['upload'] = $this->uploadScreenshot($item);

			Main::echoTxt('Загрузка видео файла на файлообменники...');
			if(!$item['links'] = $this->uploadFile($item))
			{
				echo 'Error uploadFile<pre>';
				print_r($item);
				echo '</pre>';
				exit();
			}

			Main::echoTxt('Сохранение данных в бд...');
			$this->save($item);

			// Main::echoTxt('Создание файла для форумов...');
			// $this->createTxt($item);
		}
		else
		{
			Main::echoTxt('Сохранение данных в бд...');
			$this->save($item, 3);

		}

		Main::echoTxt('Удаление файлов...');
		$this->clearFiles(@$item['download']['video']);
		// $this->clearFiles(@$item['folder']);
		$this->clearFiles();
	}


	// Получить мета дата
	public function handler($item)
	{
		$handler = new Handler;
		$result = $handler->getMetaFile($item['download']['video']);
		if(!@$result || @$handler->error)
		{
			$this->alert[]['danger'] = $handler->error;
			return false;
		}
		return $result;
	}


	// Создать постеры
	public function getPoster($item, $count = 1)
	{
		if(!empty($item['posterLink']))
		{
			$file = [
				'fileName' => $item['fileName'].'poster',
				'folder' => $item['folder'],
				'fileLink' => $item['posterLink'],
			];

			$config = Main::getConfig();
			$parser = new Parser;
			// $parser->poxy = $this->config->proxy;
			if($img1[] = $parser->downloadFile($file, false))
			{
				Main::echoTxt('Постер скачан');
				return $img1;
			}
			else
				Main::echoTxt('Ошибка при скачивании постера');
		}

		$images = new Images;
		$images->sufix = 'poster'; // Название изображения
		$images->folder = $item['folder']; // Куда сохранять изображения
		$img2 = $images->createPosters($item['download']['video'], $count);
		Main::echoTxt('Постер создан');
		return $img2;
	}


	// Загрузить скриншоты на фотохостинг
	public function uploadPoster($item)
	{
		$hostings[$this->config->forumPosterHosting] = $this->config->forumPosterHostingSize;
		if($sites = ConfigSite::findAll(['user_id' => Yii::$app->user->identity->id]))
		{
			foreach($sites as $site)
			{
				if(!array_key_exists($site->hostingPoster, $hostings))
					$hostings[$site->hostingPoster] = 'large';
			}
		}

		foreach($hostings as $hosting => $size)
			if(@$res = $this->uploadImage($item['poster']['path'], $hosting, $size))
				$result[$hosting] = $res;

		return $result;
	}


	// Создать скриншоты
	public function getScreenshot($item, $count = 1)
	{
		$images = new Images;
		$images->sufix = 'screenshot';
		$images->folder = $item['folder'];
		$images->rows = $this->config->screenshotRows;
		$images->cols = $this->config->screenshotCols;
		$images->width = $this->config->screenshotWidth;
		return $images->getScreenshotOld($item['download']['video'], $count);
	}


	// Загрузить скриншоты на фотохостинг
	public function uploadScreenshot($item)
	{
		$hostings[$this->config->forumScreenshotHosting] = $this->config->forumScreenshotHostingSize;
		if($sites = ConfigSite::findAll(['user_id' => Yii::$app->user->identity->id]))
		{
			foreach($sites as $site)
			{
				if(!array_key_exists($site->hostingScreenshot, $hostings))
					$hostings[$site->hostingScreenshot] = 'small';
			}
		}

		foreach($hostings as $hosting => $size)
			if(@$res = $this->uploadImage($item['screenshot']['path'], $hosting, $size))
				$result[$hosting] = $res;

		return $result;
	}


	// Загрузить изображения на фотохостинг
	public function uploadImage($imgPath, $hosting, $size)
	{
		$images = new Images;
		$images->hosting = $hosting; // Хостинг
		$images->size = $size; // Размер превью

		// Загружаем изображения на фоохостинг
		if(@$result = $images->upload($imgPath))
		{
			Main::echoTxt('Изображение загружено на хостинг: '.$hosting);
			return $result;
		}
	}


	// Загружает файлы на файлообменник
	public function uploadFile($item)
	{
		$temp = Main::getTemp();
		if(empty($accounts = unserialize($this->config->accountFileHosting)))
			throw new \Exception('У вас нет ниодного файлообменника!');
		if(!is_array($accounts))
			throw new \Exception('У вас нет ниодного файлообменника!');

		// Изменение MD5 и закрытие fopen
		if($this->config->md5Change)
		{
			if(is_writable($item['download']['video']))
			{
				$fopen = fopen($item['download']['video'], 'a');
				fwrite($fopen, "\n".crc32($this->user->email));
				fclose($fopen);
				Main::echoTxt('MD5 файла изменен');
			}
			else
			{
				throw new \Exception('md5 не изменен! Нет доступа к файлу: '.$item['download']['video']);
			}
		}

		foreach($accounts as $account)
		{
			if($account['hosting'] == 'keep2share.cc')
			{
				$uploadfiles = new UploadFiles();
				$uploadfiles->k2s_user = $account['login'];
				$uploadfiles->k2s_pass = $account['password'];
				$uploadfiles->tempFolder = $temp->main;
				$uploadfiles->archivesSize = $this->config->archivesSize;
				$uploadfiles->archivesSizeCheck = $this->config->archivesSizeCheck;
				$uploadfiles->uploadInFolder = $this->config->uploadInFolder;

				Main::echoTxt('Загрузка файла на файлообменник: '.$account['hosting']);
				if($result = $uploadfiles->multiUploadFileOne($item['download']['video'], $item['info']['category']))
				{
					if($uploadfiles->error)
					{
						Main::echoTxt('Ощибка загрузки файла: '.$account['login'].' / '.$uploadfiles->error);
						// $this->alert[]['danger'] = $account['hosting'].' / '.$account['login'].' / '.$uploadfiles->error;
					}
					// $this->alert[]['success'] = 'Файл загружен на '.$account['hosting'].' / '.$account['login'];
				}
			}
		}
		return @$result;
	}


	//Создает текстовый файл
	public function createTxt($item)
	{
		$item['posterLinks'] = $item['poster']['upload'][$this->config->forumPosterHosting];
		$item['screenshotLinks'] = $item['screenshot']['upload'][$this->config->forumScreenshotHosting];

		if($createTxt = Publication::createFileToForum($item, Main::getTemp()->txt, $this->config->txtFilesDelimer, $this->config->templateTxtFiles))
		{
			return true;
		}
		else
		{
			Main::echoTxt('Ошибка записи в текстовый файл');
			return false;
		}
	}


	// Публикаци на сайты
	public function publication($item)
	{
		if(count($sites = ConfigSite::findAll(['user_id' => Yii::$app->user->identity->id])) > 0)
		{
			foreach($sites as $site)
			{
				if(@!$site->stataus || $site->stataus === 0)
					continue;
				$item['posterLinks'] = $item['poster']['upload'][$site->hostingPoster];
				$item['screenshotLinks'] = $item['screenshot']['upload'][$site->hostingScreenshot];
				Main::echoTxt(Publication::addPost($site, $item));
			}
		}
		else
			Main::echoTxt('Нет сайтов для публикации');
	}


	// Сохраняет в БД
	public function save($item, $status = 4, $alert = null)
	{
		$links = [
			@$item['fileLink'],
			@$item['posterLink'],
		];

		$archive = new Archive();
		$archive->user_id = Yii::$app->user->identity->id;
		$archive->url = $item['url'];
		$archive->category = $item['info']['category'];
		$archive->tube = $item['tube'];
		$archive->downloadLinks = serialize($links);
		if(@$item['info'])
			$archive->info = serialize($item['info']);
		if(@$item['meta'])
			$archive->meta = serialize($item['meta']);
		if(@$item['poster']['upload'])
			$archive->poster = serialize($item['poster']['upload']);
		if(@$item['screenshot']['upload'])
			$archive->screenshot = serialize(@$item['screenshot']['upload']);
		if(@$item['links'])
			$archive->links = serialize(@$item['links']);
		$archive->status = $status;
		$archive->dateTime = date('Y-m-d H:i:s');
		if(!$archive->save())
		{
			$this->alert[]['danger'] = 'Ошибка при сохранении в базу данных';
			Main::echoTxt('Ошибка при сохранении в базу данных');
			echo '<pre>';
			print_r($archive->errors);
			print_r($archive);
			echo '</pre>';
			exit();
		}
		return true;
	}


	// Удаляет файлы
	public function clearFiles($filePath = false)
	{
		if($filePath)
		{
			if($txt = Main::clearFiles($filePath, true))
				Main::echoTxt($txt);
		}
		else
		{
			if($txt = Main::clearFiles(Main::getTemp()->video))
				Main::echoTxt($txt);
		}
	}


	// Проверяет прокси
	public function checkProxy($proxy)
	{
		if(Parser::checkProxy($proxy))
		{
			return '<span class="glyphicon glyphicon-ok-circle alert alert-success"></span>';
		}
		return '<span class="glyphicon glyphicon-remove-circle alert alert-danger"></span>';
	}
}