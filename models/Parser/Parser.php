<?php

namespace app\models\Parser;

use Yii;
use app\models\Archive;
use app\models\Tubes;
use yii\httpclient\Client;
use LanguageDetection\Language;
use Sunra\PhpSimple\HtmlDomParser; // парсер
// use DiDom\Document; // парсер

class Parser
{
	// public $account; // Данные для авторизации
	public $userId; // Данные для авторизации
	public $configTube; // Конфигурация источника
	public $category; // Название категории которую парсим
	public $categories; // Список категорий ?
	public $keys; // Ключевые слова для поиска по тегам
	public $maxPaginationPage = 0; // На сколько страничек можно забегать по пагинации
	public $categoryTube; // Категория источника (для панигации) ?
	public $count = 100; // Сколько парсить роликов
	public $minSaveSize = 0; // Минимальный размер скачанного видео файла
	public $maxSaveSize = 1024*1024*1024*3; // Минимальный размер скачанного видео файла

	public $alert; //  Ошибка
	public $lastPageUrl = ''; // Ссылка на последнюю страничку

	public $login; // Выбранный текущий аккаунт для парсинга и скачивания
	public $account = false; // Выбранный текущий аккаунт для парсинга и скачивания

	// public $debug = 1; // Режим отладки

	private $pageText;

	protected $proxy; // прокси ип адрес
	protected $cookieFile; // куки
	protected $scanTotal = 0; //  Сколько вобщем просканированно ссылок
	protected static $mime; // MIME тип скачанного файла
	protected $patternIdentClass = '/(\.\w|#\w|\/\/)+/';


	/**
	 * Получение категорий источника
	 *
	 * @param string $this->configTube->categoriesListLinkTag
	 * @param string $this->configTube->url
	 * @return array
	 */
	public function getCategoriesTube()
	{
		// Если указан тег для поиска
		if(isset($this->configTube->categoriesListLinkTag) && !empty($this->configTube->categoriesListLinkTag))
		{
			$explode1 = explode(' ||| ', $this->configTube->categoriesListLinkTag); // Делим на

			$explode2 = explode(' || ', $explode1[0]); // Делим на элемент и атрибут

			$explode3 = explode(' | ', $explode2[0]); // Делим на элемент и ссылку по счету

			if(isset($explode1[0]) && !empty($explode1[0]))
			{
				// Если указана страничка для поиска категории то ищем в ней или берем главную страничку
				if(isset($explode1[1]) && !empty($explode1[1]))
				{
					$url = $explode1[1];
				}
				else
				{
					$url = $this->configTube->url;
				}

				if(isset($explode2[1]) && !empty($explode2[1]))
				{
					$attr = $explode2[1];
				}
				else
				{
					$attr = 'href';
				}

				if(!$page = $this->getPageFromCurl($url))
				{
					return false;
				}

				$search = $page->find($explode3[0]);
				if(count($search) === 0)
				{
					throw new \Exception('На страничке нет списка категорий - ' . $url);
				}
				// unset($url);

				// На 1 страничке ищем ссылки на странички с роликами
				foreach($search as $key => $element)
				{
					unset($cat);
					$url = trim($element->{$attr});
					// Если в строке не найден http и строка не является числом, то добавляем в начало адрес
					if(is_numeric($url))
					{
						$cat = $url;
						$url = $this->configTube->url . '/forum/' . $url;
					}
					elseif(preg_match('/^http/', $url) == false)
					{
						$url = $this->configTube->url . $url;
					}
					$title = $this->removeStr($element->plaintext, '/[|,-]+/');

					if(isset($explode3[1]) && !empty($explode3[1]))
					{
						if(preg_match('/=/', $explode3[1]))
						{
							if($expl = explode(trim($explode3[1]), $url))
								$cat = $expl[1];
						}
						else
						{
							$explode2 = explode('/', $url);
							$cat = $explode2[$explode3[1]];
						}

						$urls[] = [
							'title' => $title,
							'category' => $cat,
							'url' => $url,
						];
					}
					else
					{
						if(isset($cat))
						{
							$category = $cat;
						}
						else
						{
							$category = $this->rusTranslit($title);
						}
						$urls[] = [
							'title' => $title,
							'category' => $category,
							'url' => $url,
						];
					}
				}
				return $urls;
			}
		}
	}


	/**
	 * Возвращает HTML DOM дерево
	 *
	 * @param string $url - ссылка на страчнику
	 * @param string $returnStr - возвращать htmldoom
	 * @param string $this->proxy
	 * @param string $this->cookieFile
	 * @return object | string
	 */
	public function getPageFromCurl($url, $returnStr = true)
	{
		$headers = [
			'Referer:'.$url,
			'User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
		];

		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport',
		]);

		@$requests = $client->createRequest()
			->setUrl($url)
			// ->setMethod('POST')
			->setHeaders($headers)
			// ->setCookies($this->cookieFile)
			->setOptions([
				'timeout' => 60,
				'proxy' => @$this->proxy,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_VERBOSE => 0,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_COOKIEFILE => @$this->cookieFile,
			])
			->send();
		// echo $requests->content; exit;
		if(@$requests->isOk)
		{
			if($returnStr)
				return HtmlDomParser::str_get_html($requests->content);
			return $requests->content;
		}
		else
			return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Парсим файлы
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Получаем ссылку на страничку категории (с пагинацией)
	 *
	 * @param integer $number
	 * @param string $slush
	 * @param string $this->categoryTube
	 * @param string $this->configTube->categoriesPaginationPage
	 * @param string $this->configTube->mainPaginationPage
	 * @return string
	 */
	public function getPagePagination($number, $slush = false)
	{
		// Если указана категоря, то ищем по шаблону странички категории
		if(!empty($this->categoryTube) && !empty(@$this->configTube->categoriesPaginationPage)) 
		{
			$url = preg_replace('|\[category\]|', $this->categoryTube, $this->configTube->categoriesPaginationPage);
		}
		else // иначе ищем по шаблону главной странички
		{
			$url = $this->configTube->mainPaginationPage;
		}

		if(@empty($url))
		{
			Main::echoTxt('Не выбрана страничка панигации');
			return false;
		}
		// Добавляем номер странички
		$result = preg_replace('|\[number\]|', $number, $url);

		return $result;
	}


	/**
	 * Убираем лишние символы
	 *
	 * @param string $url
	 * @param string $sumbol
	 * @return string
	 */
	public function removeSumbol($str, $pattern = '-')
	{
		$str = preg_replace('/\.(com|net|ru|biz|tv|porn|sex|xxx|org)/i', '', $str); // Удаляем домены
		$str = preg_replace('/&nbsp;+/', '', $str); // Удаляем лишние символы
		$str = preg_replace("/[^a-zA-Z0-9]/ui", $pattern, $str);	// Удаляем лишние символы
		$str = preg_replace("/[\|]/ui", $pattern, $str);	// Удаляем лишние символы
		$str = preg_replace("/[$pattern]+/ui", $pattern, $str); // Заменяем один и более спец символа на один
		$str = preg_replace("/[$pattern]+$/ui", '', $str); // Заменяем один и более спец символа на один
		return trim($str);
	}


	/**
	 * Убираем домены из строки
	 *
	 * @param string $str
	 * @param string $pattern
	 * @return string
	 */
	public function removeStr($str, $pattern = false, $to = '', $translate = true)
	{
		$str = preg_replace('/\.(com|net|ru|biz|tv|porn|sex|xxx|org)/i', '', $str); // Удаляем домены
		$str = preg_replace('/&nbsp;+/', '', $str); // Удаляем лишние символы
		$str = preg_replace('/([\-_@|]+)/', '', $str); // Удаляем лишние символы
		$str = preg_replace('/[^a-zA-Z0-9а-яА-Я\.\,\s]/ui', '', $str);	// Удаляем лишние символы
		$str = preg_replace('/\s{2,}/', ' ', $str);	// Два и более пробелов на 1
		$str = trim($str);
		if($pattern)
			$str = preg_replace($pattern, $to, $str); //  Удаляем по шаблону

		if($translate && strlen($str) > 5)
		{
			if(function_exists('mb_detect_encoding') && mb_detect_encoding($str) != 'UTF-8')
				$str = iconv("Windows-1251", "UTF-8", $str);
			if(!empty($strTrans = $this->language($str)))
				$str = $strTrans;
		}

		return $str;
	}


	/**
	 * Переводим буквы на латиницу перебором букв
	 *
	 * @param string $string
	 * @return string
	 */
	public function rusTranslit($string)
	{
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		return strtr($string, $converter);
	}


	/**
	 * Переводим текст через переводчик google
	 * если не удачно то переводит через $this->rusTranslit()
	 *
	 * @param string $str
	 * @param string $lang_from
	 * @param string $lang_to
	 * @return string
	 */
	public function language($str = false)
	{
		if(!$str || empty($str) || $str === null)
			return false;

		$str = preg_replace('/[_,|,@,&,-]+/', '', $str);

		if(!empty($str))
		{
			$ld = new Language;
			$lang = $ld->detect($str)->bestResults()->__toString();
			if($lang != 'en')
			{
				return $this->translate($str, $lang);
			}
			return $str;
		}
		return false;
	}


	/**
	 * Переводим текст через переводчик google
	 * если не удачно то переводит через $this->rusTranslit()
	 *
	 * @param string $str
	 * @param string $lang_from
	 * @param string $lang_to
	 * @return string
	 */
	public function translate($str, $lang_from = 'ru', $lang_to = 'en')
	{
		$query_data = array(
			'client' => 'x',
			'q' => $str,
			'sl' => $lang_from,
			'tl' => $lang_to
		);
		$filename = 'http://translate.google.ru/translate_a/t';
		$options = array(
				'http' => array(
				'user_agent' => 'Mozilla/5.0 (Windows NT 6.0; rv:26.0) Gecko/20100101 Firefox/26.0',
				'method' => 'POST',
				'header' => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($query_data)
			)
		);

		$context = stream_context_create($options);
		@$response = file_get_contents($filename, false, $context);
		if(!empty($response))
			return json_decode($response);
		else
			return $this->rusTranslit($str);
	}


	/**
	 * Устанавливает конфиг туба
	 *
	 * @param integer $id
	 * @return object
	 */
	public function setConfigTube($id)
	{
		$this->configTube = Tubes::findOne(['id' => $id]);

		if(!@$this->configTube || $this->configTube == null)
			return false;
		
		// Если парсер выключен, пропускаем его
		if($this->configTube->status != 1)
		{
			Main::echoTxt('Источник отключен: '.$this->configTube->name);
			return false;
		}

		return $this->configTube;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////
    // Парсим файлы
    ////////////////////////////////////////////////////////////////////////////////


	/**
	 * Создает папку для скачивания файлов
	 *
	 * @param string $this->categories
	 * @param string $this->keys
	 * @param string $this->cookieFile
	 * @param string $this->categoryTube
	 * @return bool
	 */
	public function getItem()
	{
		// $this->categories = (array) $this->categories;
		// shuffle($this->categories); // Перемешиваем масив

		// Перебираем все источники
		foreach($this->categories as $cat)
		{
			$explode = explode('|', $cat['category']); // Разбиваем строку на истоник и категорию
			$tube = $explode[0];
			$category = $explode[1];

			$this->categoryTube = $category;
			$this->keys = $cat['keys'];
			$this->maxPaginationPage = @$cat['maxPaginationPage'];

			// Подклюаем настройки туба
			if(!$this->configTube = Tubes::findOne(['name' => $tube]))
			{
				Main::echoTxt('Ошибка получения найстроек для источника - ' . $tube);
				continue;
			}

			// Если парсер выключен, пропускаем его
			if($this->configTube->status != 1)
			{
				Main::echoTxt('Источник отключен: '.$tube);
				continue;
			}
			else
			{
				Main::echoTxt('Парсинг источника: '.$tube.'/'.$this->categoryTube);
			}

			// Авторизация (получение куки)
			if(($dataAuth = $this->getAuthData($this->configTube->name)) || $this->configTube->dataAuth)
			{
				if(!is_array($dataAuth))
				{
					Main::echoTxt('Нет аккаунта для авторизации');
					continue;
				}

				if(!$this->cookieFile = Main::autorization($dataAuth, 1))
				{
					Main::echoTxt('Авторизация не удалась');
					continue;
				}
			}

			// Если указаны ключевые слова
			if(!@empty($this->keys))
			{
				Main::echoTxt('Поиск по ключевым словам: ' .$this->keys);
			}

			// Парсим странички
			if(!$resultSingle = $this->parsing())
			{
				Main::echoTxt('Не найдены ролики в текущем истонике');
				continue;
			}

			if(!empty($this->alert))
			{
				Main::echoTxt('Ошибка парсинга: '.$this->alert);
				continue;
			}

			return $resultSingle;

		} // End foreach tubes

		return false;
	}


    /**
     * Парсинг категории
     *
     * @param int $count
     * @param int $this->proxy
     * @param int $this->alert
     * @param int $this->scanTotal
     * @param int $this->lastPageUrl
     * @return array
     */
    public function parsing($count = 1)
    {
    	// Если есть прокси, проверяем их
		if(!empty($this->proxy) && !Main::checkProxy($this->proxy))
			throw new \Exception('Ваш прокси: '.$this->proxy.' не работает!');

		$lastPage = 0;
		$info = [];
	    $probe = 0;

		// Перебираем странички категории пока нет ошибок в парсере или колличество получееных роликов достигнет нужного колличества
		while(count($info) <= $count)
		{
			if($probe > 100)
			{
				Main::echoTxt('Сделано 100 неудачных попыток найти ролик');
				break;
			}

			// Получаем список ссылок с очередной странички
			unset($urls);
			$lastPage++;
			if($this->maxPaginationPage > 0 && $lastPage >= $this->maxPaginationPage)
			{

				$this->alert = 'Максимальное кооличество страничек достигнуто - '.$this->maxPaginationPage;
				Main::echoTxt($this->alert);
	    		break;
			}
	    	if(!$urls = $this->getLinks($lastPage)) // Получаем в категории все ссылки на странички с роликами
	    	{
	    		$this->alert = 'На страничке не найдены ссылки';
				Main::echoTxt($this->alert);
	    		break;
	    	}

	    	// Если зеркальньная страничка или если нет ссылок
	    	if(isset($previewUrls) && $previewUrls == $urls)
	    	{
	    		$this->alert = 'Зеркальна ссылка! ' . $this->lastPageUrl;
				Main::echoTxt($this->alert);
	    		break;
	    	}

	        $previewUrls = $urls; // Запоминаем список ссылок для проверки на зеркальность в следующей итерации

	        // Перебираем список полученых ссылок
	        foreach($urls as $url)
	        {
				// Проверяем нет ли уже такого видоса в базе у пользователя
				$archive = Archive::findAll(['url' => $url, 'user_id' => $this->userId]);
				if(count($archive) > 0)
				{
					continue;
				}

				if($getInfo = $this->getAllInfo($url))
				{
					return $getInfo;
				}
				else
					$probe++;
	        }
		}
		return false;
    }

    /**
     * Сканируем страничку с анонсами постов на ссылки к полными постам
     *
     * @param int $lastPage
     * @param int $this->configTube->articlesListLinkTag
     * @param int $this->lastPageUrl
     * @return array
     */
	public function getLinks($lastPage)
	{
		$result = [];
		if(!@$page = $this->getPagePagination($lastPage))
			return false;
		if(!@$html = $this->getPageFromCurl($page))
			return false;

		$this->lastPageUrl = $page;

		$search = $html->find($this->configTube->articlesListLinkTag);
		if(count($search) === 0)
		{
			// throw new Exception('На страничке нет видео роликов - '.$page);
			$this->alert = 'На страничке нет видео роликов - ' . $page;
			return false;
		}

		// На страничке ищем ссылки на странички с роликами
		foreach($search as $key => $element)
		{
			$url = $element->href;
			if(!preg_match('/^http/', $url))
			{
				$url = $this->configTube->url . $url;
			}

			$result[] = $url;
		}

		return $result;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get info from full page
	////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Получаем всю инфу со странички с видео
     *
     * @param string $url
     * @param string $this->configTube->allInfo
     * @param string $this->configTube->posterLink
     * @param string $this->configTube->fileLink
     * @param string $this->keys
     * @param string $this->alert
     * @return array
     */
	public function getAllInfo($url)
	{
		// $url = 'http://pornolab.net/forum/viewtopic.php?t=2409943';
		// Открываем страничку
		if(!$this->html = $this->getPageFromCurl($url))
		{
			return false;
		}

		// Если указан скласс для осбасти где вся инфа
		if(!empty($this->configTube->allInfo))
			$this->pageText = $this->html->find($this->configTube->allInfo, 0)->plaintext;
		else
			$this->pageText = $this->html->plaintext;

		// Получаем заголовок
		$file['info']['title'] = $this->getTitle();
		// Получаем описание
		$file['info']['description'] = $this->getDescription();
		// Получаем теги
		$file['info']['tags'] = $this->getTags();
		// Указываем категорию
		$file['info']['category'] = $this->category;
		// Указываем название студии
		$file['info']['studio'] = $this->getStudio();
		// Получаем актеров
		$file['info']['actors'] = $this->getActors();
		// Получаем год
		$file['info']['year'] = $this->getYear();
		// Указываем url
		$file['url'] = $url;
		// Указываем название туба
		$file['tube'] = $this->configTube->name;
		// Делаем имя видео файлу
		$file['fileName'] = substr($this->removeSumbol($file['info']['title']), 0, 150);
		// Получаем актеров
		$file['folder'] = Main::getTemp()->video.crc32($url).'/';
		// // Делаем путь к торрент файлу
		// $file['torrentPath'] = $file['folder'].$file['fileName'].'.torrent';
		// Делаем путь к видео файлу
		// $file['filePath'] = $file['folder'].$file['fileName'];
		// Получаем ссылку на постер
		$file['posterLink'] = $this->getLink($this->configTube->posterLink);
		// Получаем ссылку на видео файл
		if(!$file['fileLink'] = $this->getLink($this->configTube->fileLink))
		{
			Main::addIgnore($file, $this->alert);
			return false;
		}

		if(@empty($file['info']['title']))
		{
			Main::addIgnore($file, 'Нет заголовка', 9);
			return false;
		}
		if(@empty($file['info']['tags']))
		{
			Main::addIgnore($file, 'Нет тегов', 9);
			return false;
		}
		if(@empty($file['info']['actors']) || $file['info']['actors'] == 'Undefined actors')
		{
			Main::addIgnore($file, 'Нет актрисы', 9);
			return false;
		}
		if(@empty($file['info']['studio']) || $file['info']['studio'] == 'Undefined studo')
		{
			Main::addIgnore($file, 'Нет студии', 9);
			return false;
		}
		// Если название файла пустое или короче 2 символов, возвращаем false
		if(@empty($file['fileName']) || strlen($file['fileName']) < 2)
		{
			Main::echoTxt('Название файла пустое или короткое: '.$file['url']);
			Main::addIgnore($file, 'Название файла пустое или короткое: '.$file['url'], 9);
			return false;
		}

		// Поиск ключевых слов в посте
		if(!@empty($this->keys))
		{
			$explodeKeys = explode(',', $this->keys);
			foreach($explodeKeys as $key)
			{
				if(preg_match('/'.trim(strtolower($key)).'/', trim(strtolower($file['info']['title']))))
				{
					// Main::echoTxt('Найден ключ в заголовке: '.$key);
					return $file;
				}
				foreach($file['info']['tags'] as $tag)
				{
					if(preg_match('/'.trim(strtolower($key)).'/', trim(strtolower($tag))))
					{
						// Main::echoTxt('Найден ключ в тегах: '.$key);
						return $file;
					}
				}
			}
			// Main::echoTxt('Фильтр по ключам не пройден! '.$url);
			return false;
		}
		return $file;
	}


    /**
     * Ищет совпадения в массиве
     *
     * @param array $key // Ключ, что ищем
     * @param array $array // Массив, где ищем
     * @param array $pattern
     * @return array
     */
	public function searchKey1($key, $pageText = false)
	{
		$pattern = str_replace(', ', '|', $key);
		if(preg_match("/(?:".$pattern.")\s*:?\s*([^а-яА-Я]*)/", $this->pageText, $res))
			return $res[1];
		return false;
	}

	public function searchKey($key, $array, $pattern = false)
	{
		// Делим ключи на массив
		$explode = explode(',', $key);
		
		// Перебираем каждый ключ
		foreach($explode as $value)
		{
			// Убираем лишние пробелы
			$value = trim($value);

			// Проверяем еслть ли ключ в массиве $array
			if(array_key_exists($value, $array))
			{
				// Отсекаем все, что после 2 пробелов
				$explode = explode('  ', $array[$value]);
				if($pattern)
					return $this->removeStr($explode[0], $pattern);
				return $this->removeStr($explode[0]);
			}
		}

		// Если ничего не вернули, то ищем дальше, уже по регулярному выражению
		// Перебираем каждый ключ
		foreach($explode as $value)
		{
			// Убираем лишние пробелы
			$value = trim($value);
			// Проверяем еслть ли ключ в массиве $array
			foreach($array as $arrKey => $arr)
			{
				if(preg_match("|$value|", $arrKey))
				{
					// Отсекаем все, что после 2 пробелов
					$explode = explode('  ', $array[$arrKey]);
					if($pattern)
						return $this->removeStr($explode[0], $pattern);
					return $this->removeStr($explode[0]);
				}
			}
		}
	}


    /**
     * Перебирает все элементы информации со странички и создает из них массив
     *
     * @param array $allInfo
     * @return array
     */
	public function perebor($allInfo)
	{
		// Получает текст элемента
		$value = $allInfo->plaintext;

		if(count($data = preg_split("/^(.+?) :/m", $value, -1, PREG_SPLIT_DELIM_CAPTURE)) < 2)
			if(count($data = preg_split("/^(.+?): /m", $value, -1, PREG_SPLIT_DELIM_CAPTURE)) < 2)
				if(count($data = preg_split("/^(.+?): /m", $value, -1, PREG_SPLIT_DELIM_CAPTURE)) < 2)
					$data = preg_split("/^(.+?):/m", $value, -1, PREG_SPLIT_DELIM_CAPTURE);

		array_unshift($data, 'Добавленный заголовок');
		$data = array_chunk($data, 2);
		$result = [];

		foreach($data as list($k, $v))
		{
			// Убирает все теги
			$v = strip_tags(htmlspecialchars_decode($v));
			$v = preg_replace('|(&#.{1,5};)+|', '', $v);
			$v = trim($v);
			$explode = explode(PHP_EOL, $v);
		    $result[trim($k)] = $explode[0];
		}

		return $result;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * Получить категорию на основе тегов
	 *
	 * @param string $tags
	 * @return string
	 */
	public function getCategoryFromTags($tags)
	{
		if(!empty($tags))
		{
			$tags = strtolower($tags);
			$categoriesSortIDStr = Main::getConfig()->categories;
			$categoriesObj = Main::getCategoriesList();
			$categoriesSortIDArr = explode(',', $categoriesSortIDStr);

			// Делаем массив с объекта в порядке сортировки
			foreach($categoriesObj as $item)
			{
				$categoriesSort[$item->id] = $item;
			}

			// Перебираем список категорий по приоритету
			foreach($categoriesSortIDArr as $id)
			{
				$categories = explode(',', $categoriesSort[$id]->categories);
				foreach($categories as $category)
				{
					if(preg_match('/.*'.trim($category).'.*/', $tags))
					{
						return ucfirst($categoriesSort[$id]->title);
					}
				}
			}
			return 'Any';
		}
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Поиск информации на страничке
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Ищем ссылку на файл
	public function getLink($str)
	{
		if(!empty($str))
		{
			// Ищем ссылку на постеры и открываем эту страницу
			$explode0 = explode(' | ', $str);

			if(@$explode0[1] == 'script')
				$link = $this->findLinkInScript($explode0[0]);
			else
				$link = $this->findLinkInHtml($explode0[0]);
		}
		return @$link;
	}

	// Ищем ссылки по тегу
	public function findLinkInHtml($pattern)
	{
		$explode = explode(' || ', $pattern); // Разделяем на тег и аттрибут
		$searchLink = trim($explode[0]);
		if(isset($explode[1]))
		{
			$explode1 = explode(' ||| ', trim($explode[1])); // Разделяем на аттрибут и номер ссылки
			$searchAttr = $explode1[0];
		}
		else
			$searchAttr = 'href';

		// Находим все ссылки на файлы
		foreach($this->html->find($searchLink) as $link)
		{
			$href = $link->{$searchAttr};

			if(substr($href, 0, 2) == '//')
			{
				$href = 'http:' . $href;
			}
			elseif(!preg_match('/^http/', $href) and substr($href, 0, 2) != '//')
			{
				$href = $this->configTube->url . $href;
			}

			$fileLink[] = $href;
		}

		// Если указан доп параметр
		if(isset($explode1[1]))
		{
			if(is_int($explode1[1])) // Если число то возвращаем этот елемент массива
				@$fileLink = $fileLink[$explode1[1]];
			elseif(is_string($explode1[1])) // Если строка то ищем по дааному слову в строке
			{
				if(@$fileLink)
				{
					// Перебор всех найденных ссылок
					foreach($fileLink as $link)
					{
						// Если в ссылке есть искомый фрагмент
						if(preg_match('/'.$explode1[1].'/', $link))
						{
							return $link;
						}
					}
				}
				$this->alert = 'Не найдена ссылка на файл';
				return false;
			}
		}

		if(@is_array($fileLink))
			$fileLink = $fileLink[0];

		return @$fileLink;
	}

	// Ищем ссылку на файл в скриптах JS
	public function findLinkInScript($pattern)
	{
		foreach ($this->html->find('script') as $value)
		{
			if(preg_match("|$pattern|", $value->innertext, $mathes))
			{
				$res = preg_replace('|\\\|', '', $mathes[1]);
				return $res;
			}
		}
		return false;
	}

	// Ищем заголовок
	public function getTitle()
	{
		// Ищем заголовок
		if(!empty($this->configTube->titleTag))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->titleTag))
			{
				$pattern = str_replace(', ', '|', $this->configTube->titleTag);
				if(preg_match("/(?:".$pattern.")\s*:?\s*([^а-яА-Я]*)/", $this->pageText, $res))
					$title = $res[1];
				elseif(preg_match("/([^а-яА-Я]*)\s*/", $this->pageText, $res))
					$title = $res[1];
				
				if(isset($title))
				{
					$title = $this->removeStr($title, '/([0-9,.]+)/');
				}
				else
					$title = false;
			}
			else
			{
				$title = trim(strip_tags($this->html->find($this->configTube->titleTag, 0)->plaintext));
				$explode = explode('|', $title);
				$title = trim($explode[0]);
				if(function_exists('mb_detect_encoding') && mb_detect_encoding($title) != 'UTF-8')
					$title = iconv("Windows-1251", "UTF-8", $title);

				// if($this->configTube->translateRusEng)
				// 	$title = $this->translate($title);
			}
			// echo $title.PHP_EOL;
			if($title && preg_match('/[a-zA-Zа-яА-Я]{3,}?/', $title) && $this->configTube->translateRusEng == 1)
				$title = $this->language($title);
		}

		if(@empty($title) || strlen(trim($title)) <= 2)
		{
			return false;
			// return 'Undefined title'; // Можно выбирать рандомный заголовок из списка
		}
		return $title;
	}

	// Ищем описание
	public function getCategory($tags)
	{
		if(@$this->category)
			$category = ucfirst($this->category);
		else
			$category = $this->getCategoryFromTags($tags);

		if($this->configTube->translateRusEng == 1)
			$category = $this->language($category);

		if(@empty($category))
			$category = 'Undefined category';
		return $category;
	}

	// Ищем описание
	public function getDescription()
	{	
		// Ищем описание
		if(!empty($this->configTube->descriptionTag))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->descriptionTag))
			{
				$pattern = str_replace(', ', '|', $this->configTube->descriptionTag);
				if(preg_match("/(?:".$pattern.")\s*:?\s*([^а-яА-Я]*)/", $this->pageText, $res))
					$description = $res[1];
			}
			else
			{
				$explode = explode(' | ', $this->configTube->descriptionTag);
				if(isset($explode[1]))
					$num = $explode[1];
				else
					$num = 0;

				if($description = $this->html->find($explode[0], $num))
				{
					$description = ucfirst(trim($description->plaintext));
					// if($this->configTube->translateRusEng)
					// 	$description = $this->translate($description);
				}
			}
			$description = $this->removeStr(@$description);
		}

		if(@empty($description))
			$description = 'No description';
		return $description;
	}

	// Ищем теги
	public function getTags()
	{
		if(!empty($this->configTube->tagsTag))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->tagsTag))
			{
				$pattern = str_replace(', ', '|', $this->configTube->tagsTag);
				if(preg_match("/(?:".$pattern.")\s*:?\s*([^а-яА-Я]*)/", $this->pageText, $res))
				{
					$tags1 = $this->removeStr($res[1]);
					$tags1 = preg_replace('/[\/,&]/', ', ', $tags1);
					$explode = explode(',', $tags1);
					foreach($explode as $tag)
						$tags[] = trim($tag);
				}
				else
					$tags = false;
			}
			else
			{
				foreach ($this->html->find($this->configTube->tagsTag) as $tag)
				{
					$text = $tag->plaintext;
					if(!empty($text) && !preg_match('/^http/', $text))
					{
						// $tags[] = ucwords($this->removeSumbol($text, ' '));
						if($this->configTube->translateRusEng == 1)
							$text = $this->removeStr($text);

						$tags[] = ucwords($text);
					}
				}
			}
		}

		if(@empty($tags))
			return false;
		return @$tags;
	}

	// Ищем год
	public function getYear()
	{
		$year = false;
		if(!empty($this->configTube->year))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->year))
			{
				$pattern = str_replace(', ', '|', $this->configTube->year);
				if(preg_match("/(?:".$pattern.")\s*:?\s*([^а-яА-Я]*)/", $this->pageText, $res))
				{
					$year = preg_replace("/([^0-9]+)/", '', $res[1]);
					if(empty($year))
						$year = false;
				}

			}
		}

		if(!$year || @empty($year))
			$year = date('Y');
		return $year;
	}

	// Ищем актрис
	public function getActors()
	{
		if(!empty($this->configTube->actor))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->actor))
			{
				$pattern = str_replace(', ', '|', $this->configTube->actor);
				if(preg_match("/(?:".$pattern.")\s*:?\s*([^а-яА-Я]*)/", $this->pageText, $res))
				{
					$actors1 = $this->removeStr($res[1]);
					$actors1 = preg_replace('/[\/,&]+/', ', ', $actors1);
					$actors1 = preg_replace('/ and /', ', ', $actors1);
					$explode = explode(',', $actors1);
					foreach($explode as $actor)
					{
						$actor = trim($actor);
						if(!empty($actor))
							$actors[] = $actor;
					}
				}
				else
					$actors = false;
			}
			else
			{
				$explode = explode(' | ', $this->configTube->actor);
				if(isset($explode[1]))
				{
					$num = $explode[1];
				}
				else
				{
					$num = 0;
				}

				if(!preg_match('/\|/', $this->configTube->actor))
				{
					foreach ($this->html->find($explode[0]) as $key => $value)
					{
						$actors[] = $value->plaintext;
					}
				}

				if($actorsArr = $this->html->find($explode[0], $num))
				{
					foreach($actorsArr as $actor)
					{
						$text = $actor->plaintext;
						if(!empty($text) && !preg_match('/^http/', $text))
						{
							$actorsRes[] = ucwords($this->removeStr($this->removeSumbol($text, ' ')));
						}
					}
				}
			}
		}

		if(@empty($actors))
			return ['Amateur'];
		return $actors;
	}

	// Ищем студию
	public function getStudio()
	{
		if(!empty($this->configTube->studio))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->studio))
			{
				$pattern = str_replace(', ', '|', $this->configTube->studio);
				if(preg_match("/(?:".$pattern.")\s*:?\s*([^а-яА-Я]*)/", $this->pageText, $res))
				{
					$studio1 = $this->removeStr($res[1]);
					$studio1 = preg_replace('/[\/,&]/', ', ', $studio1);
					$explode = explode(',', $studio1);
					foreach($explode as $stud)
					{
						if(preg_match('/- Год производства :/', $stud))
							$stud = preg_replace('/- Год производства :/', '', $stud);
						$stud = trim($stud);
						if(!empty($stud))
							$studio[] = $stud;
					}
				}
				else
					$studio = false;
			}
			else
			{
				$explode = explode(' | ', $this->configTube->studio);
				if(isset($explode[1]))
				{
					$num = $explode[1];
				}
				else
				{
					$num = 0;
				}

				if(!preg_match('/\|/', $this->configTube->studio))
				{
					foreach ($this->html->find($explode[0]) as $key => $value)
					{
						$studio[] = $value->plaintext;
					}
				}

				if($studios = $this->html->find($explode[0], $num))
				{
					foreach($studios as $studioItem)
					{
						$text = $studioItem->plaintext;
						if(!empty($text) && !preg_match('/^http/', $text))
						{
							$studio[] = ucwords($this->removeStr($this->removeSumbol($text, ' ')));
						}
					}
				}
			}
		}

		if(@empty($studio))
			return ['Amateur'];
		return $studio;
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Получить параметры для авторизации
	 *
	 * @param string $this->configTube->name
	 * @return object
	 */
	public function getAuthData($name = false)
	{
		// Если аккаунт уже был выбран ранее
		if(is_array($this->account))
			return $this->account;

		// if($nameAtr)
		// 	$name = $nameAtr;
		// else
		// {
		// 	if(!@$this->configTube->name)
		// 		return false;
		// 	$name = $this->configTube->name;
		// }

		// Получаем инпуты
		switch($name) {
			case 'k2s.cc':
				$dataAuth['url'] = 'https://k2s.cc/login.html';
				$dataAuth['attrLogin'] = 'LoginForm[username]';
				$dataAuth['attrPassword'] = 'LoginForm[password]';
				break;
			case 'fboom.me':
				$dataAuth['url'] = 'https://fboom.me/login.html';
				$dataAuth['attrLogin'] = 'LoginForm[username]';
				$dataAuth['attrPassword'] = 'LoginForm[password]';
				break;
			case 'pornolab.net':
				$dataAuth['url'] = 'http://pornolab.net/forum/login.php';
				$dataAuth['attrLogin'] = 'login_username';
				$dataAuth['attrPassword'] = 'login_password';
				$dataAuth['add']['login'] = 'Login';
				break;
			
			default:
				return false;
				break;
		}

		// Подставление данных и получение аккаунта для авторизации в источнике
		if(@empty($accounts = unserialize(Main::getConfig()->accountTubeSite)))
			return false;

		foreach($accounts as $acc)
		{
			$acc = (object) $acc;

		    if($acc->hosting == $this->configTube->name || preg_match("/$name/", $acc->hosting))
		    {
		    	$sessId = date('Y:m:d').$acc->login;
				$toDayAccounts = Yii::$app->cache->get('accounts-'.date('Y:m:d'));
				if(is_array($toDayAccounts) && in_array($acc->login, $toDayAccounts))
				{
					Main::echoTxt($acc->login.' - Аккаунт сегодня уже отработал');
					// continue;
				}

		    	$account['url'] = $dataAuth['url'];
		    	$account['postData'][$dataAuth['attrLogin']] = $acc->login;
		    	$account['postData'][$dataAuth['attrPassword']] = $acc->password;

		    	Yii::$app->cache->set('accountActiv', $acc->login);

		    	if(isset($dataAuth['add']))
		    		$account['postData'] += $dataAuth['add'];

		    	$this->login = $acc->login;
		    	$this->account = $account;
				Main::echoTxt($acc->login.' - Аккаунт выбран');
		    	return $account;
		    }
		}
	}


	/**
	 * Проверка размера файла перед скачиванием
	 *
	 * @param string $link
	 * @return integer
	 */
	public static function checkFileSize($link)
	{
		$headers = @get_headers($link, true);
		if(is_array(@$headers['Content-Length']))
		{
			foreach($headers['Content-Length'] as $value)
			{
				if($value != 0)
				{
					$headers['Content-Length'] = $value;
				}
			}
		}
		return round(@$headers['Content-Length'] / 1024 / 1024, 2);
	}


	/**
	 * Скачивает файл
	 *
	 * @param array $item
	 * @param string $method
	 * @param string $this->proxy
	 * @param string $this->cookieFile
	 * @return array
	 */
	public function downloadFile($item, $method = 'POST')
	{

		try
		{
			$item['filePath'] = $item['folder'].$item['fileName'];
			if(is_dir($item['folder']))
			{
				foreach(scandir($item['folder']) as $fileItem)
				{
					if($fileItem == '.' || $fileItem == '..')
						continue;

					if(strpos($fileItem, $item['fileName']) !== false)
					{
						$item['filePath'] = $item['folder'].$fileItem;
						if(filesize($item['filePath']) > 1024*5)
						{
							Main::echoTxt('Файл уже скачан: '.$fileItem);
							return $this->checkFile($item['folder'].$fileItem);
						}
					}
				}
			}

			if(preg_match('/k2s.cc/', $item['fileLink']))
			{
				// Авторизация (получение куки)
				if($dataAuth = $this->getAuthData('k2s.cc'))
				{
					if(!$this->cookieFile = Main::autorization($dataAuth))
					{
						Main::echoTxt('Авторизация не удалась');
						return false;
					}
				}	
			}
			elseif(preg_match('/ascat.porn/', $item['fileLink']))
			{
				$ch = curl_init($item['fileLink']);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        	curl_setopt($ch, CURLOPT_REFERER, $item['fileLink']);
				$res = curl_exec($ch);
				$info = curl_getinfo($ch);
				curl_close($ch);
				// print_r($info);
				$item['fileLink'] = $info['url'];

				// Авторизация (получение куки)
				if($dataAuth = $this->getAuthData('fboom.me'))
				{
					// $dataAuth['proxy'] = '218.186.25.63:3128';
					if(!$this->cookieFile = Main::autorization($dataAuth, false, 1))
					{
						Main::echoTxt('Авторизация не удалась');
						return false;
					}
				}
			}
			elseif(preg_match('/fboom.me/', $item['fileLink']))
			{
				// Авторизация (получение куки)
				if($dataAuth = $this->getAuthData('fboom.me'))
				{
					if(!$this->cookieFile = Main::autorization($dataAuth))
					{
						Main::echoTxt('Авторизация не удалась');
						return false;
					}
				}	
			}
			elseif(preg_match('/pornolab.net/', $item['fileLink']))
			{
				// Авторизация (получение куки)
				if($dataAuth = $this->getAuthData('pornolab.net'))
				{
					if(!$this->cookieFile = Main::autorization($dataAuth))
					{
						Main::echoTxt('Авторизация не удалась');
						return false;
					}
				}
			}

			if(!self::createFolder(dirname($item['filePath'])))
			{
				Main::echoTxt('Не смог создать папку: '.$item['filePath']);
				return false;
			}

			$client = new Client([
				'transport' => 'yii\httpclient\CurlTransport'
			]);

			$request = $client->createRequest()
				->setUrl($item['fileLink'])
				->setOptions([
					'proxy' => @$this->proxy,
					CURLOPT_REFERER => $item['fileLink'],
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_FOLLOWLOCATION => 1,
					// CURLOPT_VERBOSE => 0,
					CURLOPT_SSL_VERIFYPEER => 0,
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_TIMEOUT => 60*60*1, // Таймаут выполнения. 0 - для неограниченного времени
					CURLOPT_CONNECTTIMEOUT => 60*2, // Таймаут соеденения. 0 - для неограниченного времени
					CURLOPT_FILE => fopen($item['filePath'], 'w'),
					CURLOPT_COOKIEFILE => @$this->cookieFile,
				]);
			if($method)
				$request->setMethod($method);

			if($response = $request->send())
			{
				if($response->isOk)
				{
					return $this->checkFile($item['filePath']);
				}
				else
				{
					Main::echoTxt('Ошибка скачивания файла! Код: '.$response->getStatusCode().PHP_EOL.$item['fileLink']);
					return false;
				}
			}
			Main::echoTxt('Ошибка скачивания файла! '.$item['fileLink']);
			return false;
		}
		catch (\yii\httpclient\Exception $e)
		{
			Main::echoTxt('Exception Ошибка скачивания файла! Код: '.$e->getMessage());
			return false;
		}
	}


	/**
	 * Проверет файл. Получает тип файла, размер и переменовывает его
	 *
	 * @param array $item
	 * @return array
	 */
	private function checkFile($file)
	{
		if(file_exists($file))
		{

			$result['mime'] = self::getFileMimeType($file);
			$result['filePath'] = self::renameFile($file, $result['mime']);
			$result['size'] = filesize($result['filePath']);
			return $result;
		}
		else
		{
			Main::echoTxt('Файл не существует! - '.$file);
			return false;
		}
	}


	/**
	 * Переименовывает в соответствии типа файла
	 *
	 * @param string $file
	 * @return string
	 */
	private static function renameFile($file, $mime)
	{
		switch($mime)
		{
			case 'x-rar':
				$newFile = self::unRar($file);
				break;
			case 'mp4':
				$newFile = $file.'.mp4';
				rename($file, $newFile);
				break;
			case 'jpeg':
				$newFile = $file.'.jpg';
				rename($file, $newFile);
				break;
			case 'png':
				$newFile = $file.'.png';
				rename($file, $newFile);
				break;
			case 'x-bittorrent':
				if(preg_match('/.torrent/', $file))
					$newFile = $file;
				else
				{
					$newFile = $file.'.torrent';
					rename($file, $newFile);
				}
				break;
			case 'html':
				$newFile = $file.'.html';
				rename($file, $newFile);
				break;
			default:
				echo $mime.PHP_EOL; 
				$newFile = $file.'.'.$mime;
				rename($file, $newFile);
				break;
		}
		return $newFile;
	}


	/**
	 * Получает типа файла
	 *
	 * @param string $file
	 * @return string
	 */
	private static function getFileMimeType($file)
	{
		$mime = mime_content_type($file);
		$explode = explode('/', $mime);
		return $explode[1];
	}


	/**
	 * Разархивирует архив
	 *
	 * @param string $file
	 * @return string
	 */
	protected static function unRar($file)
	{
		$data['folder'] = dirname($file).DIRECTORY_SEPARATOR;

		$rar_file = \RarArchive::open($file);
		if($rar_file === FALSE)
			die("Failed opening rar file");
		
		$entries = $rar_file->getEntries();
		if($entries === FALSE)
			die("Failed fetching entries");

		// Если файл изъят из архива
		foreach($entries as $entry)
		{
			// Получаем разширение файла
			$SplFileInfo =  new \SplFileInfo($entry->getName());
			$ext = '.'.$SplFileInfo->getExtension();

			// Если не видео, то пропускаем
			// if($ext != '.mp4')
			// 	continue;

			$entry->extract($data['folder']);
			// Имя файла
			$data['extractFile'] = $data['folder'].$entry->getName();
			// Ноое имя файла
			$data['newName'] = $file.$ext;
			// Удаление архива
			// unlink($file);
			// Переименование
			if(rename($data['extractFile'], $data['newName']))
				return $data['newName'];
			return $data['extractFile'];
		}
	}


	/**
	 * Удаляет папку со всеми файлами и подпапками
	 *
	 * @param string $path
	 * @return
	 */
	public static function removeDirectory($path)
	{
		if(is_dir($path))
		{
			$dir = $path;
		}
		else
		{
			$dir = dirname($path);
		}
		if($objs = glob($dir."/*"))
		{
			foreach($objs as $obj)
			{
				is_dir($obj) ? self::removeDirectory($obj) : unlink($obj);
			}
		}
		@rmdir($dir);
	}


	/**
	 * Создает папку для скачивания файлов
	 *
	 * @param string $path
	 * @return bool
	 */
	public static function createFolder($path = '')
	{
		// Создаем папку
		if(!empty($path) && !is_dir($path))
			mkdir($path, 0777, true);
		
		// Если папка существует
		if(is_dir($path))
			return true;

		return false;
	}
}
