<?php

namespace app\models\Parser;

use Yii;
use yii\httpclient\Client;
use Sunra\PhpSimple\HtmlDomParser; // парсер

class Rapidgator
{
    public $tempFolder = __DIR__.DIRECTORY_SEPARATOR;
    private $cookieFile;
 
    public function __construct($login, $pass)
    {
        // Авторизация и получение куки
        /////////////////////////////////////////////////////////////////////////////
        $data['url'] = 'https://rapidgator.net/auth/login';
        $data['postData'] = [
            'LoginForm[email]' => $login,
            'LoginForm[password]' => $pass,
            'LoginForm[twoStepAuthCode]' => '',
            'LoginForm[rememberMe]' => '1',
            'g-recaptcha-response' => '',
        ];

        Main::init(7);
        $this->cookieFile = Main::autorization($data);
    }

    public function uploadFile($file)
    {
        $data['url'] = 'https://rapidgator.net/site/index';
        // $data['postData']['MAX_FILE_SIZE'] = '5000000';
        $data['postData']['file'] = new \CURLFile($file);

        $ch = curl_init($data['url']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data['postData']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAX_RECV_SPEED_LARGE, 1024*1024*70);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        echo $postResult = curl_exec($ch);
        if(curl_errno($ch))
        {
            echo date('Y/m/d H:i:s').' - Unable to upload file. - '.curl_error($ch).PHP_EOL;
        }
        curl_close($ch);
        exit;
        return $this->getLink(basename($file));
    }

    public function getLink($name)
    {
        $url = 'https://depfile.us/myfiles';
        if($html = Main::getPageFromCurl($url, $this->cookieFile, 1))
        {
            $fields = $html->find('.myfiles input.c_file');
            if(!is_array($fields) || count($fields) < 1)
                return false;

            foreach($fields as $field)
            {
                if(preg_match('/'.$name.'/', $field->title))
                    return $field->url_s;
            }
        }
        return false;
    }
}