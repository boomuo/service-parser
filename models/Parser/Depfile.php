<?php

namespace app\models\Parser;

use Yii;
use yii\httpclient\Client;
use Sunra\PhpSimple\HtmlDomParser; // парсер

class Depfile
{
    public $tempFolder = __DIR__.DIRECTORY_SEPARATOR;
    private $cookieFile;
 
    public function __construct($login, $pass)
    {
        // Авторизация и получение куки
        /////////////////////////////////////////////////////////////////////////////
        $data['url'] = 'https://depfile.us/';
        $data['postData'] = [
            'login' => 'login',
            'loginemail' => $login,
            'loginpassword' => $pass,
            'submit' => 'login',
            'rememberme' => 'on',
        ];

        Main::init(7);
        $this->cookieFile = Main::autorization($data);
    }

    public function uploadFile($file)
    {
        $html = Main::getPageFromCurl('https://depfile.us/uploads', $this->cookieFile, 1);
        $data['url'] = $html->find('form#upload_form', 0)->action;
        $data['postData']['storageid'] = $html->find('input[name=storageid]', 0)->value;
        $data['postData']['stfoldername'] = $html->find('input[name=stfoldername]', 0)->value;
        $data['postData']['userid'] = $html->find('input[name=userid]', 0)->value;
        $data['postData']['sessionid'] = $html->find('input[name=sessionid]', 0)->value;
        $data['postData']['upfile'] = new \CURLFile($file);

        $ch = curl_init($data['url']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data['postData']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAX_RECV_SPEED_LARGE, 1024*1024*70);
        echo $postResult = curl_exec($ch);
        if(curl_errno($ch))
        {
            echo date('Y/m/d H:i:s').' - Unable to upload file. - '.curl_error($ch).PHP_EOL;
        }
        curl_close($ch);

        return $this->getLink(basename($file));
    }

    public function getLink($name)
    {
        $url = 'https://depfile.us/myfiles';
        if($html = Main::getPageFromCurl($url, $this->cookieFile, 1))
        {
            $fields = $html->find('.myfiles input.c_file');
            if(!is_array($fields) || count($fields) < 1)
                return false;

            foreach($fields as $field)
            {
                if(preg_match('/'.$name.'/', $field->title))
                    return $field->url_s;
            }
        }
        return false;
    }
}