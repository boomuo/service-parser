<?php

namespace app\models\Parser;

use Yii;
use Sunra\PhpSimple\HtmlDomParser;
use yii\httpclient\Client;
use app\models\Archive;
use app\models\Tubes;
use app\models\CategoriesTube;
use LanguageDetection\Language;

class ParserTube extends Parser
{

	protected $patternIdentClass = '/(\.\w|#\w|\/\/)+/';

	// Получение категорий источника
	public function getCategoriesTube()
	{
		// Если указан тег для поиска
		if(isset($this->configTube->categoriesListLinkTag) && !empty($this->configTube->categoriesListLinkTag))
		{
			$explode1 = explode(' ||| ', $this->configTube->categoriesListLinkTag); // Делим на

			$explode2 = explode(' || ', $explode1[0]); // Делим на элемент и атрибут

			$explode3 = explode(' | ', $explode2[0]); // Делим на элемент и ссылку по счету

			if(isset($explode1[0]) && !empty($explode1[0]))
			{
				// Если указана страничка для поиска категории то ищем в ней или берем главную страничку
				if(isset($explode1[1]) && !empty($explode1[1]))
				{
					$url = $explode1[1];
				}
				else
				{
					$url = $this->configTube->url;
				}

				if(isset($explode2[1]) && !empty($explode2[1]))
				{
					$attr = $explode2[1];
				}
				else
				{
					$attr = 'href';
				}

				if(!$page = $this->getPageFromCurl($url))
				{
					return false;
				}

				$search = $page->find($explode3[0]);
				if(count($search) === 0)
				{
					throw new \Exception('На страничке нет списка категорий - ' . $url);
				}
				// unset($url);

				// На 1 страничке ищем ссылки на странички с роликами
				foreach($search as $key => $element)
				{
					$url = $element->{$attr};
					if(!preg_match('/^http/', $url))
					{
						$url = $this->configTube->url . $url;
					}
					$title = $element->plaintext;
					if(isset($explode3[1]) && !empty($explode3[1]))
					{
						if(preg_match('/=/', $explode3[1]))
						{
							if($expl = explode(trim($explode3[1]), $url))
								$cat = $expl[1];
						}
						else
						{
							$explode2 = explode('/', $url);
							$cat = $explode2[$explode3[1]];
						}

						$urls[] = [
							'title' => $title,
							'category' => $cat,
							'url' => $url,
						];
					}
					else
					{
						$urls[] = [
							'title' => $title,
							'category' => $this->rusTranslit($title),
							'url' => $url,
						];
					}
				}
				return $urls;
			}
		}
	}

    ////////////////////////////////////////////////////////////////////////////////
    // Парсим файлы
    ////////////////////////////////////////////////////////////////////////////////

	public function parserTubeFromCategory()
	{
		$data = [];

		// Получаем все источники c данной категорией
		// $tubes = self::getTubes($this->category);


		// Перебираем все источники
		foreach($this->categories as $cat)
		{
			$explode = explode('|', $cat['category']);
			$tube = $explode[0];
			$categoryTube = $explode[1];
			$this->keys = $cat['keys'];

			// Подклюаем настройки туба
			if(!$this->configTube = Tubes::findOne(['name' => $tube]))
			{
				Main::echoTxt('Ошибка получения найстроек для источника - ' . $tube);
				continue;
			}

			// Если парсер выключен, пропускаем его
			if(!@empty($this->keys))
			{
				Main::echoTxt('Поиск по ключевым словам: ' .$this->keys);
			}

			// Если парсер выключен, пропускаем его
			if($this->configTube->status != 1)
			{
				Main::echoTxt('Источник отключен: '.$tube);
				continue;
			}
			else
			{
				Main::echoTxt('Парсинг источника: '.$tube.'/'.$categoryTube);
			}

			// Подключаем модель парсера или модель туба (если есть)
			$this->categoryActive = $categoryTube; // Категория в источнике

			// Авторизация (получение куки)
			if(($dataAuth = $this->getAuthData()) || $this->configTube->dataAuth)
			{
				if(!is_array($dataAuth))
				{
					Main::echoTxt('Нет аккаунта для авторизации');
					return false;
				}

				if(!$this->cookieFile = Main::autorization($dataAuth, 1))
				{
					Main::echoTxt('Авторизация не удалась');
					return false;
				}
			}

			// Парсим странички
			$resultSingle = $this->parsing($this->count - count($data));

			if(!empty($this->alert))
			{
				Main::echoTxt('Ошибка парсинга: '.$this->alert);
				continue;
			}

			$data += $resultSingle;

			if(count($data) >= $this->count)
			{
				break;
			}

		} // End foreach tubes

		if(empty($data))
		{
			Main::echoTxt('Просканировали все тубы. Ничего не спарсено!');
			return false;
		}

		return $data;
	}

	public function parserTube($error = false)
	{
		// Авторизация (получение куки)
		if(($dataAuth = $this->getAuthData()) || $this->configTube->dataAuth)
		{
			if(!is_array($dataAuth))
			{
				Main::echoTxt('Нет аккаунта для авторизации');
				return false;
			}

			if(!$this->cookieFile = Main::autorization($dataAuth, 1))
			{
				Main::echoTxt('Авторизация не удалась');
				return false;
			}
		}

		// Парсим странички
		$data = $this->parsing($this->count);

		if(!empty($this->alert))
		{
			Main::echoTxt('Ошибка парсинга: '.$this->alert);
			if(!$error)
			{
				Main::echoTxt('Удаляю куки и пробую еще раз');
				return $this->parserTube(1);
			}
			return false;
		}


		if(empty($data))
		{
			Main::echoTxt('Просканировали все тубы. Ничего не спарсено!');
			return false;
		}

		return $data;
	}

    // Начало
    public function parsing($count = 100)
    {
		if(!empty($this->proxy) && !Main::checkProxy($this->proxy))
			throw new \Exception('Ваш прокси: '.$this->proxy.' не работает!');

    	// $this->getProxyList(); exit;
		$lastPage = 0;
		$info = [];

		// Перебираем странички категории
		while(empty($this->alert) || count($info) <= $count)
		{
			unset($urls);
	    	if($urls = $this->getLinks(++$lastPage)) // Получаем в категории все ссылки на странички с роликами
	    	{
		    	// Если зеркальньная страничка или если нет ссылок
		    	if(isset($preview) && $preview == $urls)
		    	{
		    		$this->alert = 'Зеркальна ссылка! ' . $this->lastPageUrl . ' Найдено: ' . $this->scanTotal;
		    		break;
		    	}

		        $preview = $urls; // Запоминаем список ссылок для проверки на зеркальность в следующей итерации
		        $this->scanTotal += count($urls);

		        // Перебираем список ссылок
		        foreach($urls as $url)
		        {
					// Проверяем нет ли уже такого видоса в базе
					if(empty(Archive::findAll(['url' => $url, 'user_id' => Yii::$app->user->identity->id])))
					{
						if($getInfo = $this->getAllInfo($url))
						{
							$info[$url] = $getInfo;
						}
					}
					if(count($info) >= $count)
					{
						return $info;
					}
		        }
		    }
		    else
		    {
		    	break;
		    }
		}
		return $info;
    }

    /**
     * Сканируем страничку категории на ссылки с полными новостями
     *
     * @param int $lastPage
     *
     * @return array $result
     */
	public function getLinks($lastPage)
	{
		$result = [];
		if(!@$page = $this->getPagePagination($lastPage))
			return false;
		if(!@$html = $this->getPageFromCurl($page))
			return false;

		$this->lastPageUrl = $page;

		$search = $html->find($this->configTube->articlesListLinkTag);
		if(count($search) === 0)
		{
			// throw new Exception('На страничке нет видео роликов - '.$page);
			$this->alert = 'На страничке нет видео роликов - ' . $page;
			return false;
		}

		// На страничке ищем ссылки на странички с роликами
		foreach($search as $key => $element)
		{
			$url = $element->href;
			if(!preg_match('/^http/', $url))
			{
				$url = $this->configTube->url . $url;
			}

			$result[] = $url;
		}

		return $result;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get info from full page
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Получаем всю инфу со странички с видео
	public function getAllInfo($url)
	{
		// Открываем страничку
		if(!$this->html = $this->getPageFromCurl($url))
		{
			return false;
		}

		// Если указан скласс для осбасти где вся инфа
		if(!empty($this->configTube->allInfo))
		{
			if(!empty($allInfoObj = $this->html->find($this->configTube->allInfo)))
			{
				$this->arrayAllInfo = $this->perebor($allInfoObj);
			}
		}

		// Получаем заголовок
		$file['info']['title'] = $this->getTitle();
		// Получаем описание
		$file['info']['description'] = $this->getDescription();
		// Получаем теги
		$file['info']['tags'] = $this->getTags();
		// Указываем категорию
		$file['info']['category'] = $this->category;
		// Указываем название студии
		$file['info']['studio'] = $this->getStudio();
		// Получаем актеров
		$file['info']['actors'] = $this->getActors();
		// Получаем год
		$file['info']['year'] = $this->getYear();
		// Указываем url
		$file['url'] = $url;
		// Указываем название туба
		$file['tube'] = $this->configTube->name;
		// Делаем имя видео файлу
		$file['fileName'] = $this->removeSumbol($file['info']['title']);
		// Получаем актеров
		$file['folder'] = Main::getTemp()->video.$file['fileName'].'_'.crc32($url).'/';
		// // Делаем путь к торрент файлу
		// $file['torrentPath'] = $file['folder'].$file['fileName'].'.torrent';
		// Делаем путь к видео файлу
		// $file['filePath'] = $file['folder'].$file['fileName'];
		// Получаем ссылку на постер
		$file['posterLink'] = $this->getLink($this->configTube->posterLink);

		// Получаем ссылку на видео файл 
		if(!$file['fileLink'] = $this->getLink($this->configTube->fileLink))
		{
			Main::addIgnore($file, $this->alert);
			return false;
		}

		if(@empty($file['info']['tags']))
		{
			// Main::addIgnore($file, 'Нет тегов');
		}

		if(!@empty($this->keys))
		{
			$explodeKeys = explode(',', $this->keys);
			$explodeTags = explode(', ', $file['info']['tags']);
			foreach($explodeKeys as $key)
			{
				foreach($explodeTags as $tag)
				{
					if(preg_match('/'.trim(strtolower($key)).'/', trim(strtolower($tag))))
					{
						Main::echoTxt('Найден ключ: '.$key);
						return $file;
					}
				}
			}
			Main::echoTxt('Фильтр по ключам не пройден! '.$url);
			return false;
		}
		return $file;
	}

	// Ищем ссылку на файл
	public function getLink($str)
	{
		if(!empty($str))
		{
			// Ищем ссылку на постеры и открываем эту страницу
			$explode0 = explode(' | ', $str);

			if(@$explode0[1] == 'script')
				$link = $this->findLinkInScript($explode0[0]);
			else
				$link = $this->findLinkInHtml($explode0[0]);
		}
		return @$link;
	}

	// Ищем ссылки по тегу
	public function findLinkInHtml($pattern)
	{
		$explode = explode(' || ', $pattern); // Разделяем на тег и аттрибут
		$searchLink = trim($explode[0]);
		if(isset($explode[1]))
		{
			$explode1 = explode(' ||| ', trim($explode[1])); // Разделяем на аттрибут и номер ссылки
			$searchAttr = $explode1[0];
		}
		else
			$searchAttr = 'href';

		// Находим все ссылки на файлы
		foreach($this->html->find($searchLink) as $link)
		{
			$href = $link->{$searchAttr};

			if(substr($href, 0, 2) == '//')
			{
				$href = 'http:' . $href;
			}
			elseif(!preg_match('/^http/', $href) and substr($href, 0, 2) != '//')
			{
				$href = $this->configTube->url . $href;
			}

			$fileLink[] = $href;
		}

		// Если указан доп параметр
		if(isset($explode1[1]))
		{
			if(is_int($explode1[1])) // Если число то возвращаем этот елемент массива
				@$fileLink = $fileLink[$explode1[1]];
			elseif(is_string($explode1[1])) // Если строка то ищем по дааному слову в строке
			{
				if(@$fileLink)
				{
					// Перебор всех найденных ссылок
					foreach($fileLink as $link)
					{
						// Если в ссылке есть искомый фрагмент
						if(preg_match('/'.$explode1[1].'/', $link))
						{
							return $link;
						}
					}
				}
				$this->alert = 'Не найдена ссылка на файл';
				return false;
			}
		}

		if(@is_array($fileLink))
			$fileLink = $fileLink[0];

		return @$fileLink;
	}

	// Ищем ссылку на файл в скриптах JS
	public function findLinkInScript($pattern)
	{
		foreach ($this->html->find('script') as $value)
		{
			if(preg_match("|$pattern|", $value->innertext, $mathes))
			{
				$res = preg_replace('|\\\|', '', $mathes[1]);
				return $res;
			}
		}
		return false;
	}

	// Ищем заголовок
	public function getTitle()
	{
		// Ищем заголовок
		if(!empty($this->configTube->titleTag))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->titleTag))
				$title = $this->searchKey($this->configTube->titleTag, $this->arrayAllInfo);
			else
			{
				$title = trim(strip_tags($this->html->find($this->configTube->titleTag, 0)->plaintext));
				if(function_exists('mb_detect_encoding') && mb_detect_encoding($title) != 'UTF-8')
					$title = iconv("Windows-1251", "UTF-8", $title);

				if($this->configTube->translateRusEng)
					$title = $this->translate($title);
			}

			if(@$title)
			{
				$ld = new Language();
				if($lang = $ld->detect($title)->bestResults() != 'en')
				{
					$title = $this->translate($title, $lang);
				}
			}
		}

		if(@empty($title))
			$title = 'Undefined title';
		return $this->removeStr($title);
	}

	// Ищем описание
	public function getCategory($tags)
	{
		if(@$this->category)
			$category = ucfirst($this->category);
		else
			$category = $this->getCategoryFromTags($tags);

		if(@$category)
		{
			$ld = new Language();
			if($lang = $ld->detect($category)->bestResults() != 'en')
			{
				$category = $this->translate($category, $lang);
			}
		}

		if(@empty($category))
			$category = 'Undefined category';
		return $this->removeStr($category);

	}

	// Ищем описание
	public function getDescription()
	{	
		// Ищем описание
		if(!empty($this->configTube->descriptionTag))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->descriptionTag))
				$description = $this->searchKey($this->configTube->descriptionTag, $this->arrayAllInfo);
			else
			{
				$explode = explode(' | ', $this->configTube->descriptionTag);
				if(isset($explode[1]))
					$num = $explode[1];
				else
					$num = 0;

				if($description = $this->html->find($explode[0], $num))
				{
					$description = ucfirst(trim($description->plaintext));
					if($this->configTube->translateRusEng)
						$description = $this->translate($description);
				}
			}

			if(@$description)
			{
				$ld = new Language();
				if($lang = $ld->detect($description)->bestResults() != 'en')
				{
					$description = $this->translate($description, $lang);
				}
			}
		}

		if(@empty($description))
			$description = 'No description';
		return $this->removeStr($description);
	}

	// Ищем теги
	public function getTags()
	{
		if(!empty($this->configTube->tagsTag))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->tagsTag))
				$tags = $this->searchKey($this->configTube->tagsTag, $this->arrayAllInfo);
			else
			{
				foreach ($this->html->find($this->configTube->tagsTag) as $tag)
				{
					$text = $tag->plaintext;
					if(!empty($text) && !preg_match('/^http/', $text))
					{
						// $tags[] = ucwords($this->removeSumbol($text, ' '));
						$tags[] = ucwords($text);
					}
				}
				// Добавляем категорию к тегам (вариант 1)
				// if(isset($this->category) && !in_array($this->category, $tags))
				// {
				// 	$tags[] = ucfirst($this->category);
				// }

				if(isset($tags) && !empty($tags))
				{
					$result = implode(', ', $tags);
					if($this->configTube->translateRusEng)
					{
						$result = $this->translate($result);
						
						// Добавляем категорию к тегам (вариант 2)
						if(!preg_match('/.*('.$this->category.').*/i', $result))
						{
							$result .= ', '.$this->category;
						}
					}
					$tags = ucwords($result);
				}
			}

			if(@$tags)
			{
				$ld = new Language();
				if($lang = $ld->detect($tags)->bestResults() != 'en')
				{
					$tags = $this->translate($tags, $lang);
				}
			}
		}
		return @$tags;
	}

	// Ищем год
	public function getYear()
	{
		if(!empty($this->configTube->year))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->year))
			{
				$year = $this->searchKey($this->configTube->year, $this->arrayAllInfo);
			}
			else
			{
				
			}
		}

		if(@empty($year))
			$year = date('Y');
		return $year;
	}

	// Ищем актрис
	public function getActors()
	{
		if(!empty($this->configTube->actor))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->actor))
				$actors = $this->searchKey($this->configTube->actor, $this->arrayAllInfo);
			else
			{
				$explode = explode(' | ', $this->configTube->actor);
				if(isset($explode[1]))
				{
					$num = $explode[1];
				}
				else
				{
					$num = 0;
				}

				if(!preg_match('/\|/', $this->configTube->actor))
				{
					foreach ($this->html->find($explode[0]) as $key => $value)
					{
						$text[] = $value->plaintext;
					}

					if(!empty($text))
					{
						$text = implode(', ', $text);
						$actors = $text;
					}
				}

				if($actorsArr = $this->html->find($explode[0], $num))
				{
					foreach($actorsArr as $actor)
					{
						$text = $actor->plaintext;
						if(!empty($text) && !preg_match('/^http/', $text))
						{
							$actorsRes[] = ucwords($this->removeSumbol($text, ' '));
						}
					}

					if(isset($actorsRes) && !empty($actorsRes))
					{
						$actors = implode(', ', $actorsRes);
					}
				}
			}

			if(@$actors)
			{
				$ld = new Language();
				if($lang = $ld->detect($actors)->bestResults() != 'en')
				{
					$actors = $this->translate($actors, $lang);
				}
			}
			
		}

		if(@empty($actors))
			$actors = 'Unknown actors';
		return $actors;
	}

	// Ищем студию
	public function getStudio()
	{
		if(!empty($this->configTube->studio))
		{
			// Если найдена точка или решетка, значит искать по тегам, иначе по словам
			if(!preg_match($this->patternIdentClass, $this->configTube->studio))
			{
				$studio = $this->searchKey($this->configTube->studio, $this->arrayAllInfo);
			}
			else
			{
				$explode = explode(' | ', $this->configTube->studio);
				if(isset($explode[1]))
				{
					$num = $explode[1];
				}
				else
				{
					$num = 0;
				}

				if(!preg_match('/\|/', $this->configTube->studio))
				{
					foreach ($this->html->find($explode[0]) as $key => $value)
					{
						$text[] = $value->plaintext;
					}

					if(!empty($text))
					{
						$text = implode(', ', $text);
						$studio = $text;
					}
				}

				if($studios = $this->html->find($explode[0], $num))
				{
					foreach($studios as $studioItem)
					{
						$text = $studioItem->plaintext;
						if(!empty($text) && !preg_match('/^http/', $text))
						{
							$studio[] = ucwords($this->removeSumbol($text, ' '));
						}
					}

					if(isset($studio) && !empty($studio))
					{
						$studio = implode(', ', $studio);
					}
				}
			}

			if(@$studio)
			{
				$ld = new Language();
				if($lang = $ld->detect($studio)->bestResults() != 'en')
				{
					$studio = $this->translate($studio, $lang);
				}
			}
		}

		if(@empty($studio))
			$studio = 'Unknown studio';
		return $studio;
	}

	// Ищем трейлер
	// public function getTrailer()
	// {
	// 	// Ищем ссылку на треллер и открываем эту страницу
	// 	if(!empty($this->configTube->trailerLink))
	// 	{
	// 		// Получаем ссылку
	// 		if($trailerPageLink = $this->html->find($this->configTube->trailerLink, 0))
	// 		{
	// 			$trailerPageLinkHref = $trailerPageLink->href;
	// 			if(!empty($trailerPageLinkHref) && !preg_match('/^http/', $trailerPageLinkHref))
	// 			{
	// 				$trailerPageLinkHref = $this->configTube->url.$trailerPageLinkHref;

	// 				// Открываем страницу с треллером
	// 				if(!$this->html = $this->getPageFromCurl($trailerPageLinkHref))
	// 				{
	// 					return false;
	// 				}
	// 			}
	// 		}
	// 	}

	// 	// Ищем трейлер по ид
	// 	if(!empty($this->configTube->trailerClass))
	// 	{
	// 		$trailerExplode = explode(' | ', $this->configTube->trailerClass);
	// 		$trailerTag = $trailerExplode[0];
	// 		$trailerAttr = $trailerExplode[1];
	// 		if($trailer = $this->html->find($trailerTag, 0))
	// 		{
	// 			$trailerLink = $trailer->$trailerAttr;
	// 		}
	// 	}

	// 	// Ищем трейлер в скриптах
	// 	if(!empty($this->configTube->trailerScript))
	// 	{
	// 		foreach ($this->html->find('script') as $value)
	// 		{
	// 			$script = $value->innertext;
	// 			if(preg_match('/trailers/', $script))
	// 			{
	// 				if(preg_match('/mp4_720_2600":"(.*?)"/', $script, $matches))
	// 				{
	// 					$trailerLink = stripslashes($matches[1]);
	// 				}
	// 			}
	// 			// echo $value->innertext.'<hr>';
	// 		}
	// 	}

	// 	if(isset($trailerLink) && !empty($trailerLink))
	// 	{
	// 		return $trailerLink;
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }

	// Ищем пак постеров
	// public function getPosterPack()
	// {
	// 	if(!empty($this->configTube->imgPackClass))
	// 	{
	// 		if($imgPack = $this->html->find($this->configTube->imgPackClass))
	// 		{
	// 			foreach ($imgPack as $key => $value)
	// 			{
	// 				$posterPackLink = $value->href;
	// 				if(!empty($posterPackLink))
	// 				{
	// 					return $posterPackLink;
	// 				}
	// 			}
	// 		}
	// 	}
	// 	return false;
	// }


	////////////////////////////////////////////////////////////////////////////////////////////////////

 
    /**
     * Список категорий
     *
     * @return array $categories
     */
	public static function getCategories()
	{
		$categories = [];
		foreach(CategoriesTube::find()->asArray()->all() as $product_filter)
		{
			$categories[$product_filter['category']] = ucfirst($product_filter['category']);
		}
		ksort($categories);
		return $categories;
	}


    /**
     * Список тубов с выбранной категорией
     *
     * @param string $category
     * @return array
     */
	public static function getTubes($category)
	{
		return array_reverse(CategoriesTube::findAll(['category' => strtolower($category)]));
	}
}
