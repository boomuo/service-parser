<?php

namespace app\models\Parser;

use yii\httpclient\Client;
use app\models\Parser\Main;

class Images extends Main
{
	public $format = '.jpg'; // формат изображений
	public $sufix = 'poster'; //
	public $size = 'original'; // small, medium, large, original
	public $hyperlink = 0;

	public $imgBorder = 1; // ширина рамки (для скриншотов)
	public $rows = 2; // сколько кадров в ряд (для скриншотов)
	public $cols = 2; // сколько кадров в столбик (для скриншотов)
	public $width = 900; // ширина (для скриншотов)
	public $count = 1;
	public $hosting = 'pimpandhost';
	// public $hostingForum = 'pimpandhost';
	// public $hostingSite = 'picporn';

	public $qualityImage = 80;

	/**
	* @var int
	* До скольки секунд можно откинуть от видео ролика (для создания рандомных изображений)
	*/
	public $minusSecRandom = 10;

	/**
	* @var array
	* Массив с ошибками
	*/
	public $error;

	/**
	* @var array
	* Путь к бинарным файлам
	*/
	public $ffmpegPath;
	public $ffprobePath;
	public $folder;
	public $video_file;
	

	function __construct()
	{
		$ffmpeg = 'ffmpeg';
		$ffprobe = 'ffprobe';
		if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
		{
			$ffmpeg = 'ffmpeg.exe';
			$ffprobe = 'ffprobe.exe';
		}

		$this->ffmpegPath = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'program-files'.DIRECTORY_SEPARATOR.$ffmpeg;
		$this->ffprobePath = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'program-files'.DIRECTORY_SEPARATOR.$ffprobe;
	}

	// Разархивирует архив
	// Возвращает список файлов
	function unPack($fileZip, $count)
	{
		if(!file_exists($fileZip))
		{
			return false;
		}
		// if($fileZip == 'D:\www\parser/temp/video/Never-Interrupt-Mommy-Time_401970328/Never-Interrupt-Mommy-Time.zip')
		// {
		// 	return false;
		// }

		// Создаём объект для работы с ZIP-архивами
		$zip = new ZipArchive(); 
		
		//Открываем архив и делаем проверку успешности открытия
		if($zip->open($fileZip) === true)
		{
			$folder = dirname($fileZip);
			$result['countImagesPack'] = $zip->numFiles;
			$result['sizeImagesPack'] = Handler::size(filesize($fileZip));
			for($i=0; $i<$count; $i++)
			{
				$index = rand(0,$result['countImagesPack']-1);
				$fileName = $zip->getNameIndex($index);

				if($zip->extractTo($folder, $fileName))
				{
					$result['postersPath'][] = $folder.'/'.$fileName;
				}
			}
			
			//Завершаем работу с архивом
			$zip->close(); 
			return $result;
		}
	}

	// Создает кадры
	// Возвращает список кадров [масив]
	public function createPosters($video_file, $count)
	{
		$ffprobe = \FFMpeg\FFProbe::create([
			'ffmpeg.binaries'  => $this->ffmpegPath,
			'ffprobe.binaries'  => $this->ffprobePath,
		]);
		$duration = $ffprobe
			->format($video_file)
			->get('duration');


		$ffmpeg = \FFMpeg\FFMpeg::create([
			'ffmpeg.binaries'  => $this->ffmpegPath,
			'ffprobe.binaries'  => $this->ffprobePath,
		]);

		$video = $ffmpeg->open($video_file);
		for($i=0; $i < $count; $i++)
		{
			$img = $this->folder.$this->sufix.'-'.$i.$this->format;
			$frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(rand(10,$duration-10)));
			$frame->save($img);
			$result[] = $img;
		}
		return $result;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get trailer
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// public function getTrailer($data)
	// {
	// 	$res = [];
	// 	if(!isset($data) && empty($data))
	// 	{
	// 		throw new Exception('Не получены данные для создания треллеров');
	// 	}

	// 	foreach($data as $key => $file)
	// 	{
	// 		if(!isset($file['trailerPath']) || !file_exists($file['trailerPath']))
	// 		{
	// 			$res[$key]['trailerPath'] = $this->createTrailer($file);
	// 		}
	// 		else
	// 		{
	// 			$res[$key]['trailerPath'] = $file['trailerPath'];
	// 		}
	// 	}
	// 	return $res;
	// }

	// public function createTrailer($data)
	// {
	// 	if(!file_exists($data['filePath']))
	// 	{
	// 		return false;
	// 	}

	// 	if(@file_exists($data['trailerPath']))
	// 	{
	// 		return $data['trailerPath'];
	// 	}

	// 	$trailer = $data['folder'] . $data['fileName'] . '-trailer.mp4';

	// 	$timePart = $this->trailerTimePart;
	// 	$part = $this->trailerPart;

	// 	// $val = 4;
	// 	// if(($data['durationSecond'] / $val) < ($timePart * $part))
	// 	// {
	// 	// 	$timePart = (int) $timePart / $val;
	// 	// 	$part = (int) $part / $val;
	// 	// }
	// 	// elseif(($data['durationSecond'] / $val-1) < ($timePart * $part))
	// 	// {
	// 	// 	$timePart = (int) $timePart / $val-1;
	// 	// 	$part = (int) $part / $val-1;
	// 	// }
	// 	// elseif(($data['durationSecond'] / $val-2) < ($timePart * $part))
	// 	// {
	// 	// 	$timePart = (int) $timePart / $val-2;
	// 	// 	$part = (int) $part / $val-2;
	// 	// }

	// 	$start = 1;
	// 	$t = (int) ($data['durationSecond'] / $part);
	// 	$c = '';
	// 	for($i=1; $i<=$part; $i++)
	// 	{
	// 		$partFileTs = $data['folder'] . 'tp_'.$i.'.ts';
	// 		$cmd = "\"$this->ffmpegPath\" -ss $start -i \"$data[filePath]\" -t $timePart -vcodec copy -acodec copy -bsf h264_mp4toannexb \"$partFileTs\"";
	// 		exec($cmd, $out, $error);
	// 		if($error == 0)
	// 		{
	// 			$res[] = $partFileTs;
	// 			$start += $t;
	// 		}
	// 	}

	// 	$partsFiles = implode('|', $res);
	// 	$copy_cmd = "\"$this->ffmpegPath\" -i concat:\"$partsFiles\" -c copy -bsf:a aac_adtstoasc \"$trailer\"";
	// 	exec($copy_cmd, $out2, $error2);
	// 	foreach($res as $key => $value)
	// 	{
	// 		unlink($value);
	// 	}

	// 	return $trailer;
	// }
	
	// Создает кадры
	// Возвращает список кадров [масив]
	// function createGif()
	// {
	// 	$ffprobe = \FFMpeg\FFProbe::create([
	// 		'ffmpeg.binaries'  => $this->ffmpegPath,
	// 		'ffprobe.binaries'  => $this->ffprobePath,
	// 	]);

	// 	$ffmpeg = \FFMpeg\FFMpeg::create([
	// 		'ffmpeg.binaries'  => $this->ffmpegPath,
	// 		'ffprobe.binaries'  => $this->ffprobePath,
	// 	]);

	// 	$video
	// 		->gif(\FFMpeg\Coordinate\TimeCode::fromSeconds(20), new \FFMpeg\Coordinate\Dimension(900, 600), 6)
	// 		->save($data['filePath'].'image.gif');
	// }

	// Создает кадры
	// Возвращает список кадров [масив]
	// function createTrailer()
	// {
	// // $video->filters()->clip(\FFMpeg\Coordinate\TimeCode::fromSeconds(30), \FFMpeg\Coordinate\TimeCode::fromSeconds(5));
	// // $video->save($data['filePath'].'clip.mp4');

	// echo '<pre>';
	// // print_r(get_class_methods($ffprobe));
	// // print_r(get_class_methods($ffprobe->streams($data['filePath'])->videos()));
	// // print_r(get_class_vars(get_class($ffprobe)));
	// echo '<hr>';

	// // print_r($ffprobe->streams($data['filePath'])->audios()->first());
	// // print_r($ffprobe->getMapper($data['filePath']));
	// // print_r(get_class_methods($video));
	// // print_r($video->getFFProbe());
	// // print_r($video->getStreams());
	// echo '</pre>';
	// }

	// Подготовка к созданию постера
	// Возвращает BB код ссылку на изображение [строка]
	// function getScreenshots($video_file)
	// {
	// 	$ffprobe = \FFMpeg\FFProbe::create([
	// 		'ffmpeg.binaries'  => $this->ffmpegPath,
	// 		'ffprobe.binaries'  => $this->ffprobePath,
	// 	]);
	// 	$duration = $ffprobe
	// 		->format($video_file)
	// 		->get('duration');

	// 	// $imgPath = dirname($video_file).DIRECTORY_SEPARATOR.basename($video_file).$this->sufix.$this->format;
	// 	$widthOnce = ($this->width - $this->cols - $this->cols) / $this->cols;
	// 	$border = 1;
	// 	$count = $this->rows*$this->cols;
	// 	$count = 3;
	// 	$interval = $duration / $count + 10;

	// 	$ffmpeg = \FFMpeg\FFMpeg::create([
	// 		'ffmpeg.binaries'  => $this->ffmpegPath,
	// 		'ffprobe.binaries'  => $this->ffprobePath,
	// 	]);
	// 	$video = $ffmpeg->open($video_file);
	// 	for($i=0; $i < $count; $i++)
	// 	{
	// 		$imgPath = dirname($video_file).DIRECTORY_SEPARATOR.basename($video_file).$this->sufix.'-part-'.$i.$this->format;
	// 		$frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($interval * $i));
	// 		$frame->save($imgPath);
	// 		$result[] = $imgPath;
	// 	}

	// 	return $this->create_jpg($array_img, $jpg_file, $this->rows, $this->cols, $this->imgBorder);
	// }

	////////////////////////////////////////////////////////////////////////////////
	// Загрузка изображений на фотохостинг
	////////////////////////////////////////////////////////////////////////////////
	
	function upload($imagesPathArray)
	{
		if($this->hosting == 'imagebam') // 350px
		{
			$data['callback'] = 'html';
			$data['fileAttr'] = 'file[0]';
			$data['url'] = 'http://www.imagebam.com/sys/upload/save';
			$data['options'] = [
				'content_type' 			=> 1,
				'thumb_size' 			=> 350,
				'thumb_aspect_ratio' 	=> 'resize',
				'thumb_file_type' 		=> 'jpg'
			];
		}
		elseif($this->hosting == 'pixhost') // Off
		{
			$data['callback'] = 'html';
			$data['fileAttr'] = 'img[]';
			$data['url'] = 'http://www.pixhost.org/classic-upload/';
			$data['options'] = [
				'content_type' 	=> 1,
				'tos' 			=> 'on',
			];
		}
		elseif($this->hosting == 'crazyimg')
		{
			$data['callback'] = 'html';
			$data['fileAttr'] = 'pic_0';
			$data['url'] = 'http://crazyimg.com/upload/simple/simple.php';
			$data['options'] = [
				'optimize' 			=> 'checked',
				'optimize_value' 	=> 80,
				'resize' 			=> '',
				'resize_value' 		=> 700,
				'rotate' 			=> '',
				'rotate_value' 		=> '',
			];
		}
		elseif($this->hosting == 'picporn')
		{
			if($this->size == 'small')
			{
				$sizePx = 250;
			}
			elseif($this->size == 'medium')
			{
				$sizePx = 500;
			}
			elseif($this->size == 'large')
			{
				$sizePx = 750;
			}
			elseif($this->size == 'original')
			{
				$sizePx = 1000;
			}

			$data['callback'] = 'picporn';
			$data['fileAttr'] = 'local_uploadfile[]';
			$data['url'] = 'https://picporn.ru';
			$data['options'] = [
				'action' => 'uploadCurl',
				'resize' => 'true',
				// 'web_uploadfile' => '',
				'width' => 900,
				'height' => '',
				'thumb' => 'true',
				'thumb_width' => $sizePx,
				'thumb_height' => '',
				'texttype' => 'nothing',
				'text' => '',
			];
		}
		elseif($this->hosting == 'pimpandhost')
		{
			$data['callback'] = 'pimpandhost';
			$data['fileAttr'] = 'files';
			$data['url'] = 'http://pimpandhost.com/image/upload-file';
		}
		elseif($this->hosting == 'imagevenue')
		{
			$data['callback'] = 'html';
			$data['fileAttr'] = 'userfile[]';
			$data['url'] = 'http://imagevenue.com/upload.php';
			$data['options'] = [
				'imgcontent' 	=> 'notsafe',
				'MAX_FILE_SIZE' => ' ',
				'action' 		=> 1,
				'img_resize' 	=> '',
			];
		}
		elseif($this->hosting == 'greenpiccs') // Off
		{
			$data['callback'] = 'html';
			$data['fileAttr'] = 'pic_0';
			$data['url'] = 'http://greenpiccs.com/upload/simple/simple.php';
			$data['options'] = [
				'thumb_size'	=> $this->sizePx,
				'optimize' 		=> '',
				'optimize_value'=> 80,
				'resize' 		=> '',
				'resize_value' 	=> 900,
				'watermark' 	=> '',
				'rotate' 		=> '',
				'rotate_value' 	=> 90,
			];
		}
		elseif($this->hosting == 'picstate') // Off
		{
			// $data['callback'] = 'html';
			// $data['fileAttr'] = 'pic_0';
			// $data['url'] = 'http://picstate.com/upload/simple/simple.php';
			// $data['options'] = [
		 //		'thumb_size'	=> $this->sizePx,
		 //		'optimize' 		=> '',
		 //		'optimize_value'=> 80,
		 //		'resize' 		=> '',
		 //		'resize_value' 	=> 900,
		 //		'watermark' 	=> '',
		 //		'rotate' 		=> '',
		 //		'rotate_value' 	=> 90,
			// ];
		}
		elseif($this->hosting == 'imagetwist') // запрещен на некоторых форумах Off
		{
			if($this->size == 'small')
			{
				$thumb_size = '250x250';
			}
			elseif($this->size == 'medium')
			{
				$thumb_size = '500x500';
			}
			elseif($this->size == 'large')
			{
				$thumb_size = '750x750';
			}
			elseif($this->size == 'original')
			{
				$thumb_size = '800x800';
			}

			$data['callback'] = 'html';
			$data['fileAttr'] = 'file_0';
			$data['url'] = 'http://img26.imagetwist.com/cgi-bin/upload.cgi?upload_id=';
			$data['options'] = [
				'per_row' 		=> 1,
				'sdomain' 		=> 'imagetwist.com',
				'tos' 			=> 1,
				'submit_btn' 	=> 'Upload',
				'thumb_size' 	=> $thumb_size,
			];
		}

		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport'
		]);
		foreach($imagesPathArray as $image)
		{
			if(file_exists($image))
				$imgFile = $image;
			elseif(file_exists($image['filePath']))
				$imgFile = $image['filePath'];
			else
				return false;

			$response = $client->createRequest()
				->setMethod('post')
				->setUrl($data['url'])
				->setData(@$data['options'])
				->addFile($data['fileAttr'], $imgFile)
				->setOptions([
					'timeout' => 120,
				])
				->send();

			$this->response = $response->content;
			if(@$res = call_user_func(array($this, $data['callback'])))
				$result[] = $res;
		}
		return @$result;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	function html()
	{
		$template = "|(\[URL=.*\[\/URL])|i";
		preg_match($template, $this->response, $maches);
		if(isset($maches[0]))
		{
			return $maches[0];
		}
	}

	function picporn()
	{
		foreach(json_decode($this->response) as $image)
		{
			if($this->hyperlink == 0)
				return $image->bb_img;
			else
				return $image->bb_prev_and_img;
		}
	}

	function pimpandhost()
	{
		foreach(json_decode($this->response)->files as $file)
		{
			if(@!$file->image)
			{
				echo '<pre>';
				print_r($file);
				print_r($this->response);
				print_r($this->hosting);
				echo '</pre>';
				return false;
			}
			$img['full'] = $file->image->{0};
			if($this->size == 'small')
			{
				$img['thumb'] = $file->image->{250};
			}
			elseif($this->size == 'medium')
			{
				$img['thumb'] = $file->image->{500};
			}
			elseif($this->size == 'large')
			{
				$img['thumb'] = $file->image->{750};
			}
			elseif($this->size == 'original')
			{
				$img['thumb'] = $file->image->{0};
			}
			if($this->hyperlink == 0)
				return "[IMG]$img[thumb][/IMG]";
			else
				return "[URL=$img[full]][IMG]$img[thumb][/IMG][/URL]";
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	function resultHandler($response, $responseType)
	{
		if($responseType == 'html')
		{
			$template = "|(\[URL=.*\[\/URL])|";
			preg_match($template, $response, $maches);
			if(isset($maches[0]))
			{
				return $maches[0];
			}
		}
		elseif($responseType == 'json')
		{
			if($this->responseReturn)
			{
				foreach($response as $kk => $value0)
				{
					foreach($value0 as $value)
					{
						foreach($this->responseReturn as $key)
						{
							$attr = $key;
							$res[$kk][$key] = $value->$attr;
						}
					}
				}
				return $res;
			}
		}
		elseif($responseType == 'array')
		{
			if(!isset($response) OR empty($response))
			{
				return false;
			}

			switch($this->size)
			{
				case 'small':
					$key = 250;
					break;
				case 'medium':
					$key = 500;
					break;
				case 'large':
					$key = 750;
					break;
				case 'original':
					$key = 0;
					break;
				default:
					$key = 0;
					break;
			}

			foreach($response as $item)
			{
				if(!isset($item->files[0]->image->$key))
				{
					$this->error[]['danger'] ='Ошибка в данных после загрузки изображения. Нет нужного изображения. responseType: '.$responseType .'<br>'. print_r($item);
					continue;
				}

				$link = $item->files[0]->image->$key;
				if($this->bb)
				{
					$key = 0;
					$res[] = [
						'bb' => '[URL='.$item->files[0]->image->$key.'][IMG]'.$link.'[/IMG][/URL]',
						'thumb' => $link,
						'full' => $item->files[0]->image->$key,
					];
				}
				else
				{
					$res[] = $link;
				}
			}
				
			return $res;
		}
	}


	////////////////////////////////////////////////////////////////////


	function getScreenshotOld($video_file, $count)
	{
		for($i=0; $i < $count; $i++)
		{
			if($src = $this->createScreenshot($video_file))
				$screenshot[] = $src;
		}
		return @$screenshot;
	}

	// Подготовка к созданию постера
	// Возвращает BB код ссылку на изображение [строка]
	function createScreenshot($video_file)
	{
		if(file_exists($jpg_file = $this->folder.$this->sufix.$this->format))
			return $jpg_file;

		$ffprobe = \FFMpeg\FFProbe::create([
			'ffmpeg.binaries'  => $this->ffmpegPath,
			'ffprobe.binaries'  => $this->ffprobePath,
		]);
		$duration = $ffprobe
			->format($video_file)
			->get('duration');

		$ffmpeg = \FFMpeg\FFMpeg::create([
			'ffmpeg.binaries'  => $this->ffmpegPath,
			'ffprobe.binaries'  => $this->ffprobePath,
		]);

		$video = $ffmpeg->open($video_file);
		$count = $this->rows*$this->cols;
		$interval = ($duration-rand(0,$duration/10)) / $count;

		for ($i=1; $i <= $count; $i++)
		{
			$image_count_name = $this->folder.'-screenshot-part-'.$i.$this->format;
			if(file_exists($image_count_name))
			{
				$frames[] = $image_count_name;
				continue;
			}

			$frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($interval * $i));
			$frame->save($image_count_name);
			if(file_exists($image_count_name))
				$frames[] = $image_count_name;
		}

		if(!isset($frames) || count($frames) < 1)
		{
			// Main::echoTxt('Cкриншоты не созданы! Последняя команда:<br>');
			return false;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		// Получение размера первого фрейма как по умолчанию
		if(!$img_size_default = getimagesize($frames[0]))
		{
			$this->error[]['danger'] ='Не могу склеить изоражение, не получен размер скриншота<br>getimagesize()';
			return false;
		}

		//итоговая ширина-длина скринлиста 
		$bgWidth = $img_size_default[0] * $this->cols; 
		$bgHeight = $img_size_default[1] * $this->rows;

		//создаём пустой скринлист нужного размера 
		if(!$im = imagecreatetruecolor($bgWidth+$this->imgBorder, $bgHeight+$this->imgBorder))
		{
			$this->error[]['danger'] ='Невозможно инициализировать GD поток';
			return false;
		}
		$bg_color = imagecolorallocate($im, 103, 103, 103);
		imagefill($im, 0, 0, $bg_color);
		//$text_color = imagecolorallocate($im, 0, 0, 0);
		//imagestring($im, 5, 5, 5, '123 123 Мой текст бла бла бла', $text_color);

		$f=0; //первое изображение в images 
		//последовательно обрабатываем каждый ряд скринлиста, вставляя в нужном месте изображение  
		for($j=0; $j<$this->rows; $j++)
		{ 
			for($i=0; $i<$this->cols; $i++)
			{
				if(!isset($frames[$f]))
					break 2;
				$src = imagecreatefromjpeg($frames[$f]);
				$img_size = getimagesize($frames[$f]);
				imagecopy($im, $src, $img_size[0]*$i+$this->imgBorder, $img_size[1]*$j+$this->imgBorder, 0, 0, $img_size[0]-$this->imgBorder, $img_size[1]-$this->imgBorder); 
				$f++; //следующее изображение в images 
			} 
		} 

		$get_width = imagesx($im);
		$get_height = imagesy($im);

		$width = 1000;
		$percent = $width*100/$get_width;
		$height = $get_height*$percent/100;

		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $im, 0, 0, 0, 0, $width, $height, $get_width, $get_height);

		//сохраняем изображение
		imagejpeg($new_image, $jpg_file, $this->qualityImage);
      
		imagedestroy($im); 
		imagedestroy($src);

		// удаляем список миниатюр
		$this->delete_file_list($frames);
		return $jpg_file; 
	}

	// Удаляет все промежуточные кадры изображений
	function delete_file_list($list)
	{
		foreach ($list as $file)
		{
			if(!@unlink($file))
			{
				chmod($file, 0777);
				@unlink($file);
			}
		}
	}
}