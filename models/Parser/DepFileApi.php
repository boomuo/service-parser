<?php

namespace app\models\Parser;

use Yii;
use yii\httpclient\Client;

class DepFileApi
{
    public $userid;
    public $uploadURL;
    private $postParams = array ();
 
    public function __construct($userid)
    {
        $this->userid = $userid;
        $ch = curl_init('http://w1w.yt/api/upload.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $postResult = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($postResult);
        if(!is_object($res))
        {
        	echo date('Y/m/d H:i:s').' - Ошибка подключения к deepfile'.PHP_EOL;
        	return false;
        }
        $this->uploadURL = $res->action;
        foreach($res->postparams as $fieldName => $fieldValue){
            $this->postParams[$fieldName] = $fieldValue;
        }
        $this->postParams['userid'] = $userid;
    }
 
    public function uploadFile($file)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $this->uploadURL);
        curl_setopt($ch, CURLOPT_POST, 1);
        $this->postParams['upfile'] = new \CURLFile($file);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->postParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_MAX_RECV_SPEED_LARGE, 1024*1024*70);
        $postResult = curl_exec($ch);
        if(curl_errno($ch))
        {
            echo date('Y/m/d H:i:s').' - '.curl_error($ch).PHP_EOL;
            return false;
        }
        curl_close($ch);
        $result = json_decode($postResult, true);

        if(isset($result['error']))
            return false;
        else
        {
            $url='http://w1w.yt/api/upload.php?get=short&userfileid='.$result['userfileid'];//short URL
            //$url='http://w1w.yt/api/upload.php?get=short_captcha&userfileid='.$result['userfileid'];//short URL (CAPTCHA)
            //$url='http://w1w.yt/api/upload.php?get=full&userfileid='.$result['userfileid'];//Full URL
            //$url='http://w1w.yt/api/upload.php?get=full_captcha&userfileid='.$result['userfileid']);//full URL (CAPTCHA)
            $ch=curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $postResult = curl_exec($ch);
            curl_close($ch);
            $res = json_decode($postResult);
            if(isset($res->short))
            	return $res->short;
            echo date('Y/m/d H:i:s').' - Нет элемента short в результате загрузки на DepFileApi'.PHP_EOL;
            print_r(@$res).PHP_EOL;
            print_r(@$result).PHP_EOL;
            return false;
        }
    }
}