<?php

namespace app\models\Parser;

use Yii;
use yii\httpclient\Client;

class Downloader
{	
	/**
	* @var string
	* Прокси для скачивания файлов
	*/
	public static $proxy;

	/**
	* @var string
	* Куки для скачивания файлов
	*/
	public static $cookieFile;

	/**
	* @var array
	* Массив с ссылками для скачивания
	*/
	protected $downloadsList = array();
	
	/**
	* @var array
	* Результат скачивания
	*/
	protected $result = array();
	
	/**
	* @var int
	* Сколько успехных скачиваний файлов
	*/
	public $successsful = 0;


	/**
	 * Удаляет папку со всеми файлами и подпапками
	 *
	 * @param string $path
	 */
	static function removeDirectory($path)
	{
		if(is_dir($path))
		{
			$dir = $path;
		}
		else
		{
			$dir = dirname($path);
		}
		if($objs = glob($dir."/*"))
		{
			foreach($objs as $obj)
			{
				is_dir($obj) ? self::removeDirectory($obj) : unlink($obj);
			}
		}
		@rmdir($dir);
	}

	/**
	 * Создает папку для скачивания файлов
	 *
	 * @param array $data
	 * @param string $tempFolderVideo - Путь в директорию для сохранения файлов
	 * @return string
	 */
	public static function createFolder($path = '')
	{
		// Создаем папку
		if(!empty($path) && !is_dir($path))
			mkdir($path, 0777, true);
		
		// Если папка существует
		if(is_dir($path))
			return true;

		return false;
	}

	/**
	 * Добавить ссылку для скачивания
	 *
	 * @param array $data
	 * @return this
	 */
	public function setDownloadList($data)
	{
		if (!is_array($data)) {
			throw new \InvalidArgumentException("downloads list must be an array");
		}
		$this->downloadsList = $data;
		return $this;
	}

	/**
	 * Добавить ссылку для скачивания к имеющемуся списку
	 *
	 * @param array $data
	 * @return this
	 */
	public function addDownloadList($data)
	{
		if (!is_array($data)) {
			throw new \InvalidArgumentException("downloads list must be an array");
		}
		$this->downloadsList += $data;
		return $this;
	}

	/**
	 * Получить список ссылок на скачивание
	 *
	 * @return array
	 */
	public function getDownloadList()
	{
		return $this->downloadsList;
	}

	/**
	 * Возвращает результат скачивания
	 *
	 * @return this->result
	 */
	public function getResult()
	{
		return $this->result;
	}

	/**
	 * Скачивает все файлы из очереди
	 *
	 * @return this
	 */
	// public function execute()
	// {
	// 	$client = new Client([
 //			'transport' => 'yii\httpclient\CurlTransport'
	// 	]);
	// 	foreach($this->downloadsList as $key => $item)
	// 	{
	// 		$file = $item['path'].$item['name'];
	// 		// Проверяем не скачан ли уже видео файл и размер больше 1kb
	// 		if(file_exists($file) && filesize($file) > 1024*1024)
	// 		{
	// 			$this->result[$key][ $this->downloadsList[$key]['key'] ] = $file;
	// 			continue;
	// 		}

	// 		if($this->createFolder($item['path']))
	// 		{
	// 			$requests[$key] = $client->createRequest()
	// 				->setUrl($item['link'])
	// 				->setOptions([
	// 					'timeout' => 15,
	// 					'proxy' => @$item['proxy'],
	// 					CURLOPT_RETURNTRANSFER => 1,
	// 					CURLOPT_BINARYTRANSFER => 0,
	// 					CURLOPT_VERBOSE => 0,
	// 					CURLOPT_AUTOREFERER => 1,
	// 					CURLOPT_REFERER => 'https://www.google.com',
	// 					CURLOPT_SSL_VERIFYPEER => 0,
	// 					CURLOPT_SSL_VERIFYHOST => 0,
	// 					CURLOPT_CONNECTTIMEOUT => 30, // Таймаут соединения. 0 - для неограниченного времени
	// 					CURLOPT_TIMEOUT => 60*60*1, // Таймаут ожидания. 0 - для неограниченного времени
	// 					CURLOPT_FILE => $fopen[$key] = fopen($file, 'w'),
	// 				]);
	// 		}
	// 	}
	// 	if(isset($requests))
	// 	{
	// 		$responses = $client->batchSend($requests);
	// 		foreach($responses as $key => $value)
	// 		{
	// 			if($value->isOk)
	// 			{
	// 				// Проверка размера файла
	// 				if((isset($this->downloadsList[$key]['minSaveSize'])) && $value->getHeaders()['Content-Length'] < $this->downloadsList[$key]['minSaveSize'] * 1024 * 1024)
	// 				{
	// 					$this->alert[]['danger'] = 'Скачал маленький файл: '.round($value->getHeaders()['Content-Length'] / 1024 / 1024, 2).' < '.$this->downloadsList[$key]['minSaveSize'];
	// 					$this->removeDirectory($this->downloadsList[$key]['path']);
	// 					continue;
	// 				}

	// 				// Сохраняем путь к файлу в переменную result
	// 				$this->result[$key][ $this->downloadsList[$key]['key'] ] = $this->downloadsList[$key]['path'].$this->downloadsList[$key]['name'];
	// 				$this->successsful++;
	// 			}
	// 			else
	// 			{
	// 				$this->alert[]['danger'] = 'Ошибка! Код: '.$value->getStatusCode();
	// 				$this->removeDirectory($this->downloadsList[$key]['path']);
	// 				continue;
	// 			}
	// 		}
	// 	}

	// 	if(!empty($this->result))
	// 		return $this->result;
	// 	return false;
	// }

	/**
	 * Скачивает все файлы из очереди
	 *
	 * @return this
	 */
	public static function executeOne($item)
	{
		if(file_exists($item['file']))
		{
			$result['file'] = self::getFileType($item['file']);
			$result['size'] = filesize($result['file']);
			return $result;
		}

		if(!self::createFolder(dirname($item['file'])))
			return false;

		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport'
		]);

		@$requests = $client->createRequest()
			->setUrl($item['link'])
			->setOptions([
				'timeout' => 60*60*2,
				'proxy' => @self::$proxy,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_VERBOSE => 0,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_SSL_VERIFYHOST => 0,
				// CURLOPT_TIMEOUT => 60*60*2, // Таймаут ожидания. 0 - для неограниченного времени
				CURLOPT_FILE => fopen($item['file'], 'w'),
				CURLOPT_COOKIEFILE => @self::$cookieFile,
			])
			->send();

		if($requests->isOk)
		{
			if(file_exists($item['file']))
			{
				if($result['file'] = self::getFileType($item['file']))
					$result['size'] = filesize($result['file']);
				else
				{
					echo 'requests executeOne<pre>';
					print_r($result);
					print_r($requests);
					echo '</pre>';
					exit();
				}
			}
		}
		else
			$result['error'] = 'Ошибка скачивания файла! Код: '.$requests->getStatusCode().PHP_EOL.$item['link'];

		return $result;
	}



    /**
     * Скачивает торрент файл
     *
     * @param array $data [fileLink, cookieFile, fileName]
     * @return str fileName
     */
	public static function downloadTorrent($data)
	{
		// if(file_exists($data['torrentPath'])) return $data['torrentPath'];
		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport' //только cURL поддерживает нужные нам параметры
		]);

		if(Downloader::createFolder($data['folder']))
		{
			$response = $client->createRequest()
	            ->setMethod('POST')
	            ->setUrl($data['fileLink'])
				->setOptions([
		    		CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_FILE => $fopen = fopen($data['filePath'].'.torrent', 'w'),
		    		CURLOPT_COOKIEFILE => @self::$cookieFile,
				])
	            ->send();
	        if(!preg_match('/.*bittorrent.*/', $response->headers['content-type']))
	        {
	        	return false;
	        }
			if($response->isOk) return $data['filePath'].'.torrent';
		}
		return false;
	}


	private static function unRar($file)
	{
		$data['folder'] = dirname($file).DIRECTORY_SEPARATOR;
		
		$rar_file = \RarArchive::open($file);
		if($rar_file === FALSE)
			die("Failed opening rar file");
		
		$entries = $rar_file->getEntries();
		if($entries === FALSE)
			die("Failed fetching entries");

		// Если файл изъят из архива
		foreach($entries as $entry)
		{
			// Получаем разширение файла
			$SplFileInfo =  new \SplFileInfo($entry->getName());
			$ext = '.'.$SplFileInfo->getExtension();

			// Если не видео, то пропускаем
			if($ext != '.mp4')
				continue;

			$entry->extract($data['folder']);
			// Имя файла
			$data['extractFile'] = $data['folder'].$entry->getName();
			// Ноое имя файла
			$data['newName'] = $file.$ext;
			// Удаление архива
			// unlink($file);
			// Переименование
			if(rename($data['extractFile'], $data['newName']))
				return $data['newName'];
			return $data['extractFile'];
		}
	}

	private static function renameFile($file)
	{
		$mime = self::getFileMimeType($file);

		switch($mime)
		{
			case 'x-rar':
				$newFile = self::unRar($file);
				break;
			case 'jpeg':
				$newFile = $file.'.jpg';
				rename($file, $newFile);
				break;
			case 'png':
				$newFile = $file.'.png';
				rename($file, $newFile);
				break;
			case 'x-torrent':
				$newFile = $file.'.torrent';
				rename($file, $newFile);
				break;
			case 'html':
				Main::echoTxt('Скачан html файл');
				return false;
			default:
				$newFile = $file.'.mp4';
				rename($file, $newFile);
				break;
		}
		return $newFile;
	}

	private static function getFileMimeType($file)
	{
		$mime = mime_content_type($file);
		$explode = explode('/', $mime);
		return $explode[0];
	}
}