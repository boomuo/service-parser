<?php

namespace app\models\Parser;

use Yii;
use yii\httpclient\Client;

class Turbobit
{
    public $tempFolder = __DIR__.DIRECTORY_SEPARATOR;
    private $cookieFile;
 
    public function __construct($login, $pass)
    {
        // Авторизация и получение куки
        /////////////////////////////////////////////////////////////////////////////
        $data['url'] = 'https://turbobit.net/user/login';
        $data['postData'] = [
            'user[login]' => $login,
            'user[pass]' => $pass,
            'user[captcha_type]' => '',
            'user[captcha_subtype]' => '',
            'user[submit]' => 'Войти',
            'user[memory]' => 'on',
        ];

        Main::init(7);
        $this->cookieFile = Main::autorization($data, 1);
    }

    public function uploadFile($file)
    {
        echo $html = Main::getPageFromCurl('https://turbobit.net', $this->cookieFile);
        exit;
        $headers = [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Accept-Language' => 'ru,uk;q=0.9,en;q=0.8',
            'Cache-Control' => 'max-age=0',
            'Connection' => 'keep-alive',
            'DNT' => 1,
            'Host' => 'turbobit.net',
            'Referer' => 'https://turbobit.net/user/files',
            'Upgrade-Insecure-Requests' => 1,
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'charset' => 'utf-8'
        ];
        // echo $this->cookieFile.PHP_EOL;
        $url = 'https://turbobit.net';
        $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36');
        // curl_setopt($ch, CURLOPT_POST, 1);
        // $this->postParams['upfile'] = "@".$file;
        // $postParams['upfile'] = new \CURLFile($file);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $postResult = curl_exec($ch); 
        $info = curl_getinfo($ch); 
        if(curl_errno($ch))
        {
            echo "Unable to upload file. - ".curl_error($ch).PHP_EOL;
        }
        else
        {
            curl_close($ch);
            echo $postResult;
        }
        echo '<pre>';
        print_r($info);
        echo '</pre>';
    }
}