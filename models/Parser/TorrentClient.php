<?php

namespace app\models\Parser;

use Yii;
use Transmission\Transmission;
use Transmission\Client;

class TorrentClient
{
	public $cookieFile;
	public $count;
	public $torrentLogin = 'transmission';
	public $torrentPassword = '123123g';

	function __construct()
	{
		$client = new Client();
		$client->authenticate($this->torrentLogin, $this->torrentPassword);
		$this->transmission = new Transmission();
		$this->transmission->setClient($client);
	}


    /**
     * Получает текущие файлы в torrent клиенте
     *
     * @param string $url
     * @return object $res
     */
	public function clear()
	{
		$all = $this->transmission->all();
		foreach ($all as $key => $item)
		{
			$item->remove();
		}
	}


    /**
     * Получает текущие файлы в torrent клиенте
     *
     * @param string $url
     * @return object $res
     */
	public function getTorrent($torrentId = false)
	{
		if($torrentId)
			return $this->transmission->get($torrentId);
		else
			return $this->transmission->all();
	}


    /**
     * Качает файлы через библиотеку торент Transmission
     *
     * @param string $torrentFile
     * @return string
     */
	public function addTorrent($torrentFile, $folder)
	{
		if(file_exists($torrentFile))
			return $this->transmission->add($torrentFile, false, $folder);
		throw new \Exception('Торрент файл не существует! - '.$torrentFile);
	}


    /**
     * Качает файлы через библиотеку торент Transmission
     *
     * @param string $torrentFile
     * @return string
     */
	public function getFolder()
	{
		return $this->transmission->getSession()->getDownloadDir().DIRECTORY_SEPARATOR;
	}


    /**
     * Получает protected свойство
     *
     * @param string $property
     * @param object $obj
     * @return string
     */
	public function getProperty($property, $obj)
	{
		$ref = new \ReflectionClass($obj);
		$prop = $ref->getProperty($property);
		$prop->setAccessible(true);

		// А теперь немного магии. Переменная $res после выполнения строчки ниже будет содержать
		// значение защищенного свойства.
		return $prop->getValue($obj);
	}
}