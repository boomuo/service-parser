<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "archive".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $category
 * @property string $tube
 * @property string $url
 * @property string $info
 * @property string $meta
 * @property string $downloadLinks
 * @property string $poster
 * @property string $screenshot
 * @property string $links
 * @property integer $status
 * @property integer $statusPublicationSite
 * @property string $dateTime
 * @property string $alert
 */
class Archive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'archive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status', 'statusPublicationSite'], 'integer'],
            [['info', 'meta', 'downloadLinks', 'poster', 'screenshot', 'links'], 'string'],
            [['dateTime'], 'safe'],
            [['category', 'tube', 'url', 'alert'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'category' => 'Category',
            'tube' => 'Tube',
            'url' => 'Url',
            'info' => 'Info',
            'meta' => 'Meta',
            'downloadLinks' => 'Ссылки с исчтоников',
            'poster' => 'Poster',
            'screenshot' => 'Screenshot',
            'links' => 'Ссылки на ФО',
            'status' => 'Status',
            'statusPublicationSite' => 'Status Publication Site',
            'dateTime' => 'Date Time',
            'alert' => 'Alert',
        ];
    }
}