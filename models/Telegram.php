<?php

namespace app\models;

use Yii;
use yii\httpclient\Client;

class Telegram {

	public $token  = '455390467:AAH8TcXE16eHBMmm9fMj5hwqD9CcgV36E9Y';
	public $chat_id  = '501596017';

	private function query($method, $params = [])
	{
		$url = 'https://api.telegram.org/bot'.$this->token.'/'.$method;

		$client = new Client([
			'baseUrl' => $url,
		]);
		$response = $client->createRequest()
		    ->setMethod('GET')
		    ->setData($params)
		    ->send();

		$result = json_decode($response->content);

		if ($response->isOk) {
			return $result;
		}
		return false;
	}

	public function sendMessage($chat_id = false, $message)
	{
		if(!$chat_id)
			$chat_id = $this->chat_id;

		$params = [
			'chat_id' => $chat_id,
			'parse_mode' => 'html',
			'text' => $message,
		];
		return $this->query('sendMessage', $params);
	}
}