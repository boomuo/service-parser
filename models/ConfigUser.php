<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_config".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $categories
 * @property string $proxy
 * @property integer $md5Change
 * @property string $minSaveSize
 * @property integer $forumPosterCount
 * @property string $forumPosterHosting
 * @property string $forumPosterHostingSize
 * @property string $forumScreenshotHosting
 * @property string $forumScreenshotHostingSize
 * @property integer $screenshotCols
 * @property integer $screenshotRows
 * @property integer $screenshotWidth
 * @property integer $trailerCreate
 * @property string $trailerCountSize
 * @property string $trailerOpenloadAccount
 * @property integer $archivesSizeCheck
 * @property integer $archivesSize
 * @property integer $uploadInFolder
 * @property integer $createTxt
 * @property integer $nowPublication
 * @property string $templateTxtFiles
 * @property string $txtFilesDelimer
 * @property string $accountFileHosting
 * @property string $accountPhotoHosting
 * @property string $accountTubeSite
 */
class ConfigUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'md5Change', 'nowPublication', 'forumPosterCount', 'screenshotCols', 'screenshotRows', 'screenshotWidth', 'trailerCreate', 'archivesSizeCheck', 'archivesSize', 'createTxt', 'uploadInFolder'], 'integer'],
            [['categories', 'templateTxtFiles', 'accountFileHosting', 'accountPhotoHosting', 'accountTubeSite', 'categoriesForums'], 'string'],
            [['proxy', 'minSaveSize', 'forumPosterHosting', 'forumPosterHostingSize', 'forumScreenshotHosting', 'forumScreenshotHostingSize', 'trailerCountSize', 'trailerOpenloadAccount', 'txtFilesDelimer', 'tubes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'categories' => 'Категории',
            'categoriesForums' => 'Категории форумов',
            'proxy' => 'Прокси',
            'md5Change' => 'Менять файлам md5',
            'nowPublication' => 'Публиковать сразу посре парсинга',
            'minSaveSize' => 'Игнорировать файлы меньше (мб)',
            'forumPosterCount' => 'Постеров шт.',
            'forumPosterHosting' => 'Хостинг постера',
            'forumPosterHostingSize' => 'Размер превью постера',
            'forumScreenshotHosting' => 'Хостинг скриншота',
            'forumScreenshotHostingSize' => 'Размер превью скриншота',
            'screenshotCols' => 'Столбиков в скриншоте',
            'screenshotRows' => 'Полей в скриншоте',
            'screenshotWidth' => 'Ширина скриншота',
            'trailerCreate' => 'Создавать трейлеры?',
            'trailerCountSize' => 'Сколько кусочков видео : По сколько секунд [10:3]',
            'trailerOpenloadAccount' => 'Openload аккаунт',
            'archivesSizeCheck' => 'Архивировать если ролик больше (мб)',
            'archivesSize' => 'Делить архив на части, размером по (мб)',
            'createTxt' => 'Создавать текстовый файл для публикации постов?',
            'templateTxtFiles' => 'Шаблон',
            'txtFilesDelimer' => 'Разделитель постов',
            'uploadInFolder' => 'Загружать файлы на хостинг в одноименные папки категорий',
            'accountFileHosting' => 'Аккаунты файлообмеников',
            'accountPhotoHosting' => 'Аккаунты фотохостингов',
            'accountTubeSite' => 'Аккаунты источников',
            'tubes' => 'Где парсить',
        ];
    }

    public function setId()
    {
        $this->user_id = Yii::$app->user->identity->id;
        return $this->user_id;
    }
}